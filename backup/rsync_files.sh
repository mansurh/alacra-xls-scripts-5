if [ $# -lt 2 ]; then echo "usage $0: location type [folder]"; exit 1; fi;

location=$1
type=$2
folder=$3
if [ $location = "IAD" ]
then
  IP="172.22.52.186"
elif [ $location = "DFW" ]
then
  IP="172.20.52.158"
else
  echo "location is either IAD or DFW"
  exit 1
fi

if [ $type = "local" ]
then
  DEST="LocalDataFiles"
else
  DEST="DataFiles/data"
fi

if [ "$folder" = "" ]; then folder="."; fi;

run()
{
cd $XLSFLAT
cmd="rsync -vv -rltO $folder $IP::$DEST/ --exclude=factiva/ --exclude=ftimes/"
date
echo "$cmd"
time $cmd
rc=$?
date
return $rc
}

mkdir -p $XLSDATA/backup
logfile=$XLSDATA/backup/rsync`date +%m%d%H%M%S`.log
(run) > $logfile 2>&1
rc=$?
echo status $rc
exit $rc