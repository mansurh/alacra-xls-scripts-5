set -o pipefail
if [ $# -lt 3 ]; then echo "usage $0: location type folder [minutes_back]"; exit 1; fi;

location=$1
type=$2
folder=$3
minutes_back=720
#see /etc/rsync.conf for mappings that are used to point to NAS location. Need to provide local IPs for chopper boxes to properly sync files to NAS
if [ $location = "IAD" ]
then
  #Local IP of chopper1 -- see /etc/rsync.conf for mappings that are used to point to NAS location
  IP="172.22.52.186"
elif [ $location = "DFW" ]
then
  #Local IP of chopper2
  IP="172.20.52.158"
else
  echo "location is either IAD or DFW"
  exit 1
fi

if [ $type = "local" ]
then
  DEST="LocalDataFiles"
else
  DEST="DataFiles/data"
fi

if [ $# -gt 3 ]
then
  minutes_back=$4
fi

if [ "$folder" = "" ]; then folder="."; fi;

run()
{
cd $XLSFLAT
#cmd="rsync -vv -rltO $folder $IP::$DEST/ --exclude=factiva/ --exclude=ftimes/"
date
set -x
time find $folder -type f -mmin -${minutes_back} -print0  | xargs -P 4 -0 -n 1 -I {} -- rsync -v -rRltO {} ${IP}::$DEST/
rc=$?
set +x
date
return $rc
}

mkdir -p $XLSDATA/backup
logfile=$XLSDATA/backup/rsync_${folder}_`date +%m%d%H%M%S`.log
(run) > $logfile 2>&1
rc=$?
echo status $rc
if [ $? -ne 0 ];
then
  blat $logfile -s "Rsync failed for ${folder}" -t administrators@alacra.com
fi
exit $rc