TITLEBAR="IndustryMap Load All"

#Command line arguments.
#	1 = ip
#	2 = dataserver

	

if [ $# -lt 2 ]
then
	
	echo "*** Need to enter parameters (Ip_file; Dataserver)***"
	echo "*** Usage: loadall.sh {ip} {dataserver}***"
	exit 1
fi

ip=$1
dataserver=$2

echo "*** Loading map for ${ip} on Ip file ${dataserver}***"

# alldir="CONCEPT COUNTRY GICS INDUSTRY KEYWORD "
alldir="COUNTRY INDUSTRY "

# Load Industry Map for given ${ip}

for a in $alldir
do
	echo "*** Loading map for ${ip}, sub-directory: ${a} ***"
	loadindustryMap.sh ${dataserver} xls xls mbiener mbiener ${ip} ${a}
done

echo " * Load Complete *"
exit 0

