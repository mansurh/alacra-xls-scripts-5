PRINT "paid users without an account"
PRINT "============================="
select id,CONVERT(VARCHAR(12),login),CONVERT(varchar(32),company),CONVERT (varchar (48),billing_reference) from users p where demo_flag is null and account is null

PRINT "paid users where startdate is less than account billing date"
PRINT "============================================================"
select p.id,CONVERT(VARCHAR(12),p.login) "Login",CONVERT(varchar(32),p.company) "Company", 
   min(p.commence) "Users Start", min(c.start_date)  "Account Start"
   from users p, charge c 
   where 
      p.demo_flag is null 
      and
      p.account = c.account 
   group by 
     p.id,CONVERT(VARCHAR(12),p.login),CONVERT(varchar(32),p.company) 
   having min (c.start_date) > min(p.commence)


PRINT "paid users where enddate is greater than account billing date"
PRINT "============================================================"
select p.id,CONVERT(VARCHAR(12),p.login) "Login",CONVERT(varchar(32),p.company) "Company", 
   max(p.terminate) "Users End", max(c.end_date)  "Account End"
   from users p, charge c 
   where 
      p.demo_flag is null 
      and
      p.account = c.account 
   group by 
     p.id,CONVERT(VARCHAR(12),p.login),CONVERT(varchar(32),p.company) 
   having max (c.end_date) < max(p.terminate)





PRINT "charge codes where the start date is after the end date"
PRINT "======================================================="
select account,description from charge where end_date < start_date

PRINT "users where the start date is after the end date"
PRINT "================================================"
select id,login from users where terminate < commence


PRINT "NY Zipcodes with no sales tax rate"
PRINT "=================================="
select distinct billing_postal_code from account where billing_state_prov=47
and SUBSTRING(billing_postal_code,1,5) not in (select postal_code from taxrates)
union
select distinct postal_code from account where state_prov=47
and SUBSTRING(postal_code,1,5) not in (select postal_code from taxrates)

PRINT "Charge codes with no AR code"
PRINT "============================"
select * from charge where araccount is NULL or araccount=""