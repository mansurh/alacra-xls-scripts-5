set transaction isolation level read uncommitted
set nocount on

declare @month datetime
select @month='October 1, 2013'

declare @currentMonthStart datetime
declare @currentMonthEnd datetime
declare @previousMonthStart datetime
declare @previousMonthEnd datetime
declare @monthYearAgoStart datetime
declare @monthYearAgoEnd datetime
declare @currentRollingQuarterStart datetime
declare @currentRollingQuarterEnd datetime
declare @previousRollingQuarterStart datetime
declare @previousRollingQuarterEnd datetime
declare @quarterYearAgoStart datetime
declare @quarterYearAgoEnd datetime

select @currentMonthStart = @month
select @currentMonthEnd = DATEADD(month, 1, @month)

select @previousMonthStart = DATEADD(month, -1, @currentMonthStart)
select @previousMonthEnd = DATEADD(month, -1, @currentMonthEnd)

select @monthYearAgoStart = DATEADD(year, -1, @currentMonthStart)
select @monthYearAgoEnd = DATEADD(year, -1, @currentMonthEnd)

select @currentRollingQuarterStart = DATEADD(month, -2, @month)
select @currentRollingQuarterEnd = DATEADD(month, 1, @month)

select @previousRollingQuarterStart = DATEADD(month, -3, @currentRollingQuarterStart)
select @previousRollingQuarterEnd = DATEADD(month, -3, @currentRollingQuarterEnd)

select @quarterYearAgoStart = DATEADD(year, -1, @currentRollingQuarterStart)
select @quarterYearAgoEnd = DATEADD(year, -1, @currentRollingQuarterEnd)

/************************************************ Logins ******************************************************************/

select outera.id 'Account ID', outera.name 'Account Name', s.last_name 'DRP', sam.last_name 'SAM',

(select count(*)
from logins l, users p
where 
l.userid = p.id
and
l.time >= @currentMonthStart and l.time < @currentMonthEnd
and p.account = outera.id
and p.demo_flag is NULL) as 'Current Month Logins',

(select count(*)
from logins l, users p
where 
l.userid = p.id
and
l.time >= @previousMonthStart and l.time < @previousMonthEnd
and p.account = outera.id
and p.demo_flag is NULL) as 'Previous Month Logins',

((select count(*)
from logins l, users p
where 
l.userid = p.id
and
l.time >= @currentMonthStart and l.time < @currentMonthEnd
and p.account = outera.id
and p.demo_flag is NULL) -
(select count(*)
from logins l, users p
where 
l.userid = p.id
and
l.time >= @previousMonthStart and l.time < @previousMonthEnd
and p.account = outera.id
and p.demo_flag is NULL)) 
 as 'Month on Month Delta',

(CASE 
	WHEN
		(select count(*)
		from logins l, users p
		where 
		l.userid = p.id
		and
		l.time >= @previousMonthStart and l.time < @previousMonthEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		= 0
	THEN
		0.0
	ELSE
		(
		(select CONVERT(float,count(*))
		from logins l, users p
		where 
		l.userid = p.id
		and
		l.time >= @currentMonthStart and l.time < @currentMonthEnd
		and p.account = outera.id
		and p.demo_flag is NULL) 
		-
		(select CONVERT(float,count(*))
		from logins l, users p
		where 
		l.userid = p.id
		and
		l.time >= @previousMonthStart and l.time < @previousMonthEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		)
		/
		(select CONVERT(float,count(*))
		from logins l, users p
		where 
		l.userid = p.id
		and
		l.time >= @previousMonthStart and l.time < @previousMonthEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
	END
) 
as 'Month on Month Pct Cng', 

(select count(*)
from logins l, users p
where 
l.userid = p.id
and
l.time >= @monthYearAgoStart and l.time < @monthYearAgoEnd
and p.account = outera.id
and p.demo_flag is NULL) as 'Month Year Ago Logins',

((select count(*)
from logins l, users p
where 
l.userid = p.id
and
l.time >= @currentMonthStart and l.time < @currentMonthEnd
and p.account = outera.id
and p.demo_flag is NULL) -
(select count(*)
from logins l, users p
where 
l.userid = p.id
and
l.time >= @monthYearAgoStart and l.time < @monthYearAgoEnd
and p.account = outera.id
and p.demo_flag is NULL)) 
 as 'Month Year Ago Delta',

(CASE 
	WHEN
		(select count(*)
		from logins l, users p
		where 
		l.userid = p.id
		and
		l.time >= @monthYearAgoStart and l.time < @monthYearAgoEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		= 0
	THEN
		0.0
	ELSE
		(
		(select CONVERT(float,count(*))
		from logins l, users p
		where 
		l.userid = p.id
		and
		l.time >= @currentMonthStart and l.time < @currentMonthEnd
		and p.account = outera.id
		and p.demo_flag is NULL) 
		-
		(select CONVERT(float,count(*))
		from logins l, users p
		where 
		l.userid = p.id
		and
		l.time >= @monthYearAgoStart and l.time < @monthYearAgoEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		)
		/
		(select CONVERT(float,count(*))
		from logins l, users p
		where 
		l.userid = p.id
		and
		l.time >= @monthYearAgoStart and l.time < @monthYearAgoEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
	END
) 
as 'Month Year Ago Pct Cng', 
 
(select count(*)
from logins l, users p
where 
l.userid = p.id
and
l.time >= @currentRollingQuarterStart and l.time < @currentRollingQuarterEnd
and p.account = outera.id
and p.demo_flag is NULL) as 'Current Rolling Quarter Logins',

(select count(*)
from logins l, users p
where 
l.userid = p.id
and
l.time >= @previousRollingQuarterStart and l.time < @previousRollingQuarterEnd
and p.account = outera.id
and p.demo_flag is NULL) as 'Previous Rolling Quarter Logins',

((select count(*)
from logins l, users p
where 
l.userid = p.id
and
l.time >= @currentRollingQuarterStart and l.time < @currentRollingQuarterEnd
and p.account = outera.id
and p.demo_flag is NULL) -
(select count(*)
from logins l, users p
where 
l.userid = p.id
and
l.time >= @previousRollingQuarterStart and l.time < @previousRollingQuarterEnd
and p.account = outera.id
and p.demo_flag is NULL)) 
 as 'Quarter on Quarter Delta',


(CASE 
	WHEN
		(select count(*)
		from logins l, users p
		where 
		l.userid = p.id
		and
		l.time >= @previousRollingQuarterStart and l.time < @previousRollingQuarterEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		= 0
	THEN
		0.0
	ELSE
		(
		(select CONVERT(float,count(*))
		from logins l, users p
		where 
		l.userid = p.id
		and
		l.time >= @currentRollingQuarterStart and l.time < @currentRollingQuarterEnd
		and p.account = outera.id
		and p.demo_flag is NULL) 
		-
		(select CONVERT(float,count(*))
		from logins l, users p
		where 
		l.userid = p.id
		and
		l.time >= @previousRollingQuarterStart and l.time < @previousRollingQuarterEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		)
		/
		(select CONVERT(float,count(*))
		from logins l, users p
		where 
		l.userid = p.id
		and
		l.time >= @previousRollingQuarterStart and l.time < @previousRollingQuarterEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
	END
) 
as 'Quarter on Quarter Pct Cng', 

(select count(*)
from logins l, users p
where 
l.userid = p.id
and
l.time >= @quarterYearAgoStart and l.time < @quarterYearAgoEnd
and p.account = outera.id
and p.demo_flag is NULL) as 'Quarter Year Ago Logins',
 
((select count(*)
from logins l, users p
where 
l.userid = p.id
and
l.time >= @currentRollingQuarterStart and l.time < @currentRollingQuarterEnd
and p.account = outera.id
and p.demo_flag is NULL) -
(select count(*)
from logins l, users p
where 
l.userid = p.id
and
l.time >= @quarterYearAgoStart and l.time < @quarterYearAgoEnd
and p.account = outera.id
and p.demo_flag is NULL)) 
 as 'Quarter Year Ago Delta',


(CASE 
	WHEN
		(select count(*)
		from logins l, users p
		where 
		l.userid = p.id
		and
		l.time >= @quarterYearAgoStart and l.time < @quarterYearAgoEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		= 0
	THEN
		0.0
	ELSE
		(
		(select CONVERT(float,count(*))
		from logins l, users p
		where 
		l.userid = p.id
		and
		l.time >= @currentRollingQuarterStart and l.time < @currentRollingQuarterEnd
		and p.account = outera.id
		and p.demo_flag is NULL) 
		-
		(select CONVERT(float,count(*))
		from logins l, users p
		where 
		l.userid = p.id
		and
		l.time >= @quarterYearAgoStart and l.time < @quarterYearAgoEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		)
		/
		(select CONVERT(float,count(*))
		from logins l, users p
		where 
		l.userid = p.id
		and
		l.time >= @quarterYearAgoStart and l.time < @quarterYearAgoEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
	END
) 
as 'Quarter Year Ago Pct Cng'

from account outera LEFT JOIN salesperson s ON outera.salesperson = s.id
LEFT JOIN salesperson sam ON outera.account_manager = sam.id

where outera.id in (
	select id from account where id in (select a.id from account a, charge c where a.billingCenter in ('US','UK') and c.account = a.id and c.start_date < DATEADD (month, 2, @currentMonthStart) and c.end_date >= @currentMonthStart)
)

order by 'Quarter on Quarter Pct Cng' asc

/************************************************** Books ***********************************************************/

select outera.id 'Account ID', outera.name 'Account Name', s.last_name 'DRP', sam.last_name 'SAM',

(select count(*)
from prepbook b, users p
where 
b.userid = p.id
and
b.pbendtime >= @currentMonthStart and b.pbendtime < @currentMonthEnd
and p.account = outera.id
and p.demo_flag is NULL) as 'Current Month Books',

(select count(*)
from prepbook b, users p
where 
b.userid = p.id
and
b.pbendtime >= @previousMonthStart and b.pbendtime < @previousMonthEnd
and p.account = outera.id
and p.demo_flag is NULL) as 'Previous Month Books',

((select count(*)
from prepbook b, users p
where 
b.userid = p.id
and
b.pbendtime >= @currentMonthStart and b.pbendtime < @currentMonthEnd
and p.account = outera.id
and p.demo_flag is NULL) -
(select count(*)
from prepbook b, users p
where 
b.userid = p.id
and
b.pbendtime >= @previousMonthStart and b.pbendtime < @previousMonthEnd
and p.account = outera.id
and p.demo_flag is NULL)) 
 as 'Month on Month Delta',

(CASE 
	WHEN
		(select count(*)
		from prepbook b, users p
		where 
		b.userid = p.id
		and
		b.pbendtime >= @previousMonthStart and b.pbendtime < @previousMonthEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		= 0
	THEN
		0.0
	ELSE
		(
		(select CONVERT(float,count(*))
		from prepbook b, users p
		where 
		b.userid = p.id
		and
		b.pbendtime >= @currentMonthStart and b.pbendtime < @currentMonthEnd
		and p.account = outera.id
		and p.demo_flag is NULL) 
		-
		(select CONVERT(float,count(*))
		from prepbook b, users p
		where 
		b.userid = p.id
		and
		b.pbendtime >= @previousMonthStart and b.pbendtime < @previousMonthEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		)
		/
		(select CONVERT(float,count(*))
		from prepbook b, users p
		where 
		b.userid = p.id
		and
		b.pbendtime >= @previousMonthStart and b.pbendtime < @previousMonthEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
	END
) 
as 'Month on Month Pct Cng', 

(select count(*)
from prepbook b, users p
where 
b.userid = p.id
and
b.pbendtime >= @monthYearAgoStart and b.pbendtime < @monthYearAgoEnd
and p.account = outera.id
and p.demo_flag is NULL) as 'Month Year Ago Books',

((select count(*)
from prepbook b, users p
where 
b.userid = p.id
and
b.pbendtime >= @currentMonthStart and b.pbendtime < @currentMonthEnd
and p.account = outera.id
and p.demo_flag is NULL) -
(select count(*)
from prepbook b, users p
where 
b.userid = p.id
and
b.pbendtime >= @monthYearAgoStart and b.pbendtime < @monthYearAgoEnd
and p.account = outera.id
and p.demo_flag is NULL)) 
 as 'Month Year Ago Delta',

(CASE 
	WHEN
		(select count(*)
		from prepbook b, users p
		where 
		b.userid = p.id
		and
		b.pbendtime >= @monthYearAgoStart and b.pbendtime < @monthYearAgoEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		= 0
	THEN
		0.0
	ELSE
		(
		(select CONVERT(float,count(*))
		from prepbook b, users p
		where 
		b.userid = p.id
		and
		b.pbendtime >= @currentMonthStart and b.pbendtime < @currentMonthEnd
		and p.account = outera.id
		and p.demo_flag is NULL) 
		-
		(select CONVERT(float,count(*))
		from prepbook b, users p
		where 
		b.userid = p.id
		and
		b.pbendtime >= @monthYearAgoStart and b.pbendtime < @monthYearAgoEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		)
		/
		(select CONVERT(float,count(*))
		from prepbook b, users p
		where 
		b.userid = p.id
		and
		b.pbendtime >= @monthYearAgoStart and b.pbendtime < @monthYearAgoEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
	END
) 
as 'Month Year Ago Pct Cng', 
 
(select count(*)
from prepbook b, users p
where 
b.userid = p.id
and
b.pbendtime >= @currentRollingQuarterStart and b.pbendtime < @currentRollingQuarterEnd
and p.account = outera.id
and p.demo_flag is NULL) as 'Current Rolling Quarter Books',

(select count(*)
from prepbook b, users p
where 
b.userid = p.id
and
b.pbendtime >= @previousRollingQuarterStart and b.pbendtime < @previousRollingQuarterEnd
and p.account = outera.id
and p.demo_flag is NULL) as 'Previous Rolling Quarter Books',

((select count(*)
from prepbook b, users p
where 
b.userid = p.id
and
b.pbendtime >= @currentRollingQuarterStart and b.pbendtime < @currentRollingQuarterEnd
and p.account = outera.id
and p.demo_flag is NULL) -
(select count(*)
from prepbook b, users p
where 
b.userid = p.id
and
b.pbendtime >= @previousRollingQuarterStart and b.pbendtime < @previousRollingQuarterEnd
and p.account = outera.id
and p.demo_flag is NULL)) 
 as 'Quarter on Quarter Delta',


(CASE 
	WHEN
		(select count(*)
		from prepbook b, users p
		where 
		b.userid = p.id
		and
		b.pbendtime >= @previousRollingQuarterStart and b.pbendtime < @previousRollingQuarterEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		= 0
	THEN
		0.0
	ELSE
		(
		(select CONVERT(float,count(*))
		from prepbook b, users p
		where 
		b.userid = p.id
		and
		b.pbendtime >= @currentRollingQuarterStart and b.pbendtime < @currentRollingQuarterEnd
		and p.account = outera.id
		and p.demo_flag is NULL) 
		-
		(select CONVERT(float,count(*))
		from prepbook b, users p
		where 
		b.userid = p.id
		and
		b.pbendtime >= @previousRollingQuarterStart and b.pbendtime < @previousRollingQuarterEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		)
		/
		(select CONVERT(float,count(*))
		from prepbook b, users p
		where 
		b.userid = p.id
		and
		b.pbendtime >= @previousRollingQuarterStart and b.pbendtime < @previousRollingQuarterEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
	END
) 
as 'Quarter on Quarter Pct Cng', 

(select count(*)
from prepbook b, users p
where 
b.userid = p.id
and
b.pbendtime >= @quarterYearAgoStart and b.pbendtime < @quarterYearAgoEnd
and p.account = outera.id
and p.demo_flag is NULL) as 'Quarter Year Ago Books',
 
((select count(*)
from prepbook b, users p
where 
b.userid = p.id
and
b.pbendtime >= @currentRollingQuarterStart and b.pbendtime < @currentRollingQuarterEnd
and p.account = outera.id
and p.demo_flag is NULL) -
(select count(*)
from prepbook b, users p
where 
b.userid = p.id
and
b.pbendtime >= @quarterYearAgoStart and b.pbendtime < @quarterYearAgoEnd
and p.account = outera.id
and p.demo_flag is NULL)) 
 as 'Quarter Year Ago Delta',


(CASE 
	WHEN
		(select count(*)
		from prepbook b, users p
		where 
		b.userid = p.id
		and
		b.pbendtime >= @quarterYearAgoStart and b.pbendtime < @quarterYearAgoEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		= 0
	THEN
		0.0
	ELSE
		(
		(select CONVERT(float,count(*))
		from prepbook b, users p
		where 
		b.userid = p.id
		and
		b.pbendtime >= @currentRollingQuarterStart and b.pbendtime < @currentRollingQuarterEnd
		and p.account = outera.id
		and p.demo_flag is NULL) 
		-
		(select CONVERT(float,count(*))
		from prepbook b, users p
		where 
		b.userid = p.id
		and
		b.pbendtime >= @quarterYearAgoStart and b.pbendtime < @quarterYearAgoEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		)
		/
		(select CONVERT(float,count(*))
		from prepbook b, users p
		where 
		b.userid = p.id
		and
		b.pbendtime >= @quarterYearAgoStart and b.pbendtime < @quarterYearAgoEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
	END
) 
as 'Quarter Year Ago Pct Cng'

from account outera LEFT JOIN salesperson s ON outera.salesperson = s.id
LEFT JOIN salesperson sam ON outera.account_manager = sam.id

where outera.id in (
	select id from account where id in (select a.id from account a, charge c where a.billingCenter in ('US','UK') and c.account = a.id and c.start_date < DATEADD (month, 2, @currentMonthStart) and c.end_date >= @currentMonthStart)
)

order by 'Quarter on Quarter Pct Cng' asc

/************************************************** Downloads *******************************************************/

select outera.id 'Account ID', outera.name 'Account Name', s.last_name 'DRP', sam.last_name 'SAM',

(select count(*)
from usage u, users p
where 
u.userid = p.id
and
u.access_time >= @currentMonthStart and u.access_time < @currentMonthEnd
and p.account = outera.id
and p.demo_flag is NULL) as 'Current Month Downloads',

(select count(*)
from usage u, users p
where 
u.userid = p.id
and
u.access_time >= @previousMonthStart and u.access_time < @previousMonthEnd
and p.account = outera.id
and p.demo_flag is NULL) as 'Previous Month Downloads',

((select count(*)
from usage u, users p
where 
u.userid = p.id
and
u.access_time >= @currentMonthStart and u.access_time < @currentMonthEnd
and p.account = outera.id
and p.demo_flag is NULL) -
(select count(*)
from usage u, users p
where 
u.userid = p.id
and
u.access_time >= @previousMonthStart and u.access_time < @previousMonthEnd
and p.account = outera.id
and p.demo_flag is NULL)) 
 as 'Month on Month Delta',

(CASE 
	WHEN
		(select count(*)
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @previousMonthStart and u.access_time < @previousMonthEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		= 0
	THEN
		0.0
	ELSE
		(
		(select CONVERT(float,count(*))
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @currentMonthStart and u.access_time < @currentMonthEnd
		and p.account = outera.id
		and p.demo_flag is NULL) 
		-
		(select CONVERT(float,count(*))
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @previousMonthStart and u.access_time < @previousMonthEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		)
		/
		(select CONVERT(float,count(*))
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @previousMonthStart and u.access_time < @previousMonthEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
	END
) 
as 'Month on Month Pct Cng', 

(select count(*)
from usage u, users p
where 
u.userid = p.id
and
u.access_time >= @monthYearAgoStart and u.access_time < @monthYearAgoEnd
and p.account = outera.id
and p.demo_flag is NULL) as 'Month Year Ago Downloads',

((select count(*)
from usage u, users p
where 
u.userid = p.id
and
u.access_time >= @currentMonthStart and u.access_time < @currentMonthEnd
and p.account = outera.id
and p.demo_flag is NULL) -
(select count(*)
from usage u, users p
where 
u.userid = p.id
and
u.access_time >= @monthYearAgoStart and u.access_time < @monthYearAgoEnd
and p.account = outera.id
and p.demo_flag is NULL)) 
 as 'Month Year Ago Delta',

(CASE 
	WHEN
		(select count(*)
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @monthYearAgoStart and u.access_time < @monthYearAgoEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		= 0
	THEN
		0.0
	ELSE
		(
		(select CONVERT(float,count(*))
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @currentMonthStart and u.access_time < @currentMonthEnd
		and p.account = outera.id
		and p.demo_flag is NULL) 
		-
		(select CONVERT(float,count(*))
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @monthYearAgoStart and u.access_time < @monthYearAgoEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		)
		/
		(select CONVERT(float,count(*))
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @monthYearAgoStart and u.access_time < @monthYearAgoEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
	END
) 
as 'Month Year Ago Pct Cng', 
 
(select count(*)
from usage u, users p
where 
u.userid = p.id
and
u.access_time >= @currentRollingQuarterStart and u.access_time < @currentRollingQuarterEnd
and p.account = outera.id
and p.demo_flag is NULL) as 'Current Rolling Quarter Downloads',

(select count(*)
from usage u, users p
where 
u.userid = p.id
and
u.access_time >= @previousRollingQuarterStart and u.access_time < @previousRollingQuarterEnd
and p.account = outera.id
and p.demo_flag is NULL) as 'Previous Rolling Quarter Downloads',

((select count(*)
from usage u, users p
where 
u.userid = p.id
and
u.access_time >= @currentRollingQuarterStart and u.access_time < @currentRollingQuarterEnd
and p.account = outera.id
and p.demo_flag is NULL) -
(select count(*)
from usage u, users p
where 
u.userid = p.id
and
u.access_time >= @previousRollingQuarterStart and u.access_time < @previousRollingQuarterEnd
and p.account = outera.id
and p.demo_flag is NULL)) 
 as 'Quarter on Quarter Delta',


(CASE 
	WHEN
		(select count(*)
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @previousRollingQuarterStart and u.access_time < @previousRollingQuarterEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		= 0
	THEN
		0.0
	ELSE
		(
		(select CONVERT(float,count(*))
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @currentRollingQuarterStart and u.access_time < @currentRollingQuarterEnd
		and p.account = outera.id
		and p.demo_flag is NULL) 
		-
		(select CONVERT(float,count(*))
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @previousRollingQuarterStart and u.access_time < @previousRollingQuarterEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		)
		/
		(select CONVERT(float,count(*))
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @previousRollingQuarterStart and u.access_time < @previousRollingQuarterEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
	END
) 
as 'Quarter on Quarter Pct Cng', 

(select count(*)
from usage u, users p
where 
u.userid = p.id
and
u.access_time >= @quarterYearAgoStart and u.access_time < @quarterYearAgoEnd
and p.account = outera.id
and p.demo_flag is NULL) as 'Quarter Year Ago Downloads',
 
((select count(*)
from usage u, users p
where 
u.userid = p.id
and
u.access_time >= @currentRollingQuarterStart and u.access_time < @currentRollingQuarterEnd
and p.account = outera.id
and p.demo_flag is NULL) -
(select count(*)
from usage u, users p
where 
u.userid = p.id
and
u.access_time >= @quarterYearAgoStart and u.access_time < @quarterYearAgoEnd
and p.account = outera.id
and p.demo_flag is NULL)) 
 as 'Quarter Year Ago Delta',


(CASE 
	WHEN
		(select count(*)
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @quarterYearAgoStart and u.access_time < @quarterYearAgoEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		= 0
	THEN
		0.0
	ELSE
		(
		(select CONVERT(float,count(*))
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @currentRollingQuarterStart and u.access_time < @currentRollingQuarterEnd
		and p.account = outera.id
		and p.demo_flag is NULL) 
		-
		(select CONVERT(float,count(*))
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @quarterYearAgoStart and u.access_time < @quarterYearAgoEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		)
		/
		(select CONVERT(float,count(*))
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @quarterYearAgoStart and u.access_time < @quarterYearAgoEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
	END
) 
as 'Quarter Year Ago Pct Cng'

from account outera LEFT JOIN salesperson s ON outera.salesperson = s.id
LEFT JOIN salesperson sam ON outera.account_manager = sam.id

where outera.id in (
	select id from account where id in (select a.id from account a, charge c where a.billingCenter in ('US','UK') and c.account = a.id and c.start_date < DATEADD (month, 2, @currentMonthStart) and c.end_date >= @currentMonthStart)
)

order by 'Quarter on Quarter Pct Cng' asc


/************************************************** PPV *******************************************************/

select outera.id 'Account ID', outera.name 'Account Name', s.last_name 'DRP', sam.last_name 'SAM',

(select COALESCE(sum(u.price),0.0)
from usage u, users p
where 
u.userid = p.id
and
u.access_time >= @currentMonthStart and u.access_time < @currentMonthEnd
and p.account = outera.id
and p.demo_flag is NULL) as 'Current Month PPV',

(select COALESCE(sum(u.price),0.0)
from usage u, users p
where 
u.userid = p.id
and
u.access_time >= @previousMonthStart and u.access_time < @previousMonthEnd
and p.account = outera.id
and p.demo_flag is NULL) as 'Previous Month PPV',

((select COALESCE(sum(u.price),0.0)
from usage u, users p
where 
u.userid = p.id
and
u.access_time >= @currentMonthStart and u.access_time < @currentMonthEnd
and p.account = outera.id
and p.demo_flag is NULL) -
(select COALESCE(sum(u.price),0.0)
from usage u, users p
where 
u.userid = p.id
and
u.access_time >= @previousMonthStart and u.access_time < @previousMonthEnd
and p.account = outera.id
and p.demo_flag is NULL)) 
 as 'Month on Month Delta',

(CASE 
	WHEN
		(select COALESCE(sum(u.price),0.0)
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @previousMonthStart and u.access_time < @previousMonthEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		= 0
	THEN
		0.0
	ELSE
		(
		(select CONVERT(float,COALESCE(sum(u.price),0.0))
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @currentMonthStart and u.access_time < @currentMonthEnd
		and p.account = outera.id
		and p.demo_flag is NULL) 
		-
		(select CONVERT(float,COALESCE(sum(u.price),0.0))
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @previousMonthStart and u.access_time < @previousMonthEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		)
		/
		(select CONVERT(float,COALESCE(sum(u.price),0.0))
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @previousMonthStart and u.access_time < @previousMonthEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
	END
) 
as 'Month on Month Pct Cng', 

(select COALESCE(sum(u.price),0.0)
from usage u, users p
where 
u.userid = p.id
and
u.access_time >= @monthYearAgoStart and u.access_time < @monthYearAgoEnd
and p.account = outera.id
and p.demo_flag is NULL) as 'Month Year Ago PPV',

((select COALESCE(sum(u.price),0.0)
from usage u, users p
where 
u.userid = p.id
and
u.access_time >= @currentMonthStart and u.access_time < @currentMonthEnd
and p.account = outera.id
and p.demo_flag is NULL) -
(select COALESCE(sum(u.price),0.0)
from usage u, users p
where 
u.userid = p.id
and
u.access_time >= @monthYearAgoStart and u.access_time < @monthYearAgoEnd
and p.account = outera.id
and p.demo_flag is NULL)) 
 as 'Month Year Ago Delta',

(CASE 
	WHEN
		(select COALESCE(sum(u.price),0.0)
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @monthYearAgoStart and u.access_time < @monthYearAgoEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		= 0
	THEN
		0.0
	ELSE
		(
		(select CONVERT(float,COALESCE(sum(u.price),0.0))
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @currentMonthStart and u.access_time < @currentMonthEnd
		and p.account = outera.id
		and p.demo_flag is NULL) 
		-
		(select CONVERT(float,COALESCE(sum(u.price),0.0))
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @monthYearAgoStart and u.access_time < @monthYearAgoEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		)
		/
		(select CONVERT(float,COALESCE(sum(u.price),0.0))
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @monthYearAgoStart and u.access_time < @monthYearAgoEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
	END
) 
as 'Month Year Ago Pct Cng', 
 
(select COALESCE(sum(u.price),0.0)
from usage u, users p
where 
u.userid = p.id
and
u.access_time >= @currentRollingQuarterStart and u.access_time < @currentRollingQuarterEnd
and p.account = outera.id
and p.demo_flag is NULL) as 'Current Rolling Quarter PPV',

(select COALESCE(sum(u.price),0.0)
from usage u, users p
where 
u.userid = p.id
and
u.access_time >= @previousRollingQuarterStart and u.access_time < @previousRollingQuarterEnd
and p.account = outera.id
and p.demo_flag is NULL) as 'Previous Rolling Quarter PPV',

((select COALESCE(sum(u.price),0.0)
from usage u, users p
where 
u.userid = p.id
and
u.access_time >= @currentRollingQuarterStart and u.access_time < @currentRollingQuarterEnd
and p.account = outera.id
and p.demo_flag is NULL) -
(select COALESCE(sum(u.price),0.0)
from usage u, users p
where 
u.userid = p.id
and
u.access_time >= @previousRollingQuarterStart and u.access_time < @previousRollingQuarterEnd
and p.account = outera.id
and p.demo_flag is NULL)) 
 as 'Quarter on Quarter Delta',


(CASE 
	WHEN
		(select COALESCE(sum(u.price),0.0)
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @previousRollingQuarterStart and u.access_time < @previousRollingQuarterEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		= 0
	THEN
		0.0
	ELSE
		(
		(select CONVERT(float,COALESCE(sum(u.price),0.0))
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @currentRollingQuarterStart and u.access_time < @currentRollingQuarterEnd
		and p.account = outera.id
		and p.demo_flag is NULL) 
		-
		(select CONVERT(float,COALESCE(sum(u.price),0.0))
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @previousRollingQuarterStart and u.access_time < @previousRollingQuarterEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		)
		/
		(select CONVERT(float,COALESCE(sum(u.price),0.0))
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @previousRollingQuarterStart and u.access_time < @previousRollingQuarterEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
	END
) 
as 'Quarter on Quarter Pct Cng', 

(select COALESCE(sum(u.price),0.0)
from usage u, users p
where 
u.userid = p.id
and
u.access_time >= @quarterYearAgoStart and u.access_time < @quarterYearAgoEnd
and p.account = outera.id
and p.demo_flag is NULL) as 'Quarter Year Ago PPV',
 
((select COALESCE(sum(u.price),0.0)
from usage u, users p
where 
u.userid = p.id
and
u.access_time >= @currentRollingQuarterStart and u.access_time < @currentRollingQuarterEnd
and p.account = outera.id
and p.demo_flag is NULL) -
(select COALESCE(sum(u.price),0.0)
from usage u, users p
where 
u.userid = p.id
and
u.access_time >= @quarterYearAgoStart and u.access_time < @quarterYearAgoEnd
and p.account = outera.id
and p.demo_flag is NULL)) 
 as 'Quarter Year Ago Delta',


(CASE 
	WHEN
		(select COALESCE(sum(u.price),0.0)
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @quarterYearAgoStart and u.access_time < @quarterYearAgoEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		= 0
	THEN
		0.0
	ELSE
		(
		(select CONVERT(float,COALESCE(sum(u.price),0.0))
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @currentRollingQuarterStart and u.access_time < @currentRollingQuarterEnd
		and p.account = outera.id
		and p.demo_flag is NULL) 
		-
		(select CONVERT(float,COALESCE(sum(u.price),0.0))
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @quarterYearAgoStart and u.access_time < @quarterYearAgoEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
		)
		/
		(select CONVERT(float,COALESCE(sum(u.price),0.0))
		from usage u, users p
		where 
		u.userid = p.id
		and
		u.access_time >= @quarterYearAgoStart and u.access_time < @quarterYearAgoEnd
		and p.account = outera.id
		and p.demo_flag is NULL)
	END
) 
as 'Quarter Year Ago Pct Cng'

from account outera LEFT JOIN salesperson s ON outera.salesperson = s.id
LEFT JOIN salesperson sam ON outera.account_manager = sam.id

where outera.id in (
	select id from account where id in (select a.id from account a, charge c where a.billingCenter in ('US','UK') and c.account = a.id and c.start_date < DATEADD (month, 2, @currentMonthStart) and c.end_date >= @currentMonthStart)
)

order by 'Quarter on Quarter Pct Cng' asc
