# ABI Usage Report

# Parse the command line arguments.
#	1 = month
#	2 = year
if [ $# != 2 ]
then
	echo "Usage: abi.sh month year"
	exit 1
fi

# Save the runstring parameters in local variables
month=$1
year=$2

# XLS Server Location
xlsserver="alaxls"
xlslogin="xls"
xlspassword="xls"

# ABI Server Location
abiserver="data2"
abilogin="abi"
abipassword="abi"

echo "Running ABI Usage Report for ${month}, ${year}"

# Step 1: Extract the ABI usage from the xls database

rm -f abiusage.tmp
if [ $? != 0 ]
then
	echo "Unable to clear abiusage.tmp, exiting"
	exit 1
fi

isql /U${xlslogin} /P${xlspassword} /S${xlsserver} -n -h-1 -w600 -s"|" >abiusage.tmp << HERE

select CONVERT(varchar(12),u.access_time,107) "date",CONVERT(varchar(16),s.ipinfo) from usage u, usagespecial s, users p
where 
ip=177
and
u.usageid = s.usage
and
u.userid = p.id
and
DATENAME(month,access_time) = "${month}"
and
DATEPART(year,access_time) = "${year}"
and
u.access_time >= p.commence
and
p.demo_flag is null

HERE

if [ $? != 0 ]
then
	echo "Unable to retrieve raw ABI usage data from xls server"
	exit 1
fi

# Build the query file
rm -f abiusage.sql
if [ $? != 0 ]
then
	echo "Unable to clear abiusage.sql, exiting"
	exit 1
fi

echo "create table #usage_temp (" >> abiusage.sql
echo "    access_time datetime," >> abiusage.sql
echo "    docid varchar (255)" >> abiusage.sql
echo ")" >> abiusage.sql

sed -f abi.sed < abiusage.tmp >> abiusage.sql

echo "select CONVERT(varchar(12),u.access_time,107) 'date', CONVERT(varchar(80),dp2.prop_value) 'title', CONVERT(varchar(10),dp1.prop_value) 'pmid', count(*) 'downloads' " >> abiusage.sql
echo "from doc_props dp1, doc_props dp2, #usage_temp u" >> abiusage.sql
echo "where" >> abiusage.sql
echo "dp1.prop_name = 'pmid'" >> abiusage.sql
echo "and" >> abiusage.sql
echo "dp2.prop_name = 'publication'" >> abiusage.sql
echo "and" >> abiusage.sql
echo "dp1.doc_id = dp2.doc_id" >> abiusage.sql
echo "and" >> abiusage.sql
echo "u.docid = dp1.doc_id" >> abiusage.sql
echo "group by u.access_time, dp2.prop_value, dp1.prop_value" >> abiusage.sql

echo "drop table #usage_temp" >> abiusage.sql

# Build the report file
reportName=abi_usage_${month}_${year}.txt
rm -f $reportName
if [ $? != 0 ]
then
	echo "Unable to clear abiusage.txt, exiting"
	exit 1
fi

isql /U${abilogin} /P${abipassword} /S${abiserver} -n -w600 -s"	" <abiusage.sql | grep -v "row..*affected" > $reportName

rm -f abiusage.tmp abiusage.sql
if [ $? != 0 ]
then
	echo "Unable to clean up intermediary files, exiting"
	exit 1
fi

exit 0
