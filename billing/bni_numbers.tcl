# Split the command line arguments.
#	1 = month 
#	2 = year
#	3 = server
#	4 = user
#	5 = password
set arglist [split $argv]
if {[llength $arglist] != 5} {
	puts "Usage: bni_numbers.tcl month year server user password"
	exit
}
set month [lindex $arglist 0]
set year [lindex $arglist 1]
set server [lindex $arglist 2]
set user [lindex $arglist 3]
set password [lindex $arglist 4]

# Open the connection to SQL Server
set handle [sybconnect $user $password $server]

# Select the active accounts
sybsql $handle "select y.accession_number \"RDS Accession Number\", CONVERT (varchar(8),y.journal_code) \"Journal Code\", y.title from xls..usageSpecial s, xls..usage u, xls..users p, rds..story y where u.access_time >= \"$month 1, $year\" and u.access_time < DATEADD(month, 1, \"$month 1, $year\") and u.userid = p.id and u.ip = 37 and u.access_time >= p.commence and p.demo_flag is null and u.usageid = s.usage and y.id = CONVERT (int, s.ipinfo) order by y.journal_code"

puts "Paid B&I Usage for $month, $year"
puts "RDS Accession Number|Journal Code|Title"
sybnext $handle {
	puts "@1|@2|[join @3 " "]"
}

# Close the connection to SQL Server
sybclose $handle
