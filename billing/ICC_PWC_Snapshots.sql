/* [mailto:JeffersM@dnb.com] */

In Alacra Log:
select description, count(*)
from alacralog_archive_YYYYMM
where 
eventtime >= 'March 1, 2011' 
and eventtime < 'April 1, 2011'
and accountid=3516
and description='Company Snapshot'
group by description, DATEPART(year,eventtime),DATEPART(month,eventtime)
order by description, DATEPART(year,eventtime), DATEPART(month,eventtime) asc


In XLS:
select a.name, a.id, count(*) from usage u, users p, account a
where
u.ip in (107,131)
and u.userid = p.id
and p.account = a.id
and u.access_time >= 'March 1, 2011' and u.access_time < 'April 1, 2011'
and a.id in (3516,6465)
group by a.name, a.id
order by count(*) desc
compute sum(count(*))