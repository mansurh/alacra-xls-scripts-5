declare @startdate datetime
declare @enddate datetime

select @startdate = 'November 1, 2004'
select @enddate = 'December 1, 2004'

/* List of Users */
select 
	p.id,
	p.account,
	p.company, 
	p.last_name, 
	p.first_name,
	p.demo_flag
into
	#temp_dmcm_users
from
	users p
where
	p.service=51
	/* and
	p.demo_flag is null */
	and
	p.commence < @enddate
	and
	p.terminate >= @startdate
order by 	
	p.account,
	p.company,
	p.last_name,
	p.first_name


/* Logins */
select 
	p.id,
	count (*) "Logins"
into
	#temp_dmcm_logins
from
	users p,
	logins l
where
	p.id = l.userid
	and
	p.service=51
	/* and
	p.demo_flag is null */
	and
	l.time >= @startdate
	and
	l.time < @enddate
group by
	p.id
order by
	p.id


/* Selections */
select 
	p.id, 
	count (*) "Selections"
into
	#temp_dmcm_selections
from 
	users p,
	usage u
where 
	p.id = u.userid
	and
	p.service=51
	/* and
	p.demo_flag is null */
	and
	u.access_time >= @startdate
	and
	u.access_time < @enddate
	and
	u.ip = 239
	and
	u.description like '%New Workspace'
group by
	p.id
order by
	p.id

/* Data Points Selected */
select 
	p.id,
	sum (convert (int, u.list_price)) 'DataPointsSelected'
into
	#temp_dmcm_data_points_selected
from
	users p,
	usage u
where 
	p.id = u.userid
	and
	p.service=51
	/* and
	p.demo_flag is null */
	and
	u.access_time >= @startdate
	and
	u.access_time < @enddate
	and
	u.ip = 239
	and
	u.description like '%New Workspace'
group by
	p.id
order by
	p.id


/* Views */
select
	p.id,
	count (*) "Views"
into
	#temp_dmcm_views 
from
	users p,
	usage u
where 
	p.id = u.userid
	and
	p.service=51
	/* and
	p.demo_flag is null */
	and
	u.access_time >= @startdate
	and
	u.access_time < @enddate
	and
	u.ip = 239
	and
	u.description like 'Data Points Viewed%'
group by
	p.id
order by
	p.id


/* Data Points Viewed */
select
	p.id,
	sum (convert (int, u.list_price)) "DataPointsViewed"
into
	#temp_dmcm_data_points_viewed
from
	users p,
	usage u
where 
	p.id = u.userid
	and
	p.service=51
	/* and
	p.demo_flag is null */
	and
	u.access_time >= @startdate
	and
	u.access_time < @enddate
	and
	u.ip = 239
	and
	u.description like 'Data Points Viewed%'
group by
	p.id
order by
	p.id


select
	#temp_dmcm_users.id "User ID",
	#temp_dmcm_users.account "Account ID",
	#temp_dmcm_users.company "Company", 
	#temp_dmcm_users.last_name "Last Name", 
	#temp_dmcm_users.first_name "First Name",
	#temp_dmcm_users.demo_flag "Trial",
	#temp_dmcm_logins.Logins "Logins",
	#temp_dmcm_selections.Selections "Selections",
	#temp_dmcm_data_points_selected.DataPointsSelected "Data Points Selected",
	#temp_dmcm_views.Views "Views",
	#temp_dmcm_data_points_viewed.DataPointsViewed "Data Points Viewed"

from
	#temp_dmcm_users LEFT JOIN #temp_dmcm_logins ON
	#temp_dmcm_users.id = #temp_dmcm_logins.id LEFT JOIN #temp_dmcm_selections ON
	#temp_dmcm_users.id = #temp_dmcm_selections.id LEFT JOIN
	#temp_dmcm_data_points_selected ON 
	#temp_dmcm_users.id = #temp_dmcm_data_points_selected.id LEFT JOIN
	#temp_dmcm_views ON #temp_dmcm_users.id = #temp_dmcm_views.id LEFT JOIN
	#temp_dmcm_data_points_viewed ON
	#temp_dmcm_users.id = #temp_dmcm_data_points_viewed.id
order by
	#temp_dmcm_users.company,
	#temp_dmcm_users.account,
	#temp_dmcm_users.last_name,
	#temp_dmcm_users.first_name,
	#temp_dmcm_users.id

drop table #temp_dmcm_users
drop table #temp_dmcm_logins
drop table #temp_dmcm_selections
drop table #temp_dmcm_data_points_selected
drop table #temp_dmcm_views
drop table #temp_dmcm_data_points_viewed
