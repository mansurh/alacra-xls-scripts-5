Monthly Billing Process

XXX 1) run Merrill_NoCharge_1234567890.sql (with date change) to get rid of test usage
2) run AutoDupQuery.sql (with date change) to list out the dups
3) modify AutoDupQuery.sql to delete the dups and run
4) run Consolidated.sql to generate the Thomson usage.  Chose pipe-delimited format.  Save as TFYYYYMM_consolidated.pip.  Compress into TFYYYYMM_consolidated.zip.  FTP to FTP.ALACRA.COM using the thomsonusage login.
5) Run ThomsonUserCounts.txt (with date change).  Save result in spreadsheet AlacraUserCounts_YYYYMM.xls by overwriting the content in a previous months file (to preserve formatting).  FTP to FTP.ALACRA.COM using the thomsonusage login.

XXXX (no longer any) 6) NOTE CHANGE TO DISTRIBUTION: Run TMCUsage.sql (with date change).  Save result in spreadsheet TMCUsage_YYYYMM.xlsx by overwriting the content in a previous months file (to preserve formatting).  Email to :
	compustatusagereports@standardandpoors.com
	Limor Zahav [lzahav@spcapitaliq.com] 
	janet.tarendash@alacra.com 

6a) Run CapIQUsage.sql (with date change).  Save result in spreadsheet CapIQUsage_YYYYMM.xls by overwriting the content in a previous months file (to preserve formatting).  Email to :
	compustatusagereports@standardandpoors.com
	Limor Zahav [lzahav@spcapitaliq.com], 
	janet.tarendash@alacra.com

7) Run the Moodys usage.  Follow NewMethodology.txt in the MoodysReportTypes subdirectory.

XXXXX 8) Run NewsTex.sql (with date change).  Save reports in spreadsheet Newstex_YYYYMM.xls by overwriting the content in a previous months file (to preserve formatting).  Email to:
	clients@newstex.com
	steven.goldstein@alacra.com
	barry.graubart@alacra.com

9) Run Tradeline.sql (with date change).  Save results in spreadsheet JPMTradeline_YYYYMM.xls by overwriting the content in a previous months file (to preserve formatting).  Email to Steve G.

XXXXX 10) Run PerfectInfo.sql (with date change).  Save reports in spreadsheet PerfectInfo_YYYYMM.xls by overwriting the content in a previous months file (to preserve formatting).  Email to:
	LauraM@perfectinfo.com

11) Run Proactive Monitoring II.txt (with date change).  Save reports (3 tabs) in spreadsheet Proactive_YYYYMM.xls by overwriting the content in a previous months file (to preserve formatting).  Email to:
	Fran, Gail, Don, Steve


12) Execute EandYUsageReport.sh

XXX 13) Execute ALMUsageReport.sh

XXX 14) run ICC_PWC_Snapshots.sql and sent to Mattew Jeffers (JeffersM@dnb.com)

15) run docs/FactsetUsageQueryForDavidWerkheiser.txt and email to rhealy@factset.com





