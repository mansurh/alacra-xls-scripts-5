use Win32::OLE;
use OLE;

unless (@ARGV == 5) {
	die("usage: createMandAPageSitemaps SERVER DATABASE USER PASSWORD SRVNAME");
}
$SERVER=$ARGV[0];
$DATABASE=$ARGV[1];;
$USER=$ARGV[2];
$PASSWORD=$ARGV[3];
$SRVNAME=$ARGV[4];

my $counter=0;
$fieldNum=3;
$PRI_BATCH=5000;

$QUERY = "select c.id, master.dbo.removePunctAndSpaces(master.dbo.Anglicize(c.name)), SUBSTRING(convert(varchar, CASE WHEN max(m.deal_date) < 'August 1, 2008' then 'August 1, 2008' else max(m.deal_date) end, 120), 1, 10) " ;
$QUERY .= "from company c, deal_map m, ( " ;
$QUERY .= "	select xls_deal_id, acquirer_xlsid as xlsid from deals where status=1 and acquirer_xlsid is not null " ;
$QUERY .= "	union " ;
$QUERY .= "	select xls_deal_id, acquirer_parent_xlsid as xlsid from deals where status=1 and acquirer_parent_xlsid is not null " ;
$QUERY .= "	union " ;
$QUERY .= "	select xls_deal_id, target_parent_xlsid as xlsid from deals where status=1 and target_parent_xlsid is not null " ;
$QUERY .= ") d " ;
$QUERY .= "where c.id = d.xlsid " ;
$QUERY .= "and d.xls_deal_id = m.xls_deal_id " ;
$QUERY .= "and m.ip = 83 " ;
$QUERY .= "and m.deleted_flag is null " ;
$QUERY .= "and c.id in (select distinct company from co_industry where p_flag='p') " ;
$QUERY .= "and c.id IN ( " ;
$QUERY .= "	select m.xlsid " ;
$QUERY .= "	from company_map m, company_source s " ;
$QUERY .= "	where s.sourceid = m.source " ;
$QUERY .= "	and s.ip = 83 " ;
$QUERY .= ") " ;
$QUERY .= "and c.research_status is null " ;
$QUERY .= "group by c.id, c.name " ;
$QUERY .= "order by c.name desc " ;


$DELQUERY = "delete sitemaps where dbase='search' and regkey='deals' and server='$SRVNAME' and part > 999";

$counter=1000;

print ("$QUERY \n");

my $SITEMAPFILE;
unless (open(SITEMAPFILE, ">sitemap.sql")){
	die "cannot open sitemap.sql file";
}
#exit 0;

my $OUTFILE;
&openFile();

#Create connection strings
my $conn=CreateObject OLE "ADODB.Connection" || die "CreateObject: $!";
my $connect="DRIVER={SQL SERVER};PWD=$PASSWORD;UID=$USER;SERVER=$SERVER;DATABASE=$DATABASE;Timeout=1800";

#Connect
$conn-> {ConnectionTimeout} = 1800;
$conn-> {CommandTimeout} = 1800;
$conn->open($connect);

#Execute query
my $sql=$QUERY;

my $rs;
unless($rs=$conn->Execute($sql)){
  die("error executing the SQL statement");
}

$MAX_BATCH=48000;
$total_count=0;
$batch_count=0;
$pri_count=0;
#Get values
$myfldcnt = $rs->Fields->Count;
if ($myfldcnt eq $fieldNum ){
	print SITEMAPFILE ("$DELQUERY\n");
	while ( !$rs->EOF){
		$total_count++;
		$batch_count++;
		$pri_count++;
		if ($batch_count > $MAX_BATCH) {
			&closeFile(1, $batch_count-1);
			$batch_count=1;
			$counter++;
			&openFile();
		}
		if ($PRI_BATCH > 0 && $pri_count > $PRI_BATCH) {
			$pri_count=1;
		}
		$cid =  $rs->Fields(0)->value;
		$name = $rs->Fields(1)->value;
		$date = $rs->Fields(2)->value;
		&writeSet($name, $date, $cid);
		$rs->MoveNext;
	}
	print("total records: $total_count\n");
} else {
	print ("number of fields is not equal to $fieldNum\n");
	exit 1;
}

&closeFile(0, $batch_count-1);
$rs->Close;
$conn->Close;

sub closeFile{
	local ($gzip, $batch) = @_;
	print OUTFILE ("</urlset>\n");
	close (OUTFILE);
	$file=sprintf("search_deals_%04d.smx", $counter);
	if ($gzip eq 1 ) {
		system("gzip -f $file");
	}
	print SITEMAPFILE ("insert into sitemaps(dbase, regkey, server, part, archived, indextime, lastmod, doc_count, sitemap_size, metadata_size)\n");
	print SITEMAPFILE ("values('search', 'deals', '$SRVNAME', $counter, $gzip, getdate(), getdate(), $batch, 0, 0)\n ");


}

sub openFile {
	$file=sprintf("search_deals_%04d.smx", $counter);
	unless (open(OUTFILE, ">$file")){
		die "cannot open $file file";
	}
print ("$file\n");
	print OUTFILE ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
	print OUTFILE ("<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n");
}

sub writeSet {
	local ($stopname, $date, $cid) = @_ ;
	#$date=gmtime();
	print OUTFILE ("<url>\n");
	print OUTFILE ("<loc>http://www.alacrastore.com/mergers-acquisitions/$stopname-$cid</loc>\n");
	print OUTFILE ("<lastmod>$date</lastmod>\n");
	print OUTFILE ("<changefreq>weekly</changefreq>\n");
	print OUTFILE ("</url>\n");
}
