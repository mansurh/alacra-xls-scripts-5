use Win32::OLE;
use HTTP::Date;
use OLE;

unless (@ARGV >= 5) {
	die("usage: createGenericSitemaps SERVER DATABASE USER PASSWORD INDEXNAME SITEURI QUERY [MAX_RECORDS]");
}
my $SERVER=$ARGV[0];
my $DATABASE=$ARGV[1];;
my $USER=$ARGV[2];
my $PASSWORD=$ARGV[3];
my $INDEXNAME=$ARGV[4];
my $SITEURI=$ARGV[5];
my $QRY=$ARGV[6];
my $DEF_DATE="2013-01-01";
my $MAX_RECORDS="";
if (@ARGV > 7) {
  my $MAX_RECORDS=$ARGV[7];
}

my $freq;
my $counter=1;
$fieldNum=2;

$QUERY="";
if ($MAX_RECORDS ne "") {
	$TOP_STRING=sprintf("set rowcount %d ", $MAX_RECORDS);
}
$QUERY = sprintf("%s %s", $TOP_STRING, $QRY); 

$DELQUERY = sprintf("delete sitemaps where dbase='%s' and regkey='%s' and server = '%s'\n", $DATABASE, $INDEXNAME, $SERVER);

print "$QUERY $SITEURI\n";

my $SITEMAPFILE;
unless (open(SITEMAPFILE, ">sitemap.sql")){
	die "cannot open sitemap.sql file";
}

my $OUTFILE;
&openFile();

#Create connection strings
my $conn=CreateObject OLE "ADODB.Connection" || die "CreateObject: $!";
#30 minutes should be enough
$conn->{'CommandTimeout'} = 1800;
my $connect="DRIVER={SQL SERVER};PWD=$PASSWORD;UID=$USER;SERVER=$SERVER;DATABASE=$DATABASE";

#Connect
$conn->open($connect);

#Execute query
my $sql=$QUERY;

my $rs;

unless($rs=$conn->Execute($sql)){
  die("error executing the SQL statement");
}

$MAX_BATCH=45000;
$total_count=0;
$batch_count=0;
#Get values
$currentDateTime = HTTP::Date::time2iso();
$myfldcnt = $rs->Fields->Count;
if ($myfldcnt eq $fieldNum ){
	print SITEMAPFILE ("$DELQUERY\n");
	while ( !$rs->EOF){
		$total_count++;
		$batch_count++;
		if ($batch_count > $MAX_BATCH) {
			&closeFile(1, $batch_count-1);
			$batch_count=1;
			$counter++;
			&openFile();
		}
		$id = $rs->Fields(0)->value;
		$date = $rs->Fields(1)->value;
		$parsedt = HTTP::Date::parse_date($date);
		if (!$parsedt  || substr($date,0,4) lt "2000" || $parsedt>$currentDateTime ) 
			{ $date=$DEF_DATE; }
		else {
			$date=sprintf("%s-%s-%s", substr($date,0,4),substr($date,4,2),substr($date,6));		}
		# use translate spaces to underscores
#		$name =~ tr/ /_/;
		&writeSet($id, $date);
		$rs->MoveNext;
	} 
	print("total records: $total_count\n");
} else {
	print ("number of fields is not equal to $fieldNum\n"); 
	exit 1;
}

&closeFile(0, $batch_count-1);
$rs->Close;
$conn->Close;

sub closeFile{
	local ($gzip, $batch) = @_;
	print OUTFILE ("</urlset>\n");
	close (OUTFILE);
	$file=sprintf("%s_%s_%03d.smx", $DATABASE, $INDEXNAME, $counter);
	if ($gzip eq 1 ) {
		system("gzip -f $file");
	}
	print SITEMAPFILE ("insert into sitemaps(dbase, regkey, server, part, archived, indextime, lastmod, doc_count, sitemap_size, metadata_size)\n");
	print SITEMAPFILE ("values('$DATABASE', '$INDEXNAME', '$SERVER', $counter, $gzip, getdate(), getdate(), $batch, 0, 0)\n ");
}

sub openFile {
	$file=sprintf("%s_%s_%03d.smx", $DATABASE, $INDEXNAME, $counter);
	unless (open(OUTFILE, ">$file")){
		die "cannot open $file file";
	}
print ("$file\n");
	print OUTFILE ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
	print OUTFILE ("<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n");
}

sub writeSet {
	local ($id, $date) = @_ ;
	print OUTFILE ("<url>");
	print OUTFILE ("<loc>http://$SITEURI/$id</loc>");
	if ($date ne "") {
		print OUTFILE ("<lastmod>$date</lastmod>"); }
	print OUTFILE ("<changefreq>yearly</changefreq>");
	print OUTFILE ("</url>\n");
}
