use Win32::OLE;
use OLE;

unless (@ARGV == 5) {
	die("usage: createCompanySitemaps SERVER DATABASE USER PASSWORD SRVNAME");
}
$SERVER=$ARGV[0];
$DATABASE=$ARGV[1];;
$USER=$ARGV[2];
$PASSWORD=$ARGV[3];
my $SRVNAME=$ARGV[4];

my $freq;
my $pri = "1.0";
my $counter=0;
$fieldNum=2;
$PRI_BATCH=5000;
$QUERY = "exec SelectPulseSitemapData";
$DELQUERY = "delete sitemaps where dbase='weblogdb' and regkey='weblogdb' and server='$SRVNAME' ";
$counter=1;
$freq="daily";

# print ("$QUERY \n");


sub prettydate { 
   @_ = localtime(shift || time); 
   return(sprintf("%04d-%02d-%02d", $_[5]+1900, $_[4]+1, $_[3])); 
}


my $datestr = prettydate();

#print ("$datestr \n");

#exit 0;



my $SITEMAPFILE;
unless (open(SITEMAPFILE, ">sitemap.sql")){
	die "cannot open sitemap.sql file";
}

my $OUTFILE;
&openFile();

#Create connection strings
my $conn=CreateObject OLE "ADODB.Connection" || die "CreateObject: $!";

my $connect="DRIVER={SQL SERVER};PWD=$PASSWORD;UID=$USER;SERVER=$SERVER;DATABASE=$DATABASE";

#Connect
$conn->open($connect);
$conn->{CommandTimeout} = 900; 

#Execute query
my $sql=$QUERY;

my $rs;

unless($rs=$conn->Execute($sql)){
  die("error executing the SQL statement");
}


$MAX_BATCH=48000;
$total_count=0;
$batch_count=0;
$pri_count=0;
#Get values
$myfldcnt = $rs->Fields->Count;
if ($myfldcnt eq $fieldNum ){
	print SITEMAPFILE ("$DELQUERY\n");
	while ( !$rs->EOF){
		$total_count++;
		$batch_count++;
		$pri_count++;
		if ($batch_count > $MAX_BATCH) {
			&closeFile(1, $batch_count-1);
			$batch_count=1;
			$counter++;
			&openFile();
		}

		$url = $rs->Fields(0)->value;
		$pri = $rs->Fields(1)->value;
		&writeSet($url, $datestr, $pri);

		$rs->MoveNext;
	} 
	print("total records: $total_count\n");
} else {
	print ("number of fields is not equal to $fieldNum\n"); 
	exit 1;
}

&closeFile(0, $batch_count-1);
$rs->Close;
$conn->Close;

sub closeFile{
	local ($gzip, $batch) = @_;
	print OUTFILE ("</urlset>\n");
	close (OUTFILE);
	$file=sprintf("weblogdb_weblogdb_%03d.smx", $counter);
	if ($gzip eq 1 ) {
		system("gzip -f $file");
	}
	print SITEMAPFILE ("insert into sitemaps(dbase, regkey, server, part, archived, indextime, lastmod, doc_count, sitemap_size, metadata_size)\n");
	print SITEMAPFILE ("values('weblogdb', 'weblogdb', '$SRVNAME', $counter, $gzip, getdate(), getdate(), $batch, 0, 0)\n ");


}

sub openFile {
	$file=sprintf("weblogdb_weblogdb_%03d.smx", $counter);
	unless (open(OUTFILE, ">$file")){
		die "cannot open $file file";
	}
print ("$file\n");
	print OUTFILE ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
	print OUTFILE ("<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n");
}

sub writeSet {
	local ($url, $date, $priority) = @_ ;
	print OUTFILE ("<url>\n");
	print OUTFILE ("<loc>$url</loc>\n");
	print OUTFILE ("<lastmod>$date</lastmod>\n");
	print OUTFILE ("<changefreq>$freq</changefreq>\n");
	print OUTFILE ("<priority>$priority</priority>\n");
	print OUTFILE ("</url>\n");
}
