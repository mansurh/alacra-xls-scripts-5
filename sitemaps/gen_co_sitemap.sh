if [ $# -ne 1 ]
then
	echo "usage: $0 server"
	exit 1
fi
server=$1
logfilename=logfile${server}.log
rm -r -f ${logfilename}

wget -O temp${server}.htm "http://${server}.alacrastore.com/asp/gen_co_sitemap.asp" -a ${logfilename}
echo "result from ${server}" >> ${logfilename}
cat temp${server}.htm >> ${logfilename}

blat ${logfilename} -t "dbinfo@alacra.com, jarid.lukin@alacra.com" -s "Gen_co_sitemap results for ${server}"
exit 0
