if [ $# -ne 1 ]
then
    echo "Usage: ResetSitemaps.sh YYYY-MM-DD"
    exit 1
fi

date=$1

scriptdir=$XLS/src/scripts/sitemaps
for file in *.smx.gz
do
  if [ $file != "*.smx.gz" ]
  then
    echo ${file}
    #cp $file $file.BAK
    gzip -d ${file}
    file=${file%.gz}
    perl ${scriptdir}/ResetLastmod.pl ${file} ${date} ${file}.tmp 
    mv ${file}.tmp ${file}
    gzip ${file}
  fi
done

for file in *.smx
do
  echo ${file}
  #cp $file $file.BAK
  perl ${scriptdir}/ResetLastmod.pl ${file} ${date} ${file}.tmp 
  mv ${file}.tmp ${file}
done