# Open the connection to SQL Server
set handle [sybconnect xls xls idaho]

# Process the List of Spreadsheets
sybsql $handle "select id,title from ss where source=36 order by title"
sybnext $handle {
		puts [format "<LI><A HREF=\"/cgi-bin/spreaddetail.exe?id=%s\">%s</A>" @1 @2]
}

# Close the connection to SQL Server
sybclose $handle
