# Command file to transfer and install a software update onto a web server
# Usage: installpackage.sh packagename

# Parse the command line arguments.
#	1 = packagename
#	2 = optional 'noundo' flag
if [ $# -lt 1 ]
then
	echo "Usage: installpackage.sh packagename [undo flag]"
	exit 1
fi

# Save the runstring parameters in local variables
packagename=$1

undoflag=""
# optional 'no undo' flag - means do not make undo tar file
if [ $# -gt 1 ]
then
	undoflag=$2
fi

installsemail="installs-alacra@opus.com"
# create unique log file name, based on date
logfilename=c:/install.${packagename}.log

echo "Installing Package ${packagename}" > ${logfilename}

# Perform the install
installpackagelow.sh ${packagename} ${undoflag} >> ${logfilename} 2>&1
returncode=$?

# Mail the results to the administrators if update was no good
if [ ${returncode} != 0 ]
then
	echo "" >> ${logfilename}
	echo "Bad return code from installpackagelow.sh - mailing log file" >> ${logfilename}
	blat ${logfilename} -t "${installsemail}" -f "${installsemail}" -s "Package ${packagename} install failed on $COMPUTERNAME"
else
	echo "" >> ${logfilename}
	echo "Success from installpackagelow.sh - mailing log file" >> ${logfilename}
	blat ${logfilename} -t "${installsemail}" -f "${installsemail}" -s "Package ${packagename} successfully installed on $COMPUTERNAME"
fi

exit $returncode
