# Routine to unpack builds

shname=qpbunpack.sh

# Parse the command line arguments
#	1 = date YYYYMMDD

if [ $# != 1 ]
then
	echo "Usage: ${shname} YYYYMMDD"
	exit 1
fi

# Save the runstring parameters in local variables
thedate=$1

distlistfile=distribution.pb.${thedate}.lst
disttarzfile=distribution.pb.${thedate}.tar.Z
distunpacklogfile=distribution.pb.${thedate}.log

#echo "Press Enter to unpack distribution file ${disttarzfile}"
#read zz9
echo "Unpacking distribution file ${disttarzfile}"
zcat ${disttarzfile} | tar -xvf -  > ${distunpacklogfile} 2>&1

#start notepad ${distunpacklogfile}

exit 0
