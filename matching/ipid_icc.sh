# Script file to build the IP matching table for Investext
# 1 = IPID Server
# 2 = IPID database
# 3 = IPID Login
# 4 = IPID Password
# 5 = ICC Server
# 6 = ICC Login
# 7 = ICC Password
if [ $# -lt 7 ]
then
	echo "Usage: ipid_icc.sh ipid_server ipid_database ipid_login ipid_password icc_server icc_login icc_password"
	exit 1
fi

ipidserver=$1
ipiddatabase=$2
ipidlogin=$3
ipidpassword=$4
ipserver=$5
iplogin=$6
ippassword=$7

ipname=icc

mkdir -p $XLSDATA/matching/icc
cd $XLSDATA/matching/icc

# Name of the temp file to use
TMPFILE1=ipid_${ipname}1.tmp
DATETEMPL="`date +%Y'-'%b'-'%d`"
TMPFILE2=ICC-${DATETEMPL}.tmp

# Name of the table to use
TABLENAME=ipid_${ipname}

# Name of the format file to use
FORMAT_FILE=ipid_${ipname}.fmt

# Step 1 - remove any old temp files
rm -f ${TMPFILE1} ${TMPFILE2}

# Step 2 - select the ${ipname} data into a temporary file
isql -S${ipserver} -U${iplogin} -P${ippassword} -s"|" -w500 -n -h-1 >${TMPFILE1} << HERE
SET NOCOUNT ON
select m.regd_no, n.name, s.sedol
from (((gz_mast m inner join gz_name n on m.regd_no = n.regd_no)
inner join company_flags cf on m.regd_no = cf.regd_no)
left outer join ow_security s on m.regd_no = s.regd_no)
where cf.size_type in ('O', 'P', 'Q', 'T', 'U')
HERE

# Step 3 - post-process the temp file
sed -f ${XLS}/src/scripts/matching/match.sed < ${TMPFILE1} > ${TMPFILE2}

# Step 4 - drop the old id table - don't check for error
# as it may not exist
isql /S${ipidserver} /U${ipidlogin} /P${ipidpassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 5 - create the new table
isql /S${ipidserver} /U${ipidlogin} /P${ipidpassword} << HERE
create table ${TABLENAME} (
	matchkey varchar(8) NOT NULL,
    name varchar(160) NULL,
	sedol varchar(10) NULL
)
GO

HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 6 - bcp in the select results
bcp ${TABLENAME} in ${TMPFILE2} -S ${ipidserver} -U ${ipidlogin} -P ${ipidpassword} -f ${XLS}/src/scripts/matching/${FORMAT_FILE} -b 100
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	endupdate="`date +%a' '%b' '%e'  '%T'  '%Z' '%Y`"
	blat ${TMPFILE2} -t "administrators@alacra.com,simon.vileshin@alacra.com" -s "${endupdate}. Update of ipid_icc table on ${ipidserver} has failed."
	blat ${TMPFILE2} -t "companymatch@alacra.com,simon.vileshin@alacra.com" -s "Contents of file: ${TMPFILE2}"
	exit 1
fi

# Step 7 - create indexes
isql /S${ipidserver} /U${ipidlogin} /P${ipidpassword} << HERE
create index ${TABLENAME}_01 on ${TABLENAME}
	(matchkey)
GO
create index ${TABLENAME}_02 on ${TABLENAME}
	(name)
GO
create index ${TABLENAME}_03 on ${TABLENAME}
	(sedol)
GO
HERE

# Step 8 - notify
endupdate="`date +%a' '%b' '%e'  '%T'  '%Z' '%Y`"
blat ${TMPFILE2} -t "companymatch@alacra.com,simon.vileshin@alacra.com" -s "Contents oile: ${TMPFILE2}"

# Step 9 - clean up
#rm -f ${TMPFILE1} ${TMPFILE2}


exit 0

