--IPID_DNB and IPID_DNBUM

if Exists(SELECT * FROM sys.objects WHERE  object_id = OBJECT_ID(N'dump_ipid_data_kwic') AND type IN ( N'P', N'PC' ))
BEGIN
	drop procedure dump_ipid_data_kwic
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE dump_ipid_data_kwic
@UMTable as Varchar(2) = 'NO'
AS
BEGIN
	IF @UMTable = 'NO'
	BEGIN
		Select * from companyKwicNTS1 where dunsNo like '%0'  
		union all
		Select * from companyKwicTS2345 where dunsNo like '%0'  
	END
	ELSE 
	BEGIN 
		Select * from companykwicUM where dunsNo like '%0'  
	END

END
