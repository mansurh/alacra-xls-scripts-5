XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Script file to build the IP matching table for Moodys
# Created 21 June 2005 by John Willcox

WD=$XLS/src/scripts/matching

DATADIR=$XLSDATA/matching/moodys
mkdir -p $DATADIR
cd $DATADIR

# 1 = IPID Server
# 2 = IPID Database
# 3 = IPID Login
# 4 = IPID Password

ARGS=4
if [ $# -ne $ARGS ]
then
	echo "Usage: ipid_moodys.sh ipid_server ipid_database ipid_login ipid_password " 
	exit 1
fi

IPIDserver=$1
IPIDdatabase=$2
IPIDlogin=$3
IPIDpassword=$4
#FTPserver=$5
#FTPlogin=$6
#FTPpassword=$7
FTPserver=`get_xls_registry_value moodysconcord Server 6`
FTPlogin=`get_xls_registry_value moodysconcord User 6`
FTPpassword=`get_xls_registry_value moodysconcord Password 6`

#IPIDserver=192.168.65.131
#IPIDdatabase="concordance"
#IPIDlogin="concordance_dbo"
#IPIDpassword="concordance_dbo"
#FTPserver=ftp.moodys.com
#FTPlogin=alacraindus
#FTPpassword=Moodys1-RDS

getFilename="busn_lines_qry_fw.txt"
editFilename="busn_lines_qry_fwEDIT.txt"

IPIDtable="ipid_moodys"

formatFilename="${WD}/ipid_moodys.fmt"

# FTP the data file
echo "user ${FTPlogin} ${FTPpassword}" > ipid_moodys.ftp
echo "binary" >> ipid_moodys.ftp
echo "get ${getFilename}" >> ipid_moodys.ftp
echo "quit" >> ipid_moodys.ftp

echo "Retrieving data file from Moodys FTP server" 
ftp -n -i -s:ipid_moodys.ftp ${FTPserver} 
if [ $? != 0 ]
then
	echo "FTP process failed - exiting" 
	exit 1
fi
echo "Retrieved data file" 

# Ensure output file exists

if [ ! -e $getFilename ]
then
	echo "Data file not found - exiting" 
	exit 1
fi

# Remove first character of each line

echo "Removing first character of each line" 
cut -c2- ${getFilename} > ${editFilename}

# Drop the old ipid table

echo "Connecting to SQL server and dropping ${IPIDtable}" 
isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} << ENDSQL 
drop table ${IPIDtable}_backup
go
exec sp_rename ${IPIDtable}, ${IPIDtable}_backup
go
ENDSQL
if [ $? != 0 ]
then
	echo "Connecting to SQL server failed - exiting" 
	exit 1
fi

# Create the new ipid table

echo "Creating new IPID table ${IPIDtable}" 
isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} << ENDSQL 
create table ${IPIDtable} (
	matchkey varchar(12) not NULL,
	ticker varchar(10) NULL,
	name varchar(51) NULL,
	country varchar(51) NULL,
	broad_business_line varchar(51) NULL,
	specific_business_line varchar(51) NULL,
	specific_SIC varchar(12) NULL,
	specific_SIC_text varchar(51) NULL,
	broad_SIC varchar(11) NULL,
	broad_SIC_text varchar(51) NULL,
	industry varchar(51) NULL
)
GO
ENDSQL
if [ $? != 0 ]
then
	echo "Could not create table - exiting" 
	exit 1
fi

# Bulk copy programming ${editFilename} to ${IPIDtable}

echo "Bulk copy programming ${editFilename} to ${IPIDtable}" 
bcp ${IPIDtable} in ${editFilename} /U${IPIDlogin} /P${IPIDpassword} /S${IPIDserver} /f${formatFilename} /t /b 5000 
if [ $? != 0 ]
then
	echo "BCP not completed - exiting" 
	exit 1
fi
echo "Bulk copy completed" 
if [ $? != 0 ]
then
	echo "Could not complete bulk copy - exiting" 
	exit 1
fi

# Formatting countries to match Alacra countries

echo "Formatting countries to match Alacra countries" 
isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} << ENDSQL 
UPDATE ${IPIDtable}
SET country = REPLACE(country, 'ALDERNEY', 'UNITED KINGDOM')
GO
UPDATE ${IPIDtable}
SET country = REPLACE(country, 'BAHAMAS - OFF SHORE', 'BAHAMAS')
GO
UPDATE ${IPIDtable}
SET country = REPLACE(country, 'BAHRAIN - OFF SHORE', 'BAHRAIN')
GO
UPDATE ${IPIDtable}
SET country = REPLACE(country, 'BOSNIA AND HERZEGOVINA', 'BOSNIA & HERZEGOVINA')
GO
UPDATE ${IPIDtable}
SET country = REPLACE(country, 'BRITISH VIRGIN ISLANDS', 'VIRGIN ISLANDS')
GO
UPDATE ${IPIDtable}
SET country = REPLACE(country, 'CAYMAN ISLANDS - OFF SHORE', 'CAYMAN ISLANDS')
GO
UPDATE ${IPIDtable}
SET country = REPLACE(country, 'CHANNEL ISLANDS', 'UNITED KINGDOM')
GO
UPDATE ${IPIDtable}
SET country = REPLACE(country, 'IRELAND', 'REPUBLIC OF IRELAND')
GO
UPDATE ${IPIDtable}
SET country = REPLACE(country, 'DUBLIN', 'REPUBLIC OF IRELAND')
GO
UPDATE ${IPIDtable}
SET country = REPLACE(country, 'GIBRALTAR', 'UNITED KINGDOM')
GO
UPDATE ${IPIDtable}
SET country = REPLACE(country, 'GUERNSEY', 'UNITED KINGDOM')
GO
UPDATE ${IPIDtable}
SET country = REPLACE(country, 'ISLE OF MAN', 'UNITED KINGDOM')
GO
UPDATE ${IPIDtable}
SET country = REPLACE(country, 'JERSEY', 'UNITED KINGDOM')
GO
UPDATE ${IPIDtable}
SET country = REPLACE(country, 'KOREA', 'SOUTH KOREA')
GO
UPDATE ${IPIDtable}
SET country = REPLACE(country, 'PANAMA - OFF SHORE', 'PANAMA')
GO
UPDATE ${IPIDtable}
SET country = REPLACE(country, 'SAINT LUCIA', 'ST LUCIA')
GO
UPDATE ${IPIDtable}
SET country = REPLACE(country, 'SARK', 'UNITED KINGDOM')
GO
UPDATE ${IPIDtable}
SET country = REPLACE(country, 'SLOVAK REPUBLIC', 'SLOVAKIA')
GO
ENDSQL
echo "Countries formatted" 
if [ $? != 0 ]
then
	echo "Problem formatting countries" 
	exit 1
fi

# Remove leading zeroes

echo "Removing leading zeroes" 
isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} << ENDSQL 
UPDATE ${IPIDtable}
SET matchkey = REPLACE(LTRIM(REPLACE(RTRIM(matchkey), '0', ' ')), ' ', '0')
GO
ENDSQL
if [ $? != 0 ]
then
	echo "Leading zeroes not removed - exiting" 
	exit 1
fi

# Create indices

echo "Creating indices" 
isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} << ENDSQL 
create index ${IPIDtable}_01 on ${IPIDtable}
	(matchkey)
GO
create index ${IPIDtable}_02 on ${IPIDtable}
	(ticker)
GO
create index ${IPIDtable}_03 on ${IPIDtable}
	(name)
GO
create index ${IPIDtable}_04 on ${IPIDtable}
	(country)
GO
ENDSQL
if [ $? != 0 ]
then
	echo "Indices not created - exiting" 
	exit 1
fi

# Clean up

echo "Cleaning up" 
#rm ${getFilename}
#rm ${editFilename}

echo "Done" 
