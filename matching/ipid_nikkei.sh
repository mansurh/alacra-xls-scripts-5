# Script file to build the IP matching table for Investext
# 1 = XLS Server
# 2 = XLS Login
# 3 = XLS Password
# 4 = nikkei Server
# 5 = nikkei Login
# 6 = nikkei Password
if [ $# -lt 6 ]
then
	echo "Usage: ipid_nikkei.sh xls_server xls_login xls_password nikkei_server nikkei_login nikkei_password"
	exit 1
fi

xlsserver=$1
xlslogin=$2
xlspassword=$3
nikkeiserver=$4
nikkeilogin=$5
nikkeipassword=$6

ipname=nikkei

# Name of the temp file to use
TMPFILE1=ipid_${ipname}1.tmp
TMPFILE2=ipid_${ipname}2.tmp

# Name of the table to use
TABLENAME=ipid_${ipname}

# Name of the format file to use
FORMAT_FILE=$XLS/src/scripts/matching/ipid_${ipname}.fmt

# Step 1 - remove any old temp files
rm -f ${TMPFILE1} ${TMPFILE2}

# Step 2 - select the nikkei data into a temporary file
isql -S${nikkeiserver} -U${nikkeilogin} -P${nikkeipassword} -s"|" -n -w500 -h-1 >${TMPFILE1} << HERE
SET NOCOUNT ON
select 	
	distinct z.id, z.securityCode, z.name
from
	nikkei..zokut z
HERE

# Step 3 - post-process the temp file
sed -f match.sed < ${TMPFILE1} > ${TMPFILE2}

# Step 4 - drop the old id table - don't check for error
# as it may not exist
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 5 - create the new table
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
create table ${TABLENAME} (
	id int NULL,
	securityCode int NULL,
	name varchar(90) NULL
)
GO
create index ${TABLENAME}_01 on ${TABLENAME}
	(id)
GO
create index ${TABLENAME}_02 on ${TABLENAME}
	(securityCode)
GO
create index ${TABLENAME}_03 on ${TABLENAME}
	(name)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 6 - bcp in the select results
bcp ${TABLENAME} in ${TMPFILE2} /S${xlsserver} /U${xlslogin} /P${xlspassword} /f${FORMAT_FILE} /b100
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi

# Step 7 - clean up
#rm -f ${TMPFILE1} ${TMPFILE2}
