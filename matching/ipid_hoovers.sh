# Script file to build the IP matching table for barra
# 1 = XLS Server
# 2 = XLS Login
# 3 = XLS Password
if [ $# -ne 5 ]
then
	echo "Usage: ipid_barra.sh xls_server xls_login xls_password ipname ipid_file"
	exit 1
fi

xlsserver=$1
xlslogin=$2
xlspassword=$3
ipidfile=$5
ipname=$4

# Name of the table to use
TABLENAME=ipid_${ipname}

# Nam

# Name of the format file to use
FORMAT_FILE=${XLS}/src/scripts/matching/ipid_hoovers.fmt

# Step 4 - drop the old id table - don't check for error
# as it may not exist
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 5 - create the new table
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
create table ${TABLENAME} (
	matchkey varchar(35) NULL,
	name varchar(255) NULL,
        type varchar(255) NULL,
        country varchar(64) NULL,
        us_ticker varchar(10) NULL,
        us_exchange varchar(20) NULL,
        foreign_ticker varchar(10) NULL,
        foreign_exchange varchar(20) NULL,
        website varchar(255) NULL
)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 6 - bcp in the select results
bcp ${TABLENAME} in ${ipidfile} /S${xlsserver} /U${xlslogin} /P${xlspassword} /f${FORMAT_FILE} /b100
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi

isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
create index ${TABLENAME}_02 on ${TABLENAME}
	(matchkey)
GO
create index ${TABLENAME}_03 on ${TABLENAME}
	(name)
GO
HERE

