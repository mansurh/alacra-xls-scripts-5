# Script file to generate the IP matching reports for sharew
# 1 = Output filename
# 2 = XLS Server
# 3 = XLS Login
# 4 = XLS Password
if [ $# -lt 4 ]
then
	echo "Usage: ipid_sharew_report.sh reportfile xls_server xls_login xls_password"
	exit 1
fi

REPORTFILE=$1
xlsserver=$2
xlslogin=$3
xlspassword=$4

ipname=sharew

# Name of the table to use
TABLENAME=ipid_${ipname}

isql -S${xlsserver} -U${xlslogin} -P${xlspassword} -s"|" -n -w1000 -h0 >${REPORTFILE} << HERE
SET NOCOUNT ON

/* Required Report - New companies to appear in database */
PRINT "Potentially new companies in sharew"
select i.cusipcins_id, i.name, i.ticker, i.exchange from ipid_sharew i
where i.cusipcins_id not in (
  select sourcekey from security_map where source = 1
)
and i.ticker is not NULL

/* Required Report - Companies which have disappeared from database */
PRINT "Matched companies that have disappeared from sharew"
select s.issuer, rtrim(c.name) + ' - ' + s.name , sm.id, s.cusipcins, s.ticker, s.exchange 
from security_map sm, security s, company c
where sm.source = 1
and sm.id = s.id
and s.issuer *= c.id
and sm.sourcekey not in
(
  select cusipcins_id from ipid_sharew
)
order by c.name

/* Optional report - new information available in database */

/* Optional report - companies with name changes.  This is only */
/* valid for Finex where naming methodologies are similar. */

HERE
