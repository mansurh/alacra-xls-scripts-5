# Parse the command line arguments.
# 1 = IPID Server
# 2 = IPID Database
# 3 = IPID Login
# 4 = IPID Password
# 5 = hemscott Server
# 6 = hemscott Login
# 7 = hemscott Password
ARGS=7
if [ $# -ne $ARGS ]
then
	echo "Usage: ipid_hemscott.sh IPID_server IPID_database IPID_login IPID_password hemscott_server hemscott_login hemscott_password"
	exit 1
fi


# Save the runstring parameters in local variables
IPID_server=$1
IPID_database=$2
IPID_logon=$3
IPID_password=$4
hemscott_server=$5
hemscott_logon=$6
hemscott_password=$7


# XLSDATA=C:/users/xls/tmp
LOADDIR=${XLS}/src/scripts/loading/abi
TEMPDIR=${XLSDATA}/abi/matching

# ipid_hemscott table has been populated by the loading script: see hemscott/update_local.sh

exit 0
 
