IF EXISTS (SELECT * FROM sysobjects WHERE name='sp_CreateDirectMappingTable' and type='P')
DROP procedure sp_CreateDirectMappingTable
go


create procedure sp_CreateDirectMappingTable
as
	select substring(bvdnumber,3,8),1,bvdnumber 
	from company 
	where bvdnumber like 'GB%' 
	union
	select 'E0' + substring(bvdnumber,3,6),1,bvdnumber 
	from company 
	where bvdnumber like 'IE%' 
go