IF EXISTS (SELECT * FROM sysobjects WHERE name='sp_CreateDirectMappingTable' and type='P')
DROP procedure sp_CreateDirectMappingTable
go


create procedure sp_CreateDirectMappingTable as
	select companyid, 1, companyid from company where companyid not like 'IR%' and name is not null
	union
	select 'IE'+subtring(companyid,3,6), 1, companyid from company where companyid like 'IR%' and name is not null
go