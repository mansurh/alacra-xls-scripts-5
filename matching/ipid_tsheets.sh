# Parse the command line arguments.
# 1 = IPID Server
# 2 = IPID Server
# 3 = IPID Login
# 4 = IPID Password
# 5 = sp Server
# 6 = sp Login
# 7 = sp Password
ARGS=7
if [ $# -ne $ARGS ]
then
	echo "Usage: ipid_tsheets.sh ipid_server ipid_database ipid_login ipid_password sp_server sp_login sp_password"
	exit 1
fi


# Save the runstring parameters in local variables
IPID_server=$1
IPID_database=$2
IPID_logon=$3
IPID_password=$4
sp_server=$5
sp_logon=$6
sp_password=$7


LOADDIR=${XLS}/src/scripts/loading/tsheets
TEMPDIR=${XLSDATA}/matching/tsheets
mkdir -p ${TEMPDIR}
cd ${TEMPDIR}


#
# Put new data in a batch file.
#

echo ""
echo "Building bulk load file from companies table from ${sp_server}"
echo ""

#isql -U ${sp_logon} -P ${sp_password} -S ${sp_server} -r -Q "set nocount on select distinct cusip + '|' + name + '|' + cusip + '|' + ticker + '|' + exchange + '|??' from tsheets.dbo.companies" > ${TEMPDIR}/ipid_tsheets.dat.temp1
bcp "select distinct cusip, name, cusip, ticker, exchange , '??' from tsheets.dbo.companies" queryout ${TEMPDIR}/ipid_tsheets.dat.temp1 -U ${sp_logon} -P ${sp_password} -S ${sp_server} /c /t"|"

#tail +5 < ${TEMPDIR}/ipid_tsheets.dat.temp1 | sed -f ${LOADDIR}/ipid_tsheets.sed > ${TEMPDIR}/ipid_tsheets.dat.temp2
sed -f ${LOADDIR}/ipid_tsheets.sed ${TEMPDIR}/ipid_tsheets.dat.temp1 > ${TEMPDIR}/ipid_tsheets.dat.temp2
#while read -r line
#do
#
#    line=${line%% }
#
#    if [ "${line}" != '' ]
#    then
#
#        print ${line}
#
#    fi
#
#done < ${TEMPDIR}/ipid_tsheets.dat.temp2 > ${TEMPDIR}/ipid_tsheets.dat
cp ${TEMPDIR}/ipid_tsheets.dat.temp2 ${TEMPDIR}/ipid_tsheets.dat
#
# Drop old data from xls table ipid_tsheets
#

echo ""
echo "Dropping old ipid_tsheets data on ${IPID_server}"
echo ""

isql /U${IPID_logon} /P${IPID_password} /S${IPID_server} < ${LOADDIR}/create_ipid_tsheets.sql


# drop the indices for the table
echo ""
echo "Dropping indices for ipid_tsheets for faster loading"
echo ""

isql /U${IPID_logon} /P${IPID_password} /S${IPID_server} < ${LOADDIR}/dropindex_ipid_tsheets.sql


#
# Load all the new data...
#

DATFILE="${TEMPDIR}/ipid_tsheets.dat"

# Load the ipid_tsheets table
echo "Loading the ipid_tsheets table" 

bcp ${IPID_database}.dbo.ipid_tsheets in ${DATFILE} /U${IPID_logon} /P${IPID_password} /S${IPID_server} /f${LOADDIR}/ipid_tsheets.fmt /e${TEMPDIR}/ipid_tsheets.log
if [ $? != 0 ]
then

	echo "Error loading ipid_tsheets table"
	exit 1

fi

# Rebuild the indices for the table
echo ""
echo "Rebuilding indices for ipid_tsheets for faster loading"
echo ""

isql /U${IPID_logon} /P${IPID_password} /S${IPID_server} < ${LOADDIR}/createindex_ipid_tsheets.sql

echo ""
echo "Updated company matching for S&P OK."
echo ""

# Now build an email to send to company matching
blatfile="${TEMPDIR}/ipid_tsheets.txt"
blatfiletmp1=${blatfile}.tmp1
blatfiletmp2=${blatfile}.tmp2


echo ""
echo "Getting list of companies which were just added to ipid_tsheets"
echo ""

echo "*** ipid_tsheets update info: *** " > ${blatfile}
echo "" >> ${blatfile}
echo "*** Companies added: ***" >> ${blatfile}
echo "" >> ${blatfile}
#isql -U ${IPID_logon} -P ${IPID_password} -S ${IPID_server} -r -Q "set nocount on select distinct matchkey + '|' + name + '|' + cusip + '|' + ticker + '|' + exchange  + '|??' from ipid_tsheets where matchkey not in (select distinct sourcekey from company_map where source=9950)" > ${blatfiletmp1}
bcp "select distinct matchkey, name, cusip, ticker, exchange, '??' from ipid_tsheets where matchkey not in (select distinct sourcekey from company_map where source=9950)" queryout ${blatfiletmp1} -U ${IPID_logon} -P ${IPID_password} -S ${IPID_server} /c /t"|"
#tail +5 < ${blatfiletmp1} | sed -f ${LOADDIR}/ipid_tsheets.sed > ${blatfiletmp2}
sed -f ${LOADDIR}/ipid_tsheets.sed ${blatfiletmp1} > ${blatfiletmp2}

#while read -r line
#do
#
#    line=${line%% }
#
#    if [ "${line}" != '' ]
#    then
#
#        print "${line}"
#
#    fi
#
#done < ${blatfiletmp2} >> ${blatfile}


echo ""
echo "Get list of companies which were just deleted from ipid_tsheets"
echo ""

echo "" >> ${blatfile}
echo "*** Companies deleted: ***" >> ${blatfile}
echo "" >> ${blatfile}
#isql -U ${IPID_logon} -P ${IPID_password} -S ${IPID_server} -s"|" -r -x 255 -w 255 -Q "set nocount on select distinct m.sourcekey + '|' + convert(varchar(255), m.xlsid) + '|' + c.name + '|' + cou.name + '|' + s.cusipcins + '|' + ltrim(rtrim(s.ticker)) + '|' + e.name from company_map m, company c, country cou, security s, exchange e where source=9950 and c.id=m.xlsid and cou.id=c.country and s.cusipcins=m.sourcekey and s.exchange=e.id and ''!=isnull(m.sourcekey, '') and sourcekey not in (select distinct matchkey from ipid_tsheets)" > ${blatfiletmp1}
bcp "select distinct m.sourcekey, convert(varchar(255), m.xlsid), c.name, cou.name, s.cusipcins, ltrim(rtrim(s.ticker)), e.name from company_map m, company c, country cou, security s, exchange e where source=9950 and c.id=m.xlsid and cou.id=c.country and s.cusipcins=m.sourcekey and s.exchange=e.id and ''!=isnull(m.sourcekey, '') and sourcekey not in (select distinct matchkey from ipid_tsheets)" queryout ${blatfiletmp1} -U ${IPID_logon} -P ${IPID_password} -S ${IPID_server} /c /t"|"
#tail +5 < ${blatfiletmp1} | sed -f ${LOADDIR}/ipid_tsheets.sed > ${blatfiletmp2}
sed -f ${LOADDIR}/ipid_tsheets.sed ${blatfiletmp1} > ${blatfiletmp2}

#while read -r line
#do
#
#    line=${line%% }
#
#    if [ "${line}" != '' ]
#    then
#
#        print "${line}"
#    fi
#done < ${blatfiletmp2} >> ${blatfile}
cat ${blatfiletmp2} >> ${blatfile}

echo ""
echo "sending matching info to companymatch@alacra.com"
echo ""

yr=`date +%Y`
mo=`date +%b`
dy=`date +%d`
dt_today="${yr}-${mo}-${dy}"
subject="Contents of file: S&P-${dt_today}.rpt"

blat ${blatfile} -t "companymatch@alacra.com" -s "${subject}"

exit 0
