# Script file to build the IP matching table for Argus
# Modified by CduSaire Nov 23, 04 to use ISINs, not sedols as key
# 1 = IPID Server
# 2 = IPID Database
# 3 = IPID Login
# 4 = IPID Password
# 5 = Argus Server
# 6 = Argus Login
# 7 = Argus Password
set -x
ARGS=7
if [ $# -ne $ARGS ]
then
	echo "Usage: ipid_Argus.sh ipid_server ipid_database ipid_login ipid_password Argus_server Argus_login Argus_password"
	exit 1
fi

IPIDserver=$1
IPIDdatabase=$2
IPIDlogin=$3
IPIDpassword=$4
ipserver=$5
iplogin=$6
ippassword=$7

ipname=Argus

# Name of the temp file to use
TMPFILE1=ipid_${ipname}1.tmp
TMPFILE2=ipid_${ipname}2.tmp

# Name of the table to use
TABLENAME=ipid_${ipname}

# Name of the format file to use
FORMAT_FILE=ipid_${ipname}.fmt

# Step 1 - remove any old temp files
rm -f ${TMPFILE1} ${TMPFILE2}

# Step 2 - select the ${ipname} data into a temporary file
isql -S${ipserver} -U${iplogin} -P${ippassword} -s"|" -w500 -n -h-1 >${TMPFILE1} << ENDOfIsql
SET NOCOUNT ON
select id, name from plc
ENDOfIsql

# Step 3 - post-process the temp file
sed -f ${XLS}/src/scripts/matching/match.sed < ${TMPFILE1} > ${TMPFILE2}

# Step 4 - drop the old id table - don't check for error
# as it may not exist
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 5 - create the new table
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
create table ${TABLENAME} (
	id char(12) NOT NULL,
	name char(90) NOT NULL
)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 6 - bcp in the select results
bcp ${TABLENAME} in ${TMPFILE2} -S ${IPIDserver} -U ${IPIDlogin} -P ${IPIDpassword} -f ${XLS}/src/scripts/matching/${FORMAT_FILE} -b 100
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi

# Step 7 - index the table
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE2
create index ${TABLENAME}_02 on ${TABLENAME}
	(name)
GO
HERE2

if [ $? -ne 0 ]
then
	echo "Error indexing table, exiting"
	exit 1
fi


# Step 9 - clean up
#rm -f ${TMPFILE1} ${TMPFILE2}
