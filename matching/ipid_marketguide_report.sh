# Script file to generate the IP matching reports for finex
# 1 = Output filename
# 2 = IPID Server
# 3 = IPID Login
# 4 = IPID Password
if [ $# -lt 4 ]
then
	echo "Usage: ipid_sdc_report.sh reportfile xls_server xls_login xls_password"
	exit 1
fi

REPORTFILE=$1
IPIDserver=$2
IPIDlogin=$3
IPIDpassword=$4

ipname=marketguide

# Name of the table to use
TABLENAME=ipid_${ipname}

isql -S${IPIDserver} -U${IPIDlogin} -P${IPIDpassword} -s"|" -n -w1000 -h0 >${REPORTFILE} << HERE
SET NOCOUNT ON

/* Required Report - New companies to appear in database */
PRINT "Potentially new companies in MarketGuide"
select i.matchkey ,i.name from ipid_marketguide i
where i.matchkey not in (select sourcekey from security_map where source=9)



HERE
