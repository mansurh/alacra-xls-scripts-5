# Script file to generate the IP matching reports for mergent
#
# History:
#	07/31/01 - Created - MZ
#
# 1 = Output file name
# 2 = XLS Server
# 3 = XLS Login
# 4 = XLS Password

if [ $# -lt 4 ]
then
	echo "Usage: ipid_firstcall_report.sh outputfile, xls server, xls login, xls password"
	exit 1
fi

# Save the runstring parameters in local variables
REPORTFILE=$1
xlsserver=$2
xlslogin=$3
xlspassword=$4

cd ${XLS}/src/scripts/matching

if [ -s ${REPORTFILE} ]
then
	rm ${REPORTFILE}
fi

echo "about to run isql from ipid_firstcall_report.sh"

isql -S${xlsserver} -U${xlslogin} -P${xlspassword} -s"|" -n -w1000 -h0 >${REPORTFILE} << HERE
SET NOCOUNT ON

/* New companies */
PRINT "Potentially new companies in first call"
select matchKey,companyname, country from ipid_firstcall
where matchKey not in (select sourcekey from company_map where source = 9949)

/* Companies which have disappeared from database */
PRINT "Matched companies that have disappeared from firstcall"
select x.id, x.name, m.sourcekey from company x, company_map m
where x.id = m.xlsid and m.source = 9949 and m.sourcekey not in (select matchKey from ipid_firstcall)

HERE
