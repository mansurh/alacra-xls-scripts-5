# Script file to build the IP matching table for Mergerstat
# 1 = concordance Server
# 2 = concordance database
# 3 = concordance Login
# 4 = concordance Password
# 5 = Mergstat Server
# 6 = Mergstat Login
# 7 = Mergstat Password
if [ $# -lt 7 ]
then
	echo "Usage: ipid_Mergstat.sh xls_server xls_login xls_password Mergstat_server Mergstat_login Mergstat_password"
	exit 1
fi

xlsserver=$1
xlslogin=$3
xlspassword=$4
ipserver=$5
iplogin=$6
ippassword=$7

ipname=Mergstat

# Name of the temp file to use
TMPFILE1=ipid_${ipname}1.tmp
TMPFILE2=ipid_${ipname}2.tmp
TMPFILE3=ipid_${ipname}3.tmp

# Name of the table to use
TABLENAME=ipid_${ipname}

# Name of the format file to use
FORMAT_FILE=ipid_${ipname}.fmt

# Step 1 - remove any old temp files
rm -f ${TMPFILE1} ${TMPFILE2} ${TMPFILE3}

# Step 2 - select the ${ipname} data into a temporary file
# create records to the tempfile for pair of xlsid/cusip
#isql -S${xlsserver} /U${xlslogin} /P${xlspassword} -s"|" -w500 -n -h-1 >${TMPFILE3} << TUN1
#SET NOCOUNT ON
#select SUBSTRING(upper(s.cusipcins),1,6), min(c.id) from xls..company c, xls..security s where s.issuer = c.id
#and s.cusipcins is not null and s.cusipcins <>'' group by SUBSTRING(upper(s.cusipcins),1,6)
#union
#select SUBSTRING(upper(c.cusip6),1,6), (c.id) from xls..company c where c.cusip6 is not null and c.cusip6 <> ''
#TUN1

# drop cmap then recreate and bcp rows from the tempfile
#isql -S${ipserver} -U${iplogin} -P${ippassword} << TUN2
#drop table cmap
#GO
#create table cmap (
#	cusip char(9) NULL,
#	xlsid integer NULL
#)
#GO
#create index cmap_01 on cmap(cusip)
#GO
#TUN2

#bcp cmap in ${TMPFILE3} -S${ipserver} -U${iplogin} -P${ippassword} -f ${XLS}/src/scripts/matching/cmap.fmt -b 100
#if [ $? -ne 0 ]
#then
#	echo "Error in BCP, exiting"
#	exit 1
#fi

isql -S${ipserver} -U${iplogin} -P${ippassword} -s"|" -w500 -n -h-1 >${TMPFILE1} << TUN
SET NOCOUNT ON
select distinct (b.entry_no) as dealno, (m.xlsid) as xlsid, b.buyer_name as name, c.country_name as country, 
b.buyer_cusip, b.buyer_ticker, b.buyer_exchange, 'B' as buyerOrseller, 
d.percent_sought as percent_sought, (convert(char(6), b.b_sic1)) as sic 
from buyer b, deal d, country c, map m where b.entry_no=d.entry_no and upper(b.entry_no)*=m.entry_no and 
buyer_name is not null and c.country_init=*b.buyer_country
union
select distinct (s.entry_no) as dealno, (m.xlsid) as xlsid, s.seller_name as name, c.country_name as country, 
s.seller_cusip as cusip, s.seller_ticker as ticker, d.u_exchange as exchange,  'S' as buyerOrseller, 
d.percent_sought as percent_sought, (convert(char(6), s.seller_sic1)) as sic 
from seller s, deal d, country c, map m where d.entry_no=s.entry_no and upper(s.entry_no)*=m.entry_no and
s.seller_name is not null and c.country_init=*s.seller_country
TUN

# Step 3 - post-process the temp file
sed -f ${XLS}/src/scripts/matching/match.sed < ${TMPFILE1} > ${TMPFILE2}
# Step 4 - drop the old id table - don't check for error
# as it may not exist
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 5 - create the new table
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
create table ${TABLENAME} (
	dealno integer NULL,
	xlsid integer NULL,
	name varchar(128) NULL,
	country char(55) NULL,
	cusip varchar(9) NULL,
	ticker varchar(12) NULL,
	exchange varchar(12) NULL,
	buyerOrseller varchar(2) NULL,
	percent_sought dec(5,2) NULL,
	sic integer NULL
)
GO
create index ${TABLENAME}_01 on ${TABLENAME}
	(xlsid)
GO
create index ${TABLENAME}_02 on ${TABLENAME}
	(name)
GO
create index ${TABLENAME}_03 on ${TABLENAME}
	(country)
GO
create index ${TABLENAME}_04 on ${TABLENAME}
	(cusip)
GO
create index ${TABLENAME}_05 on ${TABLENAME}
	(ticker)
GO
create index ${TABLENAME}_06 on ${TABLENAME}
	(exchange)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 6 - bcp in the select results
bcp ${TABLENAME} in ${TMPFILE2} -S ${xlsserver} -U ${xlslogin} -P ${xlspassword} -f ${XLS}/src/scripts/matching/${FORMAT_FILE} -b 1000
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi

# step 7 - cross reference the exchange names
#isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
#update ipid_${ipname} set exchange=73 where exchange="NYSE"
#update ipid_${ipname} set exchange=3 where exchange="AMEX"
#update ipid_${ipname} set exchange=66 where exchange="NASD"
#update ipid_${ipname} set exchange=66 where exchange="OTC"
#update ipid_${ipname} set exchange=NULL where exchange not in (73,3,66)
#HERE

# step 8 - cross reference the country names
#isql /S${xlsserver} /U${xlslogin} /P${xlspassword} < ${XLS}/src/scripts/matching/ipid_${ipname}_country.sql

# Step 9 - clean up
#rm -f ${TMPFILE1} ${TMPFILE2} ${TMPFILE3}
