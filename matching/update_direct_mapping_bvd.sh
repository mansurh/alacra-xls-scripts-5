XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

cdserver=`get_xls_registry_value concordance Server`
cdlogin=`get_xls_registry_value concordance User`
cdpassword=`get_xls_registry_value concordance Password`
ipserver=`get_xls_registry_value bvd Server`
iplogin=`get_xls_registry_value bvd User`
ippassword=`get_xls_registry_value bvd Password`

timestamp="`date +%a' '%b' '%e'  '%T' '%Z' '%Y`"
echo "\n ********* ${timestamp}\n Start update ***********  \n"

workdir=$XLSDATA/bvd

# Name of the temp files to use
TMPZEPHYR=${workdir}/direct_mapping_zephyr.tmp
TMPFAME=${workdir}/direct_mapping_fame.tmp

echo "\n remove any old temp files\n"
rm -f ${TMPZEPHYR}
rm -f ${TMPFAME}

docopy=1
if [ "${docopy}" = 1 ]
then
timestamp="`date +%a' '%b' '%e'  '%T' '%Z' '%Y`"
echo "\n ********* ${timestamp}\n Start making temporary direct_mapping table ***********  \n"

echo "\n create copy of direct_mapping table to work with\n"
isql /S${cdserver} /U${cdlogin} /P${cdpassword} << HERE
select * into direct_mapping_new from direct_mapping
GO
HERE

timestamp="`date +%a' '%b' '%e'  '%T' '%Z' '%Y`"
echo "\n ********* ${timestamp}\n Start creating indexes ***********  \n"

echo "\n creating indexes for temporary direct_mapping table\n"
isql /S${cdserver} /U${cdlogin} /P${cdpassword} << HERE
create index direct_mapping01 on direct_mapping_new
    ( direct_mapping_type,  direct_mapping_value)
go
create index direct_mapping02 on direct_mapping_new
    ( source,  sourcekey)
go
HERE

fi

timestamp="`date +%a' '%b' '%e'  '%T' '%Z' '%Y`"
echo "\n ********* ${timestamp}\n Start deleting records from temporary direct_mapping ***********  \n"

echo "\n delete records from temporary direct_mapping"
isql /S${cdserver} /U${cdlogin} /P${cdpassword} << HERE
delete from direct_mapping_new where direct_mapping_new.source in (235,266)
GO
HERE


echo "\n deleting indexes to make bcp faster\n"
isql /S${cdserver} /U${cdlogin} /P${cdpassword} << HERE
drop index direct_mapping_new.direct_mapping01
go
drop index direct_mapping_new.direct_mapping02
go
HERE

##############################################

timestamp="`date +%a' '%b' '%e'  '%T' '%Z' '%Y`"
echo "\n ********* ${timestamp}\n Start creating temporary direct_mapping tables on bvd ********** \n"

echo "\n delete temporary zephyr_mapping table if it exists\n"
isql /S${ipserver} /U${iplogin} /P${ippassword} << HERE
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id('dbo.zephyr_mapping'))
BEGIN
   DROP table dbo.zephyr_mapping
END
GO
HERE

echo "\n delete temporary fame_mapping table if it exists\n"
isql /S${ipserver} /U${iplogin} /P${ippassword} << HERE
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id('dbo.fame_mapping'))
BEGIN
   DROP table dbo.fame_mapping
END
GO
HERE

echo "\n creating temporary zephyr_mapping table\n"
isql /S${ipserver} /U${iplogin} /P${ippassword} << HERE
select 235 as source, f.bvd_id as sourcekey, 1 as type, f.regd_no as value
into zephyr_mapping from company_fame f inner join zephyr_bvdid z on z.bvd_id = f.bvd_id order by f.bvd_id
GO
HERE

echo "\n creating temporary fame_mapping table\n"
isql /S${ipserver} /U${iplogin} /P${ippassword} << HERE
select 266 as source, regd_no as sourcekey, 1 as type, regd_no as value into fame_mapping from company_fame order by regd_no
GO
HERE

timestamp="`date +%a' '%b' '%e'  '%T' '%Z' '%Y`"
echo "\n ********* ${timestamp}\n Start creating temporary bcp files ********** \n"

echo "\n bcp data from zephyr_mapping table from bvd database to a temporary file"
bcp zephyr_mapping out ${TMPZEPHYR} -S ${ipserver} -U ${iplogin} -P ${ippassword}  /c /t"|" /ezephyr_mapping.err /m1000
if [ $? -ne 0 ]
then
	echo "\nError in bcp zephyr_mapping out, exiting\n"
	exit 1
fi

if [ ! -s ${TMPZEPHYR} ]
then
	echo "\n*** ${TMPZEPHYR} data file is empty. Exiting ***\n"
	exit 1
fi


echo "\n bcp data from fame_mapping table from bvd database to a temporary file"
bcp fame_mapping out ${TMPFAME} -S ${ipserver} -U ${iplogin} -P ${ippassword}  /c /t"|" /efame_mapping.err /m1000
if [ $? -ne 0 ]
then
	echo "\nError in bcp fame_mapping out, exiting\n"
	exit 1
fi

if [ ! -s ${TMPFAME} ]
then
	echo "\n*** ${TMPFAME} data file is empty. Exiting ***\n"
	exit 1
fi


echo "\n delete temporary zephyr_mapping table \n"
isql /S${ipserver} /U${iplogin} /P${ippassword} << HERE
DROP table dbo.zephyr_mapping
GO
HERE

echo "\n delete temporary fame_mapping table \n"
isql /S${ipserver} /U${iplogin} /P${ippassword} << HERE
DROP table dbo.fame_mapping
GO
HERE


timestamp="`date +%a' '%b' '%e'  '%T' '%Z' '%Y`"
echo "\n ********* ${timestamp}\n Start loading direct_mapping data ********** \n"

echo "\n bcp data from zephyr_mapping.txt into direct_mapping_new \n"
bcp direct_mapping_new in ${TMPZEPHYR} -S ${cdserver} -U ${cdlogin} -P ${cdpassword}  /c /t"|" /ezephyr_mapping.err /m1000
if [ $? -ne 0 ]
then
	echo "\nError in bcp zephyr_mapping in, exiting\n"
	exit 1
fi

echo "\n bcp data from fame_mapping.txt into direct_mapping_new \n"
bcp direct_mapping_new in ${TMPFAME} -S ${cdserver} -U ${cdlogin} -P ${cdpassword}  /c /t"|" /efame_mapping.err /m1000
if [ $? -ne 0 ]
then
	echo "\nError in bcp fame_mapping in, exiting\n"
	exit 1
fi

timestamp="`date +%a' '%b' '%e'  '%T' '%Z' '%Y`"
echo "\n ********* ${timestamp}\n Start creating indexes ***********  \n"

echo "\n creating indexes for temporary direct_mapping table\n"
isql /S${cdserver} /U${cdlogin} /P${cdpassword} << HERE
create index direct_mapping01 on direct_mapping_new
    ( direct_mapping_type,  direct_mapping_value)
go
create index direct_mapping02 on direct_mapping_new
    ( source,  sourcekey)
go
HERE



timestamp="`date +%a' '%b' '%e'  '%T' '%Z' '%Y`"
echo "\n ********* ${timestamp}\n Start renaming direct_mapping_new table into direct_mapping ***********  \n"
isql /S${cdserver} /U${cdlogin} /P${cdpassword} << HERE
exec commit_concordance_copy direct_mapping
GO
HERE

if [ $? -ne 0 ]
then
	echo "Unable to rename direct_mapping_new table into direct_mapping, exiting"
	exit 1
fi

timestamp="`date +%a' '%b' '%e'  '%T' '%Z' '%Y`"
echo "\n ********* ${timestamp}\n delete temporary direct_mapping table ***********  \n"
isql /S${cdserver} /U${cdlogin} /P${cdpassword} << HERE
drop table direct_mapping_old
GO
HERE


##############################################

timestamp="`date +%a' '%b' '%e'  '%T' '%Z' '%Y`"
echo "\n ********* ${timestamp}\n update of direct_mapping table for bvd has finished. ***********  \n"

exit 0

