XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Generic Script file to build the IP matching table 
# 1 = IPID Server
# 2 = IPID Database
# 3 = IPID Login
# 4 = IPID Password
# 5 = IPID Table Name
ARGS=5
if [ $# -ne $ARGS ]
then
	echo "Usage: $0 ipid_server ipid_database ipid_login ipid_password ipid_name"
	exit 1
fi

IPIDserver=$1
IPIDdatabase=$2
IPIDlogin=$3
IPIDpassword=$4
ipname=$5
TABNAME=ipid_${ipname}
TMPFILE=${TABNAME}.tmp
rm -f *.tmp
rm -f *.txt

bcp "select top 1 regkey, query from dmo_dbAvailable where dbname='${ipname}'" queryout ${TABNAME}.txt /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /c /t"|" 
if [ $? -ne 0 ]
then
	echo "error in getting info from ${IPIDdatabase} on ${IPIDserver}, exiting ..."
	exit 1
fi

srcdatabase=""
query=""
IFS="|"
while read col1 col2 
do
   srcdatabase=${col1}
   query=${col2}
done < ${TABNAME}.txt

if [ -z ${srcdatabase} ] || [ -z ${query} ]
then
	echo "Could not retrieve dbname or query from dmo_dbAvailable table for ${ipname}, exiting ..."
	exit 1
fi

ipserver=`get_xls_registry_value ${srcdatabase} Server | cut -d ";" -f1`
iplogin=`get_xls_registry_value ${srcdatabase} User`
ippassword=`get_xls_registry_value ${srcdatabase} Password`

bcp "${query}" queryout ${TMPFILE} /S${ipserver} /U${iplogin} /P${ippassword}  /c
if [ $? -ne 0 ]
then
	echo "error running ${ipname} query against ${ipserver}, exiting ..."
	exit 1
fi

isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /b /Q "drop table ${TABNAME}_new"
echo $?
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /b /Q "select top 0 * into ${TABNAME}_new from ${TABNAME}"
if [ $? -ne 0 ]
then
	echo "error creating ${TABNAME}_new table, exiting ..."
	exit 1
fi

bcp ${TABNAME}_new in ${TMPFILE} /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /c /e${TABNAME}.err
if [ $? -ne 0 ]
then
	echo "error copying data into ${TABNAME}_new table, exiting ..."
	exit 1
fi

typeset -i filecount=`wc -l ${TMPFILE} |cut -f1 -d" "`
$XLS/src/scripts/matching/check_ipid_row_counts.sh ${IPIDserver} ${IPIDlogin} ${IPIDpassword} ${TABNAME} ${filecount}
if [ $? -ne 0 ]
then
	echo "Counts check failed, exiting ..."
	exit 1
fi

$XLS/src/scripts/utility/create_index_sql.sh ${IPIDserver} ${IPIDlogin} ${IPIDpassword} ${TABNAME} ${TABNAME}_index.sql
if [ $? -ne 0 ]
then
	echo "error in $XLS/src/scripts/utility/create_index_sql.sh, exiting ..."
	exit 1
fi

isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /b < ${TABNAME}_index.sql
if [ $? -ne 0 ]
then
	echo "error creating indexes on ${TABNAME}_new table, exiting ..."
	exit 1
fi

isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /b /Q "drop table ${TABNAME}_bak"

isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /b /Q "exec sp_rename '${TABNAME}', '${TABNAME}_bak'; exec sp_rename '${TABNAME}_new', '${TABNAME}'"
if [ $? -ne 0 ]
then
	echo "error renaming ${ipname}_new table, exiting ..."
	exit 1
fi

if [ -s ${XLS}/src/scripts/matching/${TABNAME}.sql ]
then
	isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /b < ${XLS}/src/scripts/matching/${TABNAME}.sql
	if [ $? -ne 0 ]
	then
		echo "error in ${XLS}/src/scripts/matching/${TABNAME}.sql, exiting ..."
		exit 1
	fi
fi
