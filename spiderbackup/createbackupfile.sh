# Create the back file for Inverted Files

if [ $# -lt 1 ]
then
	echo "Usage: createbackupfile.sh indexLocation"
	exit 1
fi

# Save the runstring parameters in local variables
indexloc=$1

backupfilename="backup.txt"

#delete any old backup files
rm -f ${backupfilename}

echo "/disk1/${indexloc}/englishindex0.pst" > ${backupfilename}
echo "/disk1/${indexloc}/englishindex1.pst" >> ${backupfilename}
echo "/disk1/${indexloc}/englishindex2.pst" >> ${backupfilename}
echo "/disk1/${indexloc}/englishindex3.pst" >> ${backupfilename}
echo "/disk1/${indexloc}/englishindex4.pst" >> ${backupfilename}
echo "/disk1/${indexloc}/englishindex5.pst" >> ${backupfilename}
echo "/disk1/${indexloc}/englishindex6.pst" >> ${backupfilename}
echo "/disk1/${indexloc}/englishindex7.pst" >> ${backupfilename}
echo "/disk1/${indexloc}/frenchindex0.pst" >> ${backupfilename}
echo "/disk1/${indexloc}/frenchindex1.pst" >> ${backupfilename}
echo "/disk1/${indexloc}/spanishindex0.pst" >> ${backupfilename}
echo "/disk2/${indexloc}/englishindex8.pst" >> ${backupfilename}
echo "/disk2/${indexloc}/englishindex9.pst" >> ${backupfilename}
echo "/disk2/${indexloc}/englishindexA.pst" >> ${backupfilename}
echo "/disk2/${indexloc}/englishindexB.pst" >> ${backupfilename}
echo "/disk2/${indexloc}/englishindexC.pst" >> ${backupfilename}
echo "/disk2/${indexloc}/englishindexD.pst" >> ${backupfilename}
echo "/disk2/${indexloc}/englishindexE.pst" >> ${backupfilename}
echo "/disk2/${indexloc}/englishindexF.pst" >> ${backupfilename}
echo "/disk2/${indexloc}/frenchindex2.pst" >> ${backupfilename}
echo "/disk2/${indexloc}/frenchindex3.pst" >> ${backupfilename}
echo "/disk2/${indexloc}/spanishindex1.pst" >> ${backupfilename}
echo "/disk3/${indexloc}/english.cfg" >> ${backupfilename}
echo "/disk3/${indexloc}/englishindex.doc" >> ${backupfilename}
echo "/disk3/${indexloc}/englishindex.idx" >> ${backupfilename}
echo "/disk3/${indexloc}/englishindex10.pst" >> ${backupfilename}
echo "/disk3/${indexloc}/englishindex11.pst" >> ${backupfilename}
echo "/disk3/${indexloc}/englishindex12.pst" >> ${backupfilename}
echo "/disk3/${indexloc}/englishindex13.pst" >> ${backupfilename}
echo "/disk3/${indexloc}/englishindex14.pst" >> ${backupfilename}
echo "/disk3/${indexloc}/englishindex15.pst" >> ${backupfilename}
echo "/disk3/${indexloc}/englishindex16.pst" >> ${backupfilename}
echo "/disk3/${indexloc}/englishindex17.pst" >> ${backupfilename}
echo "/disk3/${indexloc}/englishsiter.log" >> ${backupfilename}
echo "/disk3/${indexloc}/french.cfg" >> ${backupfilename}
echo "/disk3/${indexloc}/frenchindex.doc" >> ${backupfilename}
echo "/disk3/${indexloc}/frenchindex.idx" >> ${backupfilename}
echo "/disk3/${indexloc}/frenchsiter.log" >> ${backupfilename}
echo "/disk3/${indexloc}/sitelist" >> ${backupfilename}
echo "/disk3/${indexloc}/spanish.cfg" >> ${backupfilename}
echo "/disk3/${indexloc}/spanishindex.doc" >> ${backupfilename}
echo "/disk3/${indexloc}/spanishindex.idx" >> ${backupfilename}
echo "/disk3/${indexloc}/spanishsiter.log" >> ${backupfilename}
echo "/mnt1/${indexloc}/englishindex0.pst" >> ${backupfilename}
echo "/mnt1/${indexloc}/englishindex1.pst" >> ${backupfilename}
echo "/mnt1/${indexloc}/englishindex2.pst" >> ${backupfilename}
echo "/mnt1/${indexloc}/englishindex3.pst" >> ${backupfilename}
echo "/mnt1/${indexloc}/englishindex4.pst" >> ${backupfilename}
echo "/mnt1/${indexloc}/englishindex5.pst" >> ${backupfilename}
echo "/mnt1/${indexloc}/englishindex6.pst" >> ${backupfilename}
echo "/mnt1/${indexloc}/englishindex7.pst" >> ${backupfilename}
echo "/mnt1/${indexloc}/germanindex0.pst" >> ${backupfilename}
echo "/mnt1/${indexloc}/germanindex1.pst" >> ${backupfilename}
echo "/mnt2/${indexloc}/englishindex8.pst" >> ${backupfilename}
echo "/mnt2/${indexloc}/englishindex9.pst" >> ${backupfilename}
echo "/mnt2/${indexloc}/englishindexA.pst" >> ${backupfilename}
echo "/mnt2/${indexloc}/englishindexB.pst" >> ${backupfilename}
echo "/mnt2/${indexloc}/englishindexC.pst" >> ${backupfilename}
echo "/mnt2/${indexloc}/englishindexD.pst" >> ${backupfilename}
echo "/mnt2/${indexloc}/englishindexE.pst" >> ${backupfilename}
echo "/mnt2/${indexloc}/englishindexF.pst" >> ${backupfilename}
echo "/mnt2/${indexloc}/germanindex2.pst" >> ${backupfilename}
echo "/mnt2/${indexloc}/germanindex3.pst" >> ${backupfilename}
echo "/mnt3/${indexloc}/english.cfg" >> ${backupfilename}
echo "/mnt3/${indexloc}/englishindex.doc" >> ${backupfilename}
echo "/mnt3/${indexloc}/englishindex.idx" >> ${backupfilename}
echo "/mnt3/${indexloc}/englishindex10.pst" >> ${backupfilename}
echo "/mnt3/${indexloc}/englishindex11.pst" >> ${backupfilename}
echo "/mnt3/${indexloc}/englishindex12.pst" >> ${backupfilename}
echo "/mnt3/${indexloc}/englishindex13.pst" >> ${backupfilename}
echo "/mnt3/${indexloc}/englishindex14.pst" >> ${backupfilename}
echo "/mnt3/${indexloc}/englishindex15.pst" >> ${backupfilename}
echo "/mnt3/${indexloc}/englishindex16.pst" >> ${backupfilename}
echo "/mnt3/${indexloc}/englishindex17.pst" >> ${backupfilename}
echo "/mnt3/${indexloc}/englishsiter.log" >> ${backupfilename}
echo "/mnt3/${indexloc}/german.cfg" >> ${backupfilename}
echo "/mnt3/${indexloc}/germanindex.doc" >> ${backupfilename}
echo "/mnt3/${indexloc}/germanindex.idx" >> ${backupfilename}
echo "/mnt3/${indexloc}/germansiter.log" >> ${backupfilename}
echo "/mnt3/${indexloc}/sitelist" >> ${backupfilename}
echo "/se51/${indexloc}/englishindex0.pst" >> ${backupfilename}
echo "/se51/${indexloc}/englishindex1.pst" >> ${backupfilename}
echo "/se51/${indexloc}/englishindex2.pst" >> ${backupfilename}
echo "/se51/${indexloc}/englishindex3.pst" >> ${backupfilename}
echo "/se51/${indexloc}/englishindex4.pst" >> ${backupfilename}
echo "/se51/${indexloc}/englishindex5.pst" >> ${backupfilename}
echo "/se51/${indexloc}/englishindex6.pst" >> ${backupfilename}
echo "/se51/${indexloc}/englishindex7.pst" >> ${backupfilename}
echo "/se52/${indexloc}/englishindex8.pst" >> ${backupfilename}
echo "/se52/${indexloc}/englishindex9.pst" >> ${backupfilename}
echo "/se52/${indexloc}/englishindexA.pst" >> ${backupfilename}
echo "/se52/${indexloc}/englishindexB.pst" >> ${backupfilename}
echo "/se52/${indexloc}/englishindexC.pst" >> ${backupfilename}
echo "/se52/${indexloc}/englishindexD.pst" >> ${backupfilename}
echo "/se52/${indexloc}/englishindexE.pst" >> ${backupfilename}
echo "/se52/${indexloc}/englishindexF.pst" >> ${backupfilename}
echo "/se53/${indexloc}/english.cfg" >> ${backupfilename}
echo "/se53/${indexloc}/englishindex.doc" >> ${backupfilename}
echo "/se53/${indexloc}/englishindex.idx" >> ${backupfilename}
echo "/se53/${indexloc}/englishindex10.pst" >> ${backupfilename}
echo "/se53/${indexloc}/englishindex11.pst" >> ${backupfilename}
echo "/se53/${indexloc}/englishindex12.pst" >> ${backupfilename}
echo "/se53/${indexloc}/englishindex13.pst" >> ${backupfilename}
echo "/se53/${indexloc}/englishindex14.pst" >> ${backupfilename}
echo "/se53/${indexloc}/englishindex15.pst" >> ${backupfilename}
echo "/se53/${indexloc}/englishindex16.pst" >> ${backupfilename}
echo "/se53/${indexloc}/englishindex17.pst" >> ${backupfilename}
echo "/se53/${indexloc}/englishsiter.log" >> ${backupfilename}
echo "/se53/${indexloc}/sitelist" >> ${backupfilename}
