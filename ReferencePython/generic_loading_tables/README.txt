Command Line: python3 ipid_loader.py origin destination --tablename 'my_table' --max_dev '80' --stage 'test'
		
		Argument Description:

			-ipid_loader.py ~ The python file that will run.

			-origin(required) ~ The directory in the Data Server section of the registry that contains the 
					    credentials for the server used by the select statement in the load file.

			-destination(optional) ~ The directory in the Data Server section of the registry that contains
					    the credentials for the server that the table will be created on. Default is concordance.

			-tablename(optional) - If you want to run this loader for a specific table that is in a directory with other tables
						you can specify the table name using this argument. This argument must be entered as follows...
						--tablename 'name_of_my_table'

			-max_dev(optional) - This argument is used to define the max deviation allowed between the new table and the table it
						is replacing. If this is not defined it will default to 20%, it can be defined as follows...
						--max_dev '100'

			-stage(optional) - This argument is used to define the stage you want to run the loader in (test or prod). By default 
						it will be prod, it can be defined as follows...
						--stage 'test'	or	--stage 'prod'



Required Files - 
	Note: All of these files should be in the c:/User/xls/src/scripts/ReferencePython/generic_loading_tables/arg_files/$origin$ directory.
		  If a directory with your $origin$ is not there you need to create the directory and put the files in it.
		  
	1. Create file: 
		-Naming scheme: create_ipid_clientName_tableDescription
		-Contents: The name and type of columns that the table will contain, i.e.
					id|int null
					lastname|varchar(400)
					address|varchar(200) not null
					city|varchar(100) null
					state|varchar(100) null
					country|char(200) not null
				   Each new row represents a new column, columnName|columnType
		
	2. Load file:
		-Naming scheme: load_ipid_clientName_tableDescription
		-Contents: The server you want the command to be run in ('origin' or 'destination')
				   and a sql statement that will select the table that is to be created.
				   If there are multiple commands to run seperate them with '<><>'
		-Example: origin|select id, master.dbo.anglicize(lastname), master.dbo.anglicize(countries) from loadtable where ei = 'e'
				  <><>
				  destination|update ipid_concordance_wcheck_new set countries = upper(name) from country where countries = country.iso3166_A3
				  <><>
			      destination|update ipid_concordance_wcheck_new set countries = null where countries = 'UNKNOWN'
				  <><>
				  destination|execute wcheck_python_proc

		
	3. Parse file:
		-Naming scheme: parse_ipid_clientName_tableDescription
		-Contents: If a column from the table that is selected using the statement in the load file 
				   must be parsed than you can use this file to do so...
				   Line 1: Column # that needs parsing
				   Line 2: Delimiter
				   Line 3: Which parts of the parsed column do you want?
				   
				   i.e.
				   Line 1: 3
				   Line 2: ;
				   Line 3: 0,1
				   
				   Table Columns[column #]: id[0], name[1], address[2], city[3]
				   Column # 3 Data: city;state;zipcode
				   
				   Because Line 1 contains '3' the system will choose column # 3 (city) to parse.
				   Since Line 2 contains ';' the system will set the delimiter to ';' and split the column by that character.
				   Lastly, on line 3 there is '0,1' this tells the system to take fields 0 and 1 when the column is split, in this case
				   it will be city as field 0 and state as field 1, if you wanted the zipcode you would have put 2 on line 3.


	4. Procedure file:
		-Naming scheme: procedure_ipid_clientName_tableDescription
		-Contents: In this file just put "exec" + the name of the procedure you want to execute
				   i.e. "exec my_procedure" 