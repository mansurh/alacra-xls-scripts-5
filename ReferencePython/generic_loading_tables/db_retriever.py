import pyodbc
import subprocess as sproc
from log_me import logger
import sys
import time
import os
import traceback

"""
"""
class DB_Retriever(object):
	database = None;
	conn = None;
	columns = None;
	myTable = None;
	id_to_content_type = None;
	creds = None;
	sql_command = None;
	loaddir = None;
	scriptdir = None;
	logging = None;
	
	""" 
	Initializes the DatabaseWorker with a given database name 
	"""
	def __init__(self, database, columns, sql_command, client_dir, logger):
		self.database = database
		self.columns = columns
		self.sql_command = sql_command
		self.scriptdir = 'c:/users/xls/src/scripts/ReferencePython/generic_loading_tables/'
		self.loaddir = os.environ.get('XLSDATA') + '/' 
		#initialize logging file name
		self.logging = logger
		
		
	""" 
	Provides registry data to help set the credentials
	Param: val - a string of the database and credential desired, example below 
	"""
	def get_registry_data_server(self, val, serverDir):
		# e.g. val like dba2/Password OR concordance/Server
		try:
			val = sproc.check_output("regtool get '/machine/software/xls/" + serverDir + "/{0}'".format(val), shell=True)
		except sproc.CalledProcessError:
			traceback.print_exc()
			self.logging.error('subprocess.CalledProcessError: Invalid server directory - check database|client|regDir names.')
		return val.decode().strip()
		
		
	""" 
	Returns the credentials for the database 
	"""
	def get_creds(self, serverDir):
		creds = {}
		try:
			val = self.database + '/Server'
			creds["dba_server"] = self.get_registry_data_server(val, serverDir)
			val = self.database + '/User'
			creds["dba_user"] = self.get_registry_data_server(val, serverDir)
			val = self.database + '/Password'
			creds["dba_pass"] = self.get_registry_data_server(val, serverDir)
		except FileNotFoundError:
			traceback.print_exc()
			self.logging.error('FileNotFoundError: ' + val)
		self.creds = creds
		return creds
	
	
	""" 
	Opens and returns a connection to the database
	Param: creds - the database credentials (SERVER, USER, PASSWORD) 
	"""
	def open_connection(self, creds):
		try:
			conn = pyodbc.connect('DRIVER=SQL Server;SERVER={0};DATABASE={1};UID={2};PWD={3};'.format(
			creds["dba_server"], self.database, creds["dba_user"], creds["dba_pass"]), autocommit=True)
		except FileNotFoundError:
			traceback.print_exc()
			self.logging.error('FileNotFoundError: Invalid database name provided.')
		return conn
	
	"""
	"""
	def set_connection(self, conn):
		self.conn = conn

	"""
	"""		
	def set_id_to_content_type(self, id_to_content_type):
		self.id_to_content_type = id_to_content_type
		
	"""
	"""		
	def get_table_columns(self):
		columns = self.columns
		tableString = "("
		for col in columns:
			tableString = tableString + '[' + col[0] + ']'
			tableString = tableString + " " + col[1] + ", "
		tableString = tableString[:-2]
		tableString = tableString + ");"
		return tableString
	
	"""
	Creates a table of the table_name given by the user
	Param: columns - dictionary of column name : type .. i.e. Primary Name : varchar(100)
	"""
	def create_table(self, table_name, suffix):
		self.myTable = table_name
		tableString = self.get_table_columns()
		cursor = self.conn.cursor()
		sql_command = """IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[""" + table_name + suffix + """]') AND type in (N'U'))
						DROP TABLE [dbo].[""" + table_name + suffix + """]"""

		cursor.execute(sql_command)
		sql_command = "CREATE TABLE " + table_name + suffix + tableString
		cursor.execute(sql_command)
		self.conn.commit()
	
	"""
	"""		
	def get_data(self, sql_command):
		cursor = self.conn.cursor()
		cursor.execute(sql_command)
		return cursor

	def execute_command(self, sql_command):
		cursor = self.conn.cursor()
		cursor.execute(sql_command)
		self.conn.commit()
		
	def execute_procedure(self, procedure, args):
		cursor = self.conn.cursor()
		cursor.callproc(procedure)
		self.conn.commit()
		return cursor
		
	"""
	"""		
	def rename_tables(self):
		cursor = self.conn.cursor()
		original_name = self.myTable
		old_name = original_name + '_bak'
		self.drop_table(self.conn, old_name)
		new_name = original_name + '_new'
		# changing the original table to the old table
		try:
			sql_command = "exec sp_rename '" + original_name + "', '" + old_name + "'"
			cursor.execute(sql_command)
			self.conn.commit()
		except pyodbc.ProgrammingError:
			pass
		# changing the new table to the original table
		sql_command = "exec sp_rename '" + new_name + "', '" + original_name + "'"
		cursor.execute(sql_command)
		self.conn.commit()
		# time.sleep(1)
		# dropping the new table because it is now the original table
		# sql_command = 'DROP TABLE [dbo].[' + new_name + ']'
		# cursor.execute(sql_command)
		# self.conn.commit()

	"""
	"""		
	def rename_tables2(self, table_name):
		cursor = self.conn.cursor()
		original_name = table_name
		old_name = original_name + '_bak'
		self.drop_table(self.conn, old_name)
		new_name = original_name + '_new'
		# changing the original table to the old table
		try:
			sql_command = "exec sp_rename '" + original_name + "', '" + old_name + "'"
			cursor.execute(sql_command)
			self.conn.commit()
		except pyodbc.ProgrammingError:
			pass
		# changing the new table to the original table
		sql_command = "exec sp_rename '" + new_name + "', '" + original_name + "'"
		cursor.execute(sql_command)
		self.conn.commit()
		# time.sleep(1)
		# dropping the new table because it is now the original table
		# sql_command = 'DROP TABLE [dbo].[' + new_name + ']'
		# cursor.execute(sql_command)
		# self.conn.commit()
		
	"""
	"""		
	def drop_table(self, db_conn, table_name):
		cursor = db_conn.cursor()
		sql_command = """IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[""" + table_name + """]') AND type in (N'U'))
						DROP TABLE [dbo].[""" + table_name + """]"""
		cursor.execute(sql_command)
		db_conn.commit()

	"""
	"""		
	def find_deviation(self, db_conn, max_deviation):
		if max_deviation >= 100:
			return True
		passed_test = False
		cursor = db_conn.cursor()
		try:
			sql_command = 'select count(*) from ' + self.myTable
			sql_command_new = 'select count(*) from ' + self.myTable + '_new'
			cursor.execute(sql_command)
			row_count = cursor.fetchone()[0]
			cursor.execute(sql_command_new)
			row_count_new = cursor.fetchone()[0]
		except pyodbc.ProgrammingError: #might have to do more here
			traceback.print_exc()
			self.create_table(self.myTable,'')
			self.rename_tables()
			return True
		diff = None
		try:
			if row_count_new >= row_count:
				diff = 1 - (row_count/row_count_new)
			else:
				diff = 1 - (row_count_new/row_count)
			if diff >= (max_deviation/100):
				self.logging.error('Max deviation failed. Exiting system.')
			elif diff < (max_deviation/100):
				passed_test = True
			else:
				print('else statement reached in find_deviation() that shouldnt be reached ever') # turn this into an error
		except ZeroDivisionError:
			if row_count_new > row_count:
				passed_test = True
			else:
				traceback.print_exc()
				self.logging.error("ZeroDivisionError: New table is empty. Table must be populated to continue.")
		return passed_test

		
		
		
	
		