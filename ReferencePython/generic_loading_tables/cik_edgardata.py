import argparse
import sys
import os
import time
import subprocess
import io
from datetime import timedelta, date

def main():
	# Command line parsing.
	def msg(name='edgardata.py'):
		return "python " + name + ".py TODO msg()"
	# Parser for user argument input.
	parser = argparse.ArgumentParser(prog='cik', usage=msg())
	
	# The destination argument is used to define the directory where files will be downloaded to.
	# $XLSDATA/destination
	parser.add_argument('--destination', type=str, metavar='',nargs='?')
	parser.add_argument('--batch', type=str, metavar='',nargs='?')

	args = parser.parse_args()
	
	# If the user provided a destination argument it is set here.
	destination = args.destination 
	# If the user did not define a destination argument it is set to 'cik'.
	if destination == None: destination = 'cik/download' 
	
	batch = args.batch
	if not batch: batch = '2'
	
	
	# ---------- VARIABLES ---------- #
	XLSDATA = os.environ.get('XLSDATA') + '/'
	XLS = os.environ.get('XLS') + '/' 
	# ---------- VARIABLES ---------- #
	
	file = open(XLSDATA + "cik/download/filelist"+batch+".tmp", 'r')
	count = 1
	for line in file.readlines():
		print(count)
		print(os.popen(line.strip('\n')).read())
		count += 1
		
		
if __name__ == '__main__':
	main()