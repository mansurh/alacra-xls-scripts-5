client_dir=""
destination="--destination 'concordance'"
server=""

if [ $# -gt 0 ]
then
  client_dir=$1 
fi
if [ $# -gt 1 ]
then
  destination="--destination $2"
fi
if [ $# -gt 2 ]
then
  server="--server $3"
fi

loaddir="$XLSDATA/Auto_IA"
mkdir -p ${loaddir}
logfile="${loaddir}/sh_log.log"

main()
{
 echo "python3 ${XLS}/src/scripts/ReferencePython/generic_loading_tables/ipid_loader.py ${client_dir} ${destination} ${server}"
 python3 ${XLS}/src/scripts/ReferencePython/generic_loading_tables/auto_investments_advisors.py ${client_dir} ${destination} ${server}
}
main > ${logfile} 2>&1

exit 0