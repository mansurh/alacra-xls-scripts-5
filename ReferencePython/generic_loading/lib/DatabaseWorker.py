import pyodbc
import subprocess as sproc
import logging
import sys
import time

""" 
Allows ease of use when working with a database 
"""
class DatabaseWorker(object):
	database = None 
	source = None
	table_name = None
	
	""" 
	Initializes the DataBase worker with its default database name 
	"""
	def __init__(self):
		self.database = 'concordance'
		#initialize logging file name
		logging.basicConfig(filename='logger_bad.log', filemode='w', level=logging.DEBUG, 
		format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p: ')
	
	""" 
	Initializes the DatabaseWorker with a given database name 
	"""
	def __init__(self, database, source):
		self.source = source
		self.database = database
		#initialize logging file name
		logging.basicConfig(filename='logger_bad.log', filemode='w', level=logging.DEBUG)

	""" 
	Provides registry data to help set the credentials
	Param: val - a string of the database and credential desired, example below 
	"""
	def get_registry_data_server(self, val, serverDir):
		# e.g. val like dba2/Password OR concordance/Server
		try:
			val = sproc.check_output("regtool get '/machine/software/xls/" + serverDir + "/{0}'".format(val), shell=True)
		except sproc.CalledProcessError:
			logging.error('subprocess.CalledProcessError: Invalid server directory - check database|client|regDir names.')
			print('subprocess.CalledProcessError: Invalid server directory - check database|client|regDir names.')
			sys.exit()
		return val.decode().strip()
		
	""" 
	Returns the credentials for the database 
	"""
	def get_creds(self, serverDir):
		creds = {}
		try:
			val = self.database + '/Server'
			creds["dba_server"] = self.get_registry_data_server(val, serverDir)
			val = self.database + '/User'
			creds["dba_user"] = self.get_registry_data_server(val, serverDir)
			val = self.database + '/Password'
			creds["dba_pass"] = self.get_registry_data_server(val, serverDir)
		except FileNotFoundError:
			logging.error('FileNotFoundError: ' + val)
			print('FileNotFoundError: ' + val)
			sys.exit()
		return creds
		
		
	def get_sftp_creds(self, serverDir):
		creds = {}
		try:
			val = self.database + '/Server'
			creds["dba_server"] = self.get_registry_data_server(val, serverDir)
			val = self.database + '/User'
			creds["dba_user"] = self.get_registry_data_server(val, serverDir)
			val = self.database + '/Key'
			creds["dba_key"] = self.get_registry_data_server(val, serverDir)
			val = self.database + '/Port'
			creds["dba_port"] = self.get_registry_data_server(val, serverDir)
			if len(creds["dba_port"]) < 1:
				creds["dba_pass"] = None
		except FileNotFoundError:
			if val == self.database + '/Port':
				creds["dba_port"] = None
			else:
				logging.error('FileNotFoundError: ' + val)
				print('FileNotFoundError: ' + val)
				sys.exit()
		return creds
	
	""" 
	Opens and returns a connection to the database
	Param: creds - the database credentials (SERVER, USER, PASSWORD) 
	"""
	def open_connection(self, creds):
		try:
			conn = pyodbc.connect('DRIVER=SQL Server;SERVER={0};DATABASE={1};UID={2};PWD={3};'.format(
			creds["dba_server"], self.database, creds["dba_user"], creds["dba_pass"]))
		except FileNotFoundError:
			logging.error('FileNotFoundError: Invalid database name provided.')
			print('FileNotFoundError: Invalid database name provided.')
			sys.exit()
		return conn
		
	def set_table_name(self, table_name):
		self.table_name = table_name
		
	def get_table_details(self, conn):
		sql_command = """select [file],[table_name] from ipid_config where source='"""+self.source+"""'"""
		try:
			cursor = conn.cursor().execute(sql_command)
			results = {}
			for row in cursor.fetchall():
				results[row[0]]=row[1]
			print(results)
		except TypeError:
			logging.error('NoneTypeError: Invalid client name provided.')
			print('NoneTypeError: Invalid client name provided.')
			#sys.exit()
		except pyodbc.ProgrammingError:
			logging.error('pyodbc.ProgrammingError: Invalid sql command.')
			print('pyodbc.ProgrammingError: Invalid sql command.')
			sys.exit()
		return results # dictionary of { file : table_name }
		
		
	"""
	Selects the table given by the user
	Param: conn - an open connections to a database
	"""
	def select_table(self, conn, table_name):
		# sql_command = 'select * from ipid_config'
		sql_command = """select name, value from ipid_table where table_name='""" + table_name + """' order by id"""
		try:
			cursor = conn.cursor().execute(sql_command)
			columns = [] 
			for column in cursor.description:
				columns.append(column[0])
			results = []
			for row in cursor.fetchall():
				results.append(dict(zip(columns,row)))
		except TypeError:
			logging.error('NoneTypeError: Invalid client name provided.')
			print('NoneTypeError: Invalid client name provided.')
			sys.exit()
		except pyodbc.ProgrammingError:
			logging.error('pyodbc.ProgrammingError: Invalid sql command.')
			print('pyodbc.ProgrammingError: Invalid sql command.')
			sys.exit()
		return results # array of dictionaries ... [{name : name, value : value }, {"}, {"}]
	
	"""
	Parses the results from selectTable()
	Param: results - the return statement from selectTable()
	"""
	def parse_select_statement(self, results):
		tableString = "("
		for r in results:
			tableString = tableString + '[' + r['name'] + ']'
			tableString = tableString + " " + r['value'] + ", "
		tableString = tableString[:-2]
		tableString = tableString + ");"
		return tableString
	
	"""
	Creates a table of the table_name given by the user
	Param: conn - an open connections to a database
	"""
	def create_table(self, conn, table_name, table_suffix):
		self.table_name = table_name 
		tableString = self.parse_select_statement(self.select_table(conn, table_name))
		cursor = conn.cursor()
		#try:
		sql_command = """IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[""" + self.table_name + table_suffix + """]') AND type in (N'U'))
						DROP TABLE [dbo].[""" + self.table_name + table_suffix + """]"""

		cursor.execute(sql_command)
		sql_command = "CREATE TABLE " + self.table_name + table_suffix + tableString
		print(sql_command)
		cursor.execute(sql_command)
		conn.commit()
		#except pyodbc.ProgrammingError:
			#logging.error("pyodbc.ProgrammingError: Invalid client name provided.")
			#print("pyodbc.ProgrammingError:: Invalid client name provided. Exiting system.")
			#sys.exit()
		
	def rename_tables(self, db_conn):
		self.create_table(db_conn, self.table_name, '_bak')
		cursor = db_conn.cursor()
		original_name = self.table_name
		old_name = original_name + '_bak'
		new_name = original_name + '_new'
		# changing the original table to the old table
		self.delete_table_data(db_conn, old_name)
		sql_command = 'INSERT INTO ' + old_name + ' SELECT * FROM ' + original_name
		cursor.execute(sql_command)
		db_conn.commit()
		# changing the new table to the original table
		self.delete_table_data(db_conn, original_name)
		sql_command = 'INSERT INTO ' + original_name + ' SELECT * FROM ' + new_name
		cursor.execute(sql_command)
		db_conn.commit()
		time.sleep(1)
		# dropping the new table because it is now the original table
		sql_command = 'DROP TABLE [dbo].[' + new_name + ']'
		cursor.execute(sql_command)
		db_conn.commit()
			
	def delete_table_data(self, db_conn, table_name):
		cursor = db_conn.cursor()
		sql_command = 'DELETE FROM ' + table_name
		cursor.execute(sql_command)
		db_conn.commit()
		
	def find_ipid_client_data(self, db_conn, column_name):
		cursor = db_conn.cursor()
		sql_command = "SELECT " + column_name +" FROM ipid_client where source='" + self.source + "'"
		data = None
		try:
			cursor.execute(sql_command)
			data = cursor.fetchone()[0]
		except TypeError:
			logging.error('NoneTypeError: Invalid client name.')
			print('NoneTypeError: Invalid client name.')
			sys.exit()
		except pyodbc.ProgrammingError:
			logging.error('pyodbc.ProgrammingError: Invalid sql command.')
			print('pyodbc.ProgrammingError: Invalid sql command.')
			sys.exit()
		return data
		