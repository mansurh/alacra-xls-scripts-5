#set -x
XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

#parse the command line arguments
# 1 = source server
# 2 = sourcedb
# 3 = dest server
# 4 = dest db
# 5 = srctable
# 6 = dest table

if [ $# -lt 5 ]
then
    echo "Usage: transferTable.sh srcserver srcdb destserver destdb srctable [desttable]"
    exit 1
fi

srcserver=$1
srcdb=$2
destserver=$3
destdb=$4
srctable=$5
desttable=$6

if [ $# -lt 6 ]
then
	desttable=$5
fi

srcuser=`get_xls_registry_value ${srcdb} User`
srcpasswd=`get_xls_registry_value ${srcdb} Password`

destuser=`get_xls_registry_value ${destdb} User`
destpasswd=`get_xls_registry_value ${destdb} Password`

run() {

echo "Database from:           $srcserver"
echo "Database to:             $destserver"

. $XLS/src/scripts/transfer/transfertable2.sh ${srctable} ${srcdb} ${srcserver} ${srcuser} ${srcpasswd} ${desttable} ${destdb} ${destserver} ${destuser} ${destpasswd}

echo "DONE"
}

mkdir -p $XLSDATA/transfer
logfile=$XLSDATA/transfer/table_copy_${srcserver}_${srctable}.log
(run) > ${logfile} 2>&1
if [ $? -ne 0 ]
then
	blat ${filename} -t administrators@alacra.com -s "Copy of ${srcdb}.${srctable} from ${srcserver} to ${destserver} failed."
	exit 1
fi

exit 0