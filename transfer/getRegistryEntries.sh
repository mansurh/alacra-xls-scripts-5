# Parse command line arguments.
#	1 = ControlDB name
XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

if [ $# -ne 1 ]
then
    echo "Usage: getRegistryEntries.sh ControlDBName"
    return 1
fi

# Save the runstring parameters in local variables
controldbname=$1

# See where the ControlDB database is
destserver=`get_xls_registry_value ${controldbname} Server`
# Check for error
if [ $? != 0 ]
then
	destserver=`get_xls_registry_value ${controldbname} server`
	if [ $? != 0 ]
	then
		echo "Error getting ${controldbname} database server from registry"
		exit 1
	fi
fi

# Get the ControlDB database user
destuser=`get_xls_registry_value ${controldbname} user`
# Check for error
if [ $? != 0 ]
then
	destuser=`get_xls_registry_value ${controldbname} User`
	if [ $? != 0 ]
	then
		echo "Error getting ${controldbname} database user name from registry"
		exit 1
	fi
fi

# Get the ControlDB database password
destpasswd=`get_xls_registry_value ${controldbname} password`
# Check for error
if [ $? != 0 ]
then
	destpasswd=`get_xls_registry_value ${controldbname} Password`
	if [ $? != 0 ]
	then
		echo "Error getting ${controldbname} database password from registry"
		exit 1
	fi
fi

# -------------------------------------------------------------------------------------------------------------------

# See where the SpiderDB database is
srcserver=`get_xls_registry_value spiderdb server`
# Check for error
if [ $? != 0 ]
then
	srcserver=`get_xls_registry_value spiderdb Server`
	if [ $? != 0 ]
	then
		echo "Error getting spiderdb database server from registry"
		exit 1
	fi
fi

# Get the SpiderDB database user
srcuser=`get_xls_registry_value spiderdb user`
# Check for error
if [ $? != 0 ]
then
	srcuser=`get_xls_registry_value spiderdb User`
	if [ $? != 0 ]
	then
		echo "Error getting spiderdb database user name from registry"
		exit 1
	fi
fi

# Get the SpiderDB database password
srcpasswd=`get_xls_registry_value spiderdb password`
# Check for error
if [ $? != 0 ]
then
	srcpasswd=`get_xls_registry_value spiderdb Password`
	if [ $? != 0 ]
	then
		echo "Error getting spiderdb database password from registry"
		exit 1
	fi
fi
