# DevCopyXlsDb.sh
# Parse the command line arguments.
#	1 = source server
#	2 = source user
#	3 = source psw
#	4 = dest server
#	5 = dest user
#	6 = dest psw
#	7 = noget (optional)

shname=DevCopyXlsDb.sh

TITLEBAR="Making Development copy of xls database"

if [ $# -lt 6 ]
then
	echo "Usage: ${shname} sourceserver sourceuser sourcepsw destserver destuser destpsw"
	exit 1
fi

# Save the runstring parameters in local variables
sourceserver=$1
sourceuser=$2
sourcepsw=$3
destserver=$4
destuser=$5
destpsw=$6
# optional 'noget' as 7th param skips source safe step
if [ $# -gt 6 ]
then
	noget=$7
else
	noget=N
fi



# log file name
mkdir -p $XLSDATA/DevCopyXlsDb
logfilename=$XLSDATA/DevCopyXlsDb/DevCopyXlsDb.log

echo "${shname}: Refreshing the Development (test) Copy of the XLS Database" > ${logfilename}
echo "${shname}: Will selectively copy xls database from ${sourceserver} to ${destserver}" >> ${logfilename}
echo ""

# Perform the update
$XLS/src/scripts/transfer/DevCopyXlsDb/DevCopyXlsDbLow.sh ${sourceserver} ${sourceuser} ${sourcepsw} ${destserver} ${destuser} ${destpsw} ${noget} >> ${logfilename} 2>&1
returncode=$?

# Mail the results to the administrators if update was no good
if [ ${returncode} != 0 ]
then
	echo "${shname}: Bad return code from DevCopyXlsDbLow.sh - mailing log file" >> ${logfilename}
	blat ${logfilename} -t "administrators@xls.com" -s "Development DB Copy Failed"
else
	# good return code, all done
fi

exit $returncode
