XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

if [ $# -lt 1 ] || [ $# -gt 3 ]
then
    echo "usage: $0 projname [ftp] [reg name]"
    exit 1
fi

projname=$1

regname=$1
if [ $# -gt 2 ]
then
	regname=$3
fi

user=`get_xls_registry_value ${regname} User 6`
pass=`get_xls_registry_value ${regname} Password 6`
ftpserver=`get_xls_registry_value ${regname} Server 6`
sftpkey=`get_xls_registry_value ${regname} Key 6`
ftpfolder=`get_xls_registry_value ${regname} Outbound 6`
clienttype=`get_xls_registry_value ${regname} ClientType 6`
errorstr="(Connection closed by remote host|Access is denied)"

doftp="1"
if [ $# -gt 1 ]
then
	if [ $2 == "noftp" ]
	then
		doftp="0"
	else
		ftpserver=$2
	fi
fi

usecurl=0
if [ "$clienttype" = "curl" ]; then usecurl=1; fi;

main()
{
cd $XLSDATA/$projname
if [ $? -ne 0 ]
then
	echo "$0: error in cd to $XLSDATA/$projname, exiting ..." 
	exit 3
fi

echo "$0:" `date +%Y%m%d-%H:%M` 

verb=del
if [ $doftp -eq 1 ]
then
  ftpcmdfile=delfiles.ftp
  rm -f ${ftpcmdfile}
  if [ $usecurl -eq 1 ]
  then
    curl -s -k -O "sftp://${user}:${pass}@${ftpserver}/${ftpfolder}/${filelist}" 
    returncode=$?
  elif [ "${sftpkey}" = "" ]
  then
    echo "user $user " > ${ftpcmdfile}
    echo "$pass" >> ${ftpcmdfile}
    echo "binary " >> ${ftpcmdfile}
    if [ "${ftpfolder}" != "" ]
    then
      echo "cd ${ftpfolder}" >> ${ftpcmdfile}
    fi
    echo "get ${filelist}" >> ${ftpcmdfile}
    echo "quit" >> ${ftpcmdfile}
    ftp -i -n -s:${ftpcmdfile} ${ftpserver} 
    returncode=$?
  else
    verb=rm
    if [ "${ftpfolder}" != "" ]
    then
      echo "cd ${ftpfolder}" >> ${ftpcmdfile}
    fi
    echo "get ${filelist}" >> ${ftpcmdfile}
    echo "quit" >> ${ftpcmdfile}
    sftp -b ${ftpcmdfile} -oIdentityFile=$XLS/src/${sftpkey} ${user}@${ftpserver} 
    returncode=$?
  fi

  if [ $returncode -ne 0 ]
  then
	echo "$0: " `date +%Y%m%d-%H:%M` 
	echo "$0: error in getting ${filelist} from ${ftpserver}, exiting ... " 
	exit $returncode
  fi
fi

if [ ! -s ${filelist} ]
then
	echo "$0:" `date +%Y%m%d-%H:%M` 
	echo "$0: ${filelist} is empty on $COMPUTERNAME, exiting ... " 
	rm -f ${filelist}
	exit 1
fi

awk -f $XLS/src/scripts/ManageFTP/del.awk -v PASS=$pass -v USER=$user -v DIR="${ftpfolder}" -v DELCMD="${verb}" ${filelist} > delfiles.ftp
returncode=$?
if [ $returncode -ne 0 ]
then
	echo "$0:" `date +%Y%m%d-%H:%M` 
	echo "$0: error in del.awk, exiting ... " 
	exit $returncode
fi

if [ $doftp -eq 1 ]
then
  if [ ${usecurl} -eq 1 ]
  then
    #This is most importan file to delete, other files can be reloaded, so don't check their errors
    curl -s -k "sftp://${user}:${pass}@${ftpserver}/${ftpfolder}/" -Q "rm /${ftpfolder}/${filelist}"  #> /dev/null
    if [ $? -ne 0 ]; then echo "$0: error in deleting ${filelist}, exiting ..."; exit 1; fi;

    while read filename
    do
	#try deleting individual files, don't stop if they fail, as we can rerun them later
	curl -s -k "sftp://${user}:${pass}@${ftpserver}/${ftpfolder}/" -Q "rm /${ftpfolder}/${filename}"  > /dev/null
	echo "status deleting $filename - " $?
#	if [ $? -ne 0 ]; then echo "$0: error in deleting $filename, exiting ..."; exit 1; fi;
    done < ${filelist}
  elif [ "${sftpkey}" != "" ]
  then
    sed -e "1,3d" < delfiles.ftp > delfiles.sftp
    sftp -b delfiles.sftp -oIdentityFile=$XLS/src/${sftpkey} ${user}@${ftpserver} 
    returncode=$?
  else
    ftp -i -n -s:delfiles.ftp ${ftpserver} > temp.log 2>&1
    returncode=$?
    egrep -q "$errorstr" temp.log
    if [ $? -eq 0 -o $returncode -ne 0 ]
    then
		#retry the command if ftp failed or Connection closed by remote host
		echo "FTP failed, retry one more time" 
		ftp -i -n -s:delfiles.ftp ${ftpserver} > temp.log 2>&1	
		returncode=$?
		egrep -q "$errorstr" temp.log
		if [ $? -eq 0 -a $returncode -eq 0 ]; then returncode=1; fi;
    fi
    cat temp.log
  fi

  if [ $returncode -ne 0 ]
  then
	echo "$0:" `date +%Y%m%d-%H:%M` 
	echo "$0: error deleting files on ${ftpserver}, exiting ... " 
	exit $returncode
  fi
fi

echo "$0:" `date +%Y%m%d-%H:%M` 
rm -f ${filelist}
rm -f ${counterfile}
}

logfile=$XLSDATA/$projname/ManageFtp`date +%m%d%H`.log
filelist=${ftpserver}.${projname}.filelist
counterfile=${ftpserver}.${projname}.counter

(main) >> ${logfile} 2>&1
rc=$?
exit $rc

