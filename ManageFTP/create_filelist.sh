if [ $# -lt 2 ] || [ $# -gt 3 ]
then
    echo "usage: $0 filename.type database"
    exit 1
fi

files=$1
database=$2
ftpname=ftp
if [ $# -gt 2 ]
then
	ftpname=$3
fi

getfilelist.sh ${database} ${files} ${ftpname}
returncode=$?
exit $returncode
