rkey="HKEY_LOCAL_MACHINE\\SOFTWARE\\XLS\\Mailto Forms\\privategrid1"
registry -s -k "$rkey" -n "required_fields" -v "email"
registry -s -k "$rkey" -n "web_form" -v "c:\\users\\xls\\src\\scripts\\mailto\\privategrid\\emailform.htm"

rkey="HKEY_LOCAL_MACHINE\\SOFTWARE\\XLS\\Mailto Forms\\privategrid1\\applicantletter"
registry -s -k "$rkey" -n "template_files" -v "c:\\users\\xls\\src\\scripts\\mailto\\privategrid\\emailstep1.txt"
registry -s -k "$rkey" -n "command" -v "blat \${_file0} -f \"Administrators@PrivateGrid.com\" -t \"\${email,c}\" -s \"PrivateGrid Account Application\" -q"

rkey="HKEY_LOCAL_MACHINE\\SOFTWARE\\XLS\\Mailto Forms\\privategrid1\\sqlinsert"
registry -s -k "$rkey" -n "template_files" -v "c:\\users\\xls\\src\\scripts\\mailto\\privategrid\\step1.sql"
registry -s -k "$rkey" -n "command" -v "isql /S data9 /U mailto /P mailto < \${_file0} > nul"

rkey="HKEY_LOCAL_MACHINE\\SOFTWARE\\XLS\\Mailto Forms\\privategrid1\\webresponse"
registry -s -k "$rkey" -n "template_files" -v "c:\\usr\\netscape\\server\\docs\\privategrid\\step1web.htm"
registry -s -k "$rkey" -n "command" -v "web response"


rkey="HKEY_LOCAL_MACHINE\\SOFTWARE\\XLS\\Mailto Forms\\privategrid2"
registry -s -k "$rkey" -n "web_form" -v "c:\\usr\\netscape\\server\\docs\\privategrid\\appform.htm"
registry -s -k "$rkey" -n "required_fields" -v "address1,city,state,zip"

rkey="HKEY_LOCAL_MACHINE\\SOFTWARE\\XLS\\Mailto Forms\\privategrid2\\script"
registry -s -k "$rkey" -n "template_files" -v "c:\\users\\xls\\src\\scripts\\mailto\\privategrid\\step2emailok.txt,c:\\users\\xls\\src\\scripts\\mailto\\privategrid\\step2emailng.txt,c:\\users\\xls\\src\\scripts\\mailto\\privategrid\\step2.sql"
registry -s -k "$rkey" -n "command" -v "sh.exe -c \"c:/users/xls/src/scripts/mailto/privategrid/step2.sh \"data9\" \"mailto\" \"mailto\" \"\${_file2,k}\" \"\${_file0,k}\" \"\${_file1,k}\" \"\${contactemail,c}\" \${_uuid,c}\""

rkey="HKEY_LOCAL_MACHINE\\SOFTWARE\\XLS\\Mailto Forms\\privategrid2\\webresponse"
registry -s -k "$rkey" -n "template_files" -v "c:\\usr\\netscape\\server\\docs\\privategrid\\step2web.htm"
registry -s -k "$rkey" -n "command" -v "web response"

