. $XLS/src/scripts/loading/dba/get_registry_value.fn

jscheddir=`get_registry_value "HKEY_LOCAL_MACHINE\\SOFTWARE\\XLS\\Jsched" "logdir"`
pvtdir=`get_registry_value "HKEY_LOCAL_MACHINE\\SOFTWARE\\XLS\\Pvt files" "Pathfile"`

run() {
cd $XLS
find . -size +2000 -type f -ls > filelist.txt 2>&1

if [ -s filelist.txt ]
then
  blat filelist.txt -s "${COMPUTERNAME}: Listing of files larger than 1MB in $XLS" -t operations@alacra.com
  rm -f filelist.txt
fi

echo "start cleanup: logfiles" 
cd c:/logfiles
if [ $? -ne 0 ]; 
then 
  echo "error in cd to c:/logfiles" 
  exit 1;
fi

days=180  
echo "Deleting log files older than ${days} days" 
find . -type f -mtime +${days} -ls -delete

days=30
echo "Archiving log files older than ${days} days"
find . -type f ! -name "*.gz" -ctime +${days} -ls -exec gzip -f \{\} \;

# If logging to the d: drive
if [ -d d:/logfiles ]
then
	cd d:/logfiles
	if [ $? -ne 0 ]; 
	then 
	  echo "error in cd to d:/logfiles" 
	  exit 1;
	fi

	days=180  
	echo "Deleting log files older than ${days} days" 
	find . -type f -mtime +${days} -ls -delete

	days=30
	echo "Archiving log files older than ${days} days"
	find . -type f ! -name "*.gz" -ctime +${days} -ls -exec gzip -f \{\} \;
fi


days=15
echo "Deleting private files older than ${days} days" 
cd ${pvtdir}
if [ $? -ne 0 ] || [ -z ${pvtdir} ]
then
	echo "Jsched dir ${pvtdir} doesn't exist or not in registry" 
	return 1
else
	find . -type f -mtime +${days} -ls -delete
fi

cd c:/usr/netscape/server/docs/tmp/acms
if [ $? -eq 0 ]
then
	days=15
	echo "Deleting acms files older than ${days} days" 
	find . -type f -mtime +${days} -ls -delete
fi

if [ -e c:/inetpub/logs/LogFiles ]
then
  echo "Found IIS7 directory: c:/inetpub/logs/LogFiles"
  cd c:/inetpub/logs/LogFiles 
elif [ -e c:/windows/system32/logfiles ]
then
  echo "Found IIS6 directory: c:/inetpub/logs/LogFiles "
  cd c:/windows/system32/logfiles
else
  echo "No IIS directory for logs found, exiting ..."
  exit 1
fi

if [ $? -ne 0 ];
then
  echo "Error in cd to Logfiles directory, exiting ..."
  exit 1
fi

days=180  
echo "Deleting log files older than ${days} days" 
find . -name \*.log\* -mtime +${days} -ls -delete

days=14
echo "Archiving log files older than ${days} days"
find . -name \*.log -mtime +${days} -ls -exec gzip -f \{\} \;


echo "jsched dir ${jscheddir}" 
cd ${jscheddir}
if [ $? -ne 0 ] || [ -z ${jscheddir} ]
then
	echo "Jsched dir '${jscheddir}' doesn't exist or not in registry" 
	return 1
else
	find . -name job\*.log -mtime +365 -delete 	 
fi

}

cd c:/logfiles
logfile=cleanup_logs`date +%m%d`.log 
(run) > ${logfile} 2>&1
if [ $? -ne 0 ]
then
  blat ${logfile} -s "${COMPUTERNAME}: Error running cleanup for log files" -t administrators@alacra.com
  exit 1
else
  echo ${logfile}
#  blat ${logfile} -s "Cleanup log for ${COMPUTERNAME}" -t operations@alacra.com
fi

