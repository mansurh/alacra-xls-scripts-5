XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

server=""
server=`get_xls_registry_value tempiddb server`
DATE=`date +%y%m%d%H`
log="deltables$1$DATE.log"
if [ "$server" == "" ]
then
	echo "nothing to do"
	exit 0
fi

deltemptables.exe $1 > $log 2>&1

#if log is empty, delete it
if [ ! -s $log ]
then
  rm $log
else
  blat ${log} -t administrators@alacra.com -s "Delete temptables failed on $COMPUTERNAME"
  exit 1
fi
