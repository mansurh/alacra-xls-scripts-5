# Main script to process update for lexis Database
#
# History: 
#	06/16/05 - Created - MZ
#

set -x

if [ $# -lt 4 ]
then
	echo "Usage: lexisPurge.sh server user password daysback"
	exit 1
fi

# Save the runstring parameters in local variables
server=$1
logon=$2
password=$3
days=$4

date >lexis.log

string="delete storycache where DATEDIFF(day,downloaddate, GETDATE()) > "$days
echo $string>>lexis.log

isql /U${logon} /P${password} /S${server} /Q"delete storycache where DATEDIFF(day,downloaddate, GETDATE()) > "$days >>lexis.log

blat lexis.log -t "michael.zaccardi@alacra.com" -s "Lexis story cache Update - ${server}"

erase lexis.log

exit ${returncode}