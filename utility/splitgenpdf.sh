# Routine to split all .pdf files in a gentext app

shname=splitgenpdf.sh

# Parse the command line arguments
#	1 = Server
#	2 = User
#	3 = Password

if [ $# != 3 ]
then
	echo "Usage: ${shname} server user password"
	exit 1
fi

tempfile=temp.tmp


dbserver=$1
dbuser=$2
dbpassword=$3

query="select path + '/' + filename from extfile where UPPER(filename) like '%.PDF'"


if [ -e ${tempfile} ]
then
	rm -f ${tempfile}
fi

bcp "${query}" queryout ${tempfile} /S${dbserver} /U${dbuser} /P${dbpassword} /c 


while read pdffile
do
	outputdir=${pdffile%/*}

	filename=${pdffile##*/}
	textfile=${filename%.*}.txt

	if [ ! -e ${outputdir}/${textfile} ]
	then
#		echo "$XLS/src/scripts/utility/splitpdf.sh ${pdffile} ${outputdir}"
		$XLS/src/scripts/utility/splitpdf.sh ${pdffile} ${outputdir}
		if [ $? != 0 ]
		then
		    echo "${shname}: Error converting ${pdffile} to text"
		fi
	fi

done < ${tempfile}


rm ${tempfile}

exit 0
