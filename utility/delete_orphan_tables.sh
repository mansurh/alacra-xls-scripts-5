XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

server=`get_xls_registry_value tempiddb server`

logfile=delete_orphan.log
orphanfile=orphan.txt
rm -f ${logfile}
bcp "select u.name + '.' + o.name From sysobjects o, sysusers u where o.crdate < dateadd(day, -3, getdate()) and o.name like 'tmp%' and o.uid = u.uid order by o.crdate" queryout ${orphanfile} /S${server} /Uwspace /Pwspace /c >> ${logfile} 2>&1
if [ $? -ne 0 ]
then
	echo "error bcp out temp tables, exiting ..." >> ${logfile}
	blat ${logfile} -t isrequests@alacra.com -s "Error getting orphan tables on $server"
	exit 1
fi

while read a
do
  echo "dropping table $a"
  isql /S${server} /Uwspace /Pwspace /Q "drop table $a"
done < ${orphanfile}

bcp "select u.name + '.' + o.name From sysobjects o, sysusers u where o.crdate < dateadd(day, -3, getdate()) and o.name like 'tmp%' and o.uid = u.uid order by o.crdate" queryout ${orphanfile} /S${server} /Uwspace /Pwspace /c >> ${logfile} 2>&1
if [ $? -ne 0 ]
then
	echo "error bcp2 out temp tables, exiting ..." >> ${logfile}
	blat ${logfile} -t isrequests@alacra.com -s "Error getting orphan tables on $server"
	exit 1
fi

if [ -s ${orphanfile} ]
then
	blat ${orphanfile} -t isrequests@alacra.com -s "Error deleting orphan tables on $server"
	exit 1
fi
