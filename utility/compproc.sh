shname=compproc.sh
if [ $# -lt 7 ]
then
    echo "Usage: ${shname} srcserver srclogin srcpasswd destserver destlogin destpasswd procname"
    exit 1
fi

srcserver=${1}
srcuser=${2}
srcpasswd=${3}
destserver=${4}
destuser=${5}
destpasswd=${6}
procname=${7}

if [ -e temp.tmp ]
then
	rm temp.tmp
fi

mysql="sp_helptext ${procname}"
tempfile="temp.tmp"

file1=${srcserver}_${procname}.sql
rm -f ${file1}
isql /Q "${mysql}" /S ${srcserver} /U ${srcuser} /P ${srcpasswd} -b -n -h-1 -w4096 > ${tempfile}
if [ $? != 0 ]
then
    echo "${shname}: Error in isql against server ${srcserver}"
    exit 1
fi

#echo "waiting..."
#read zzz

grep "[A-Za-z0-9]" ${tempfile} > ${file1}

file2=${destserver}_${procname}.sql
rm -f ${file2}
isql /Q "${mysql}" /S ${destserver} /U ${destuser} /P ${destpasswd} -b -n -h-1 -w4096 > ${tempfile}
if [ $? != 0 ]
then
    echo "${shname}: Error in isql against server ${destserver}"
    exit 1
fi

#echo "waiting..."
#read zzz

grep "[A-Za-z0-9]" ${tempfile} > ${file2}

rm ${tempfile}

diff -b ${file1} ${file2}
exit $?
