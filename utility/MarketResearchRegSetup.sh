rkey="HKEY_LOCAL_MACHINE\\SOFTWARE\\XLS\\Application Options\\marketresearch"
registry -s -k "$rkey" -n "DownloadTimeout" -v "180"
registry -s -k "$rkey" -n "DownloadUrl" -v "http://www.marketresearch.com/feed/alacra/download.asp?"
registry -s -k "$rkey" -n "MaxThreads" -v "10"
registry -s -k "$rkey" -n "PartnerID" -v "899763025"
registry -s -k "$rkey" -n "DownloadTimeInterval" -v "10"

# This password is for test web servers
#registry -s -k "$rkey" -n "Password" -v "alacrademo"

# This password is for production web servers
registry -s -k "$rkey" -n "Password" -v "alacralive66pine"

registry -s -k "$rkey" -n "ProductUrl" -v "http://www.marketresearch.com/feed/prod_listing.asp?partnerid=899763025&productID="
registry -s -k "$rkey" -n "SearchUrl" -v "http://www.marketresearch.com/feed/alacra/search_results.asp"
registry -s -k "$rkey" -n "SearchUrl_debug" -v "http://www.marketresearch.com/feed/alacra/search_results_debug.asp"
registry -s -k "$rkey" -n "SliceUrl" -v "http://www.marketresearch.com/feed/prod_slices.asp?partnerid=899763025&productid="
registry -s -k "$rkey" -n "SubmitXMLOrderUrl" -v "https://www.marketresearch.com/feed/xcbl/submitorder.asp"

# This user id is for test web servers
#registry -s -k "$rkey" -n "UserID" -v "111766"

# This user id is for production web servers
registry -s -k "$rkey" -n "UserID" -v "111777"
