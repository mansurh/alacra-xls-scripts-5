#retuns number of pages in the pdf document
if [ $# -ne 1 ]
then
  echo "usage: $0 filename"
  exit 1
fi
pagelst=`pdls.exe $1 |grep -i page |tail -1| awk '{print $2 }'`
echo $pagelst
exit 0
