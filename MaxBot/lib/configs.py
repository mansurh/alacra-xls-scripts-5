import functools
import pyodbc
import subprocess as sproc
from .colors import colors
from .print import cprint
from .globals import globals, whereisenv


creds = {}

@functools.lru_cache()
def get_ace_pods():
    print(creds["dba_server"], creds["dba_user"], creds["dba_pass"])
    conn = pyodbc.connect('DRIVER={{SQL Server}};SERVER={0};DATABASE=dba2;UID={1};PWD={2}'.format(
        creds["dba_server"], creds["dba_user"], creds["dba_pass"]
    ))
    c = conn.cursor()
    c.execute("select * from resource where name like 'ace%' and type = 6")
    dbs = [row.name for row in c.fetchall()]
    dbs_in_env = []
    for db in dbs:
        c.execute("exec whereis ?, ?", db, whereisenv[globals.env])
        row = c.fetchone()
        if row is not None and row.status != 'offline':
            dbs_in_env.append(db)
    return dbs_in_env

def get_registry_data_server(val):
    """e.g. val like dba2/Password"""
    val = sproc.check_output("regtool get '/machine/software/xls/Data Servers/{0}'".format(val), shell=True)
    return val.decode().strip()

def get_cred_from_user(db, cred_type):
    key = "{0}_{1}".format(db, cred_type)
    ask_user = False
    if key not in creds:
        if cred_type != 'Master':
            if globals.args.dry_run:
                val = cred_type.lower()
            elif globals.env == 'dev' and globals.args.no_credentials:
                print('Assuming {db} user and pass is {dbdbo}'.format(db=db, dbdbo=db+'dbo'))
                val = db + 'dbo'
            else:
                ask_user = True
        else:
            # See if there is more than 1 server for this env
            conn = pyodbc.connect('DRIVER={{SQL Server}};SERVER={0};DATABASE=dba2;UID={1};PWD={2}'.format(
                creds["dba_server"], creds["dba_user"], creds["dba_pass"]
            ))
            c = conn.cursor()
            c.execute("exec whereis ?, ?", db, whereisenv[globals.env])
            servers = [row.server for row in c.fetchall() if row.status != 'offline']
            if len(servers) == 0:
                cprint.warning('Warning: No non-offline servers found for db {db} in {env}'.format(db=db, env=globals.env))
            if len(servers) != 1: 
                ask_user = True
            else:
                val = servers[0]
                # cprint.warning('Only one server for {db} in {env}; assuming it is the Master: {server}'.format(db=db, env=globals.env, server=val))
                
        if ask_user:
            if cred_type == 'Master':
                color = colors.BLUEBG
            else:
                color = colors.GREEN
            val = input("Enter" + color + " {0} ".format(db) + colors.END + globals.env + color + " {0} ".format(cred_type) + colors.END + "(default: {db}dbo) ".format(db=db))
            if val == "":
                val = db + "dbo"
            
        creds[key] = val
    return creds[key]


# initialize creds
creds["dba_server"] = get_registry_data_server('dba2/Server')
creds["dba_user"] = get_registry_data_server('dba2/User')
creds["dba_pass"] = get_registry_data_server('dba2/Password')
