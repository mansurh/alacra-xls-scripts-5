from .colors import colors


whereisenv = {
    'dev': 'development',
    'test': 'test',
    'stage': 'staging',
    'prod': 'production'
}

fromenv = {
    'dev': '',
    'test': 'IN_DEV',
    'stage': 'IN_TEST',
    'prod': 'IN_STAGE'
}

identityenv = {
    'dev': 'IN_DEV',
    'test': 'IN_TEST',
    'stage': 'IN_STAGE',
    'prod': 'IN_PROD'
}

class Globals:

    def get_env_suffix(self):
        env_suffix = ""
        if not globals.args.no_label_suffix:
            while len(env_suffix) == 0:
                env_suffix = input(colors.GREEN + "Label suffix? [if don't want to use suffix, rerun with -nls argument] " + colors.END)
                if self.logfile:
                    self.logfile.write("Label suffix? ")
                    self.logfile.write(env_suffix + '\n')
        self.env_suffix = env_suffix

    def get_env(self):
        env = ""
        while env not in ('dev', 'test', 'stage', 'prod'):
            env = input(colors.GREEN + "What environment are we releasing to? [dev/test/stage/prod] " + colors.END)
            if self.logfile:
                self.logfile.write("What environment are we releasing to? [dev/test/stage/prod] ")
                self.logfile.write(env + '\n')
        self.env = env

        if globals.args.rerun:
            self.fromenv = identityenv[env]
        else:
            self.fromenv = fromenv[env]

    def set_logfile(self, logfile):
        self.logfile = logfile


globals = Globals()

globals.tmpbasedir = '/cygdrive/c/Temp/packageSQL/'
