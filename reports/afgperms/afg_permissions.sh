XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Parse the command line arguments
#	1 - database
if [ $# -lt 1 ]
then
	echo "Usage: $0 database"
	exit 1
fi

database=$1
# Save the runstring parameters
logon=`get_xls_registry_value ${database} User`
password=`get_xls_registry_value ${database} Password`
dataserver=`get_xls_registry_value ${database} Server`
emailgrp=afgperms@alacra.com

mkdir -p $XLSDATA/reports
cd $XLSDATA/reports

outputfile=afg_temp.txt
reportfile=afg_permissions`date +%m`.xls

bcp "select l.account, u.login, l.lwid, ip.name, permission, dataset, tier from lwid_perm l, users u, ip where l.userid=u.id and l.ip=ip.id and l.account in (6423, 6450) order by l.account, l.lwid, ip.name" queryout ${outputfile} /U${logon} /P${password} /S${dataserver} /c
rc=$?
if [ $rc -ne 0 ]
then
	blat -body "Error generating AFG permissions report" -t administrators@alacra.com -s "Error generating AFG permissions report"
	exit $rc
fi

cat $XLS/src/scripts/reports/afgperms/afg_header.tsv ${outputfile} > ${reportfile}

blat -body "attached" -attach ${reportfile} -t ${emailgrp} -s "AFG permissions"

