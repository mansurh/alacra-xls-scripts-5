set nocount on
set transaction isolation level read uncommitted

/* Active Feeds */
select a.id, REPLACE(a.url,'|',' '), REPLACE(a.source,'|',' '), a.source_type, b.descr, b.group_id, c.descr
from feed_candidates a, source_type b, source_group c
where a.source_type=b.id and b.group_id=c.id and deleted_flag is NULL