set nocount on
set transaction isolation level read uncommitted

/* Active Analysts */
select a.analyst_id, a.first_name, a.middle_name, a.last_name, a.full_name, 'firmID'=b.id, 'firmName'=b.name, t.analyst_type
from analysts a, firms b, analyst_firm c, analyst_type t
where a.analyst_id=c.analyst_id and b.id=c.firm_id
and c.analyst_type=t.id
and c.date_ended is null 
and a.correction is null
order by b.name, t.analyst_type, a.last_name, a.first_name
