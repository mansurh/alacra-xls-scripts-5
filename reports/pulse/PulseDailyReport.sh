 XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

if [ $# -lt 1 ]
then
	echo "Usage: PulseDailyRegs.sh emailDest"
	exit 1
fi

shname=$0
emailDest=$1




# --------------------------------


out_dir=$XLSDATA/pulse
logfile=${out_dir}/PulseDailyReport_`date +%y%m%d`.log
reportfile=PulseDailyReport_`date +%y%m%d`.xlsx
fullreportfile=${out_dir}/PulseDailyReport_`date +%y%m%d`.xlsx


if [ ! -d ${out_dir} ]
then
	mkdir -p ${out_dir}
fi


PulseDailyReport.exe -i=$XLS/src/scripts/reports/pulse/PulseDailyReport.dat -o=${fullreportfile} > ${logfile}
if [ $? -ne 0 ]
then
	blat ${logfile} -t administrators@alacra.com -s "Pulse Daily Report failed."
	exit 1
else
	desc="Pulse Daily Report"

	# Mail out the report.  Must be a binary attachment as UTF-8 uses the high bit
	cd ${out_dir}

	blat -to ${emailDest} -subject "${desc}" -body "${desc}.  See Attached." -uuencode -attach ${reportfile}
	if [ $? != 0 ]
	then
		echo "Error sending report file via Blat, terminating"
		exit 1
	fi
fi

exit 0

