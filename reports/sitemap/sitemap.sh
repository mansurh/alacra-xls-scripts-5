TITLEBAR="Sitemap Check"

if [ $# -ne 3 ]
then
    echo "Usage: sitemap.sh server user password"
    exit 1
fi

server=$1
user=$2
password=$3

TEMPDIR=$XLSDATA/sitemap

echo "Checking Alacrastore Sitemap Index" >$TEMPDIR/sitemap.log
echo >> $TEMPDIR/sitemap.log

perl $XLS/src/scripts/reports/sitemap/indexcheck.pl "http://s1.alacrastore.com/cgi-bin/sitemapindex.exe?msg=index&mode=validate" $TEMPDIR/update.sql >> $TEMPDIR/sitemap.log 2>&1

if [ -s $TEMPDIR/update.sql ]
then
# errors found in the sitemap
grep -v OK $TEMPDIR/sitemap.log >$TEMPDIR/badsites.log

# update the table
isql /S${server} /U${user} /P${password} <<EOF
update sitemaps set disable=NULL where disable=1
EOF

if [ $? -ne 0 ]
then
    echo "Unable to reset the DB for changes"
    exit 1
fi

isql /S${server} /U${user} /P${password} <$TEMPDIR/update.sql

if [ $? -ne 0 ]
then
    echo "Unable to disable bad sitemaps"
    exit 1
fi

isql /S${server} /U${user} /P${password} /n >$TEMPDIR/devdisable.log <<EOF 
set nocount on
select distinct regkey from sitemaps where disable=2
EOF

tail +3 $TEMPDIR/devdisable.log >>$TEMPDIR/devdisable2.log

if [ -s $TEMPDIR/devdisable2.log ]
then
echo "\n\nDEVELOPMENT DISABLED SITES" >> $TEMPDIR/badsites.log
cat $TEMPDIR/devdisable2.log >> $TEMPDIR/badsites.log
fi

blat $TEMPDIR/badsites.log -t "administrators@xls.com" -s "Sitemap Index Problems"
fi

