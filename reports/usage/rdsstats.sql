declare @startdate smalldatetime
declare @enddate smalldatetime

/* Run for previous month */
select @startdate = DATEADD(month, -1, GETDATE())

/* Move to beginning of the month */
select @startdate = DATEADD(day, -1 * (DATEPART(day, @startdate) -1), @startdate)
select @startdate = DATEADD(hour, -1 * DATEPART(hour, @startdate), @startdate)
select @startdate = DATEADD(minute, -1 * DATEPART(minute, @startdate), @startdate)

/* Calculate the end of the monthly period */
select @enddate = DATEADD (month, 1, @startdate)

select "Month Starting", @startdate


/****************************** RDSSuite *************************************/
print " "
print "RDSSuite Concurrent User Limits"
print "==============================="
select 
	a.id "Account", CONVERT (varchar(48),a.name) "Company/School", l.max_users "Concurrent Users", l.min_to_doze "Timeout 1", l.time_out "Timeout 2"
from
	account a, users_limit l
where
	a.id = l.account
and
	l.service=11

print " "
print "Valid RDSSuite User IDs"
print "======================="
select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", p.last_name "Last Name", p.login "Login", CONVERT (varchar(12),p.commence,107) "Start Date", CONVERT (varchar(12),p.terminate,107) "End-Date"
from
	account a, users p
where
	p.account *= a.id
and
	p.service = 11
and
	p.terminate >= @startdate
and
	p.demo_flag is null
order by
	COALESCE (p.company, a.name), p.last_name

print " "
print "RDSSuite Logins During Month"
print "============================"
select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", p.last_name "User", count(*) "Number of Logins"
from 
	logins l, account a, users p
where
	p.account = a.id
and
	l.userid = p.id
and
	l.return_code=0
and
	p.service = 11
and
	l.time >= @startdate
and
	l.time < @enddate
and
	p.demo_flag is null
group by
	CONVERT (varchar(48),COALESCE (p.company, a.name)), p.last_name

print " "
print "RDSSuite users that did not login during month"
print "=============================================="
select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", p.last_name "Last Name", p.login "Login", CONVERT (varchar(12),p.commence,107) "Start Date", CONVERT (varchar(12),p.terminate,107) "End-Date"
from
	account a, users p
where
	p.account *= a.id
and
	p.service = 11
and
	p.commence < @enddate
and
	p.terminate >= @startdate
and
	p.demo_flag is null
and
	p.id not in (select userid from logins where time >= @startdate and time < @enddate)
order by
	COALESCE (p.company, a.name), p.last_name

print " "
print "RDSSuite users that did not download anything during month"
print "=========================================================="
select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", p.last_name "Last Name", p.login "Login", CONVERT (varchar(12),p.commence,107) "Start Date", CONVERT (varchar(12),p.terminate,107) "End-Date"
from
	account a, users p
where
	p.account *= a.id
and
	p.service = 11
and
	p.commence < @enddate
and
	p.terminate >= @startdate
and
	p.demo_flag is null
and
	p.id not in (select userid from usage where access_time >= @startdate and access_time < @enddate)
order by
	COALESCE (p.company, a.name), p.last_name


print " "
print "RDSSuite Lockouts During Month"
print "=============================="
select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", p.last_name "User", count(*) "Number of Lockouts"
from 
	logins l, account a, users p
where
	p.account = a.id
and
	l.userid = p.id
and
	l.return_code=4
and
	p.service = 11
and
	l.time >= @startdate
and
	l.time < @enddate
and
	p.demo_flag is null
group by
	CONVERT (varchar(48),COALESCE (p.company, a.name)), p.last_name

print " "
print "Monthly RDSSuite Subscription Usage"
print "================================"
select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", CASE u.ip WHEN 37 THEN "B&I" WHEN 66 THEN "Tablebase" WHEN 67 THEN "BaMP" WHEN 49 THEN "CWI" END "Database", count(distinct u.usageid) "Downloads", count (s.usage) "Stories"
from 
	usage u, users p , account a, usageSpecial s
where
	u.userid = p.id 
and
	p.account *= a.id
and
	u.usageid = s.usage
and 
	p.service = 11 
and 
	p.demo_flag is null
and
	access_time >= @startdate and access_time < @enddate
group by
	p.company, a.name, u.ip
order by p.company, a.name
compute sum(count(distinct u.usageid)), sum(count (s.usage)) by p.company, a.name


/****************************** BIDB *************************************/
print " "
print "BIDB Concurrent User Limits"
print "==========================="
select 
	a.id "Account", CONVERT (varchar(48),a.name) "Company/School", l.max_users "Concurrent Users", l.min_to_doze "Timeout 1", l.time_out "Timeout 2"
from
	account a, users_limit l
where
	a.id = l.account
and
	l.service=5

print " "
print "Valid BIDB User IDs"
print "==================="
select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", p.last_name "Last Name", p.login "Login", CONVERT (varchar(12),p.commence,107) "Start Date", CONVERT (varchar(12),p.terminate,107) "End-Date"
from
	account a, users p
where
	p.account *= a.id
and
	p.service = 5
and
	p.terminate >= @startdate
and
	p.demo_flag is null
order by
	COALESCE (p.company, a.name), p.last_name

print " "
print "BIDB Logins During Month"
print "========================"
select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", p.last_name "User", count(*) "Number of Logins"
from 
	logins l, account a, users p
where
	p.account = a.id
and
	l.userid = p.id
and
	l.return_code=0
and
	p.service = 5
and
	l.time >= @startdate
and
	l.time < @enddate
and
	p.demo_flag is null
group by
	CONVERT (varchar(48),COALESCE (p.company, a.name)), p.last_name

print " "
print "BIDB users that did not login during month"
print "=========================================="
select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", p.last_name "Last Name", p.login "Login", CONVERT (varchar(12),p.commence,107) "Start Date", CONVERT (varchar(12),p.terminate,107) "End-Date"
from
	account a, users p
where
	p.account *= a.id
and
	p.service = 5
and
	p.commence < @enddate
and
	p.terminate >= @startdate
and
	p.demo_flag is null
and
	p.id not in (select userid from logins where time >= @startdate and time < @enddate)
order by
	COALESCE (p.company, a.name), p.last_name

print " "
print "BIDB users that did not download anything during month"
print "======================================================"
select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", p.last_name "Last Name", p.login "Login", CONVERT (varchar(12),p.commence,107) "Start Date", CONVERT (varchar(12),p.terminate,107) "End-Date"
from
	account a, users p
where
	p.account *= a.id
and
	p.service = 5
and
	p.commence < @enddate
and
	p.terminate >= @startdate
and
	p.demo_flag is null
and
	p.id not in (select userid from usage where access_time >= @startdate and access_time < @enddate)
order by
	COALESCE (p.company, a.name), p.last_name

print " "
print "BIDB Lockouts During Month"
print "=========================="

select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", p.last_name "User", count(*) "Number of Lockouts"
from 
	logins l, account a, users p
where
	p.account = a.id
and
	l.userid = p.id
and
	l.return_code=4
and
	p.service = 5
and
	l.time >= @startdate
and
	l.time < @enddate
and
	p.demo_flag is null
group by
	CONVERT (varchar(48),COALESCE (p.company, a.name)), p.last_name



print " "
print "Monthly BIDB Subscription Usage"
print "==============================="
select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", count(distinct u.usageid) "Downloads", count (s.usage) "Stories"
from 
	usage u, users p , account a, usageSpecial s
where
	u.userid = p.id 
and
	p.account *= a.id
and
	u.usageid = s.usage
and 
	p.service = 5 
and 
	p.demo_flag is null
and
	access_time >= @startdate and access_time < @enddate
group by
	CONVERT (varchar(48),COALESCE (p.company, a.name))



/****************************** BAMP *************************************/
print " "
print "BAMP Concurrent User Limits"
print "==========================="
select 
	a.id "Account", CONVERT (varchar(48),a.name) "Company/School", l.max_users "Concurrent Users", l.min_to_doze "Timeout 1", l.time_out "Timeout 2"
from
	account a, users_limit l
where
	a.id = l.account
and
	l.service=9

print " "
print "Valid BAMP User IDs"
print "==================="
select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", p.last_name "Last Name", p.login "Login", CONVERT (varchar(12),p.commence,107) "Start Date", CONVERT (varchar(12),p.terminate,107) "End-Date"
from
	account a, users p
where
	p.account *= a.id
and
	p.service = 9
and
	p.terminate >= @startdate
and
	p.demo_flag is null
order by
	COALESCE (p.company, a.name), p.last_name

print " "
print "BAMP Logins During Month"
print "========================"
select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", p.last_name "User", count(*) "Number of Logins"
from 
	logins l, account a, users p
where
	p.account = a.id
and
	l.userid = p.id
and
	l.return_code=0
and
	p.service = 9
and
	l.time >= @startdate
and
	l.time < @enddate
and
	p.demo_flag is null
group by
	CONVERT (varchar(48),COALESCE (p.company, a.name)), p.last_name

print " "
print "BAMP users that did not login during month"
print "=========================================="
select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", p.last_name "Last Name", p.login "Login", CONVERT (varchar(12),p.commence,107) "Start Date", CONVERT (varchar(12),p.terminate,107) "End-Date"
from
	account a, users p
where
	p.account *= a.id
and
	p.service = 9
and
	p.commence < @enddate
and
	p.terminate >= @startdate
and
	p.demo_flag is null
and
	p.id not in (select userid from logins where time >= @startdate and time < @enddate)
order by
	COALESCE (p.company, a.name), p.last_name

print " "
print "BAMP users that did not download anything during month"
print "======================================================"
select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", p.last_name "Last Name", p.login "Login", CONVERT (varchar(12),p.commence,107) "Start Date", CONVERT (varchar(12),p.terminate,107) "End-Date"
from
	account a, users p
where
	p.account *= a.id
and
	p.service = 9
and
	p.commence < @enddate
and
	p.terminate >= @startdate
and
	p.demo_flag is null
and
	p.id not in (select userid from usage where access_time >= @startdate and access_time < @enddate)
order by
	COALESCE (p.company, a.name), p.last_name

print " "
print "BAMP Lockouts During Month"
print "=========================="

select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", p.last_name "User", count(*) "Number of Lockouts"
from 
	logins l, account a, users p
where
	p.account = a.id
and
	l.userid = p.id
and
	l.return_code=4
and
	p.service = 9
and
	l.time >= @startdate
and
	l.time < @enddate
and
	p.demo_flag is null
group by
	CONVERT (varchar(48),COALESCE (p.company, a.name)), p.last_name


print " "
print "Monthly BAMP Subscription Usage"
print "==============================="
select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", count(distinct u.usageid) "Downloads", count (s.usage) "Stories"
from 
	usage u, users p , account a, usageSpecial s
where
	u.userid = p.id 
and
	p.account *= a.id
and
	u.usageid = s.usage
and 
	p.service = 9 
and 
	p.demo_flag is null
and
	access_time >= @startdate and access_time < @enddate
group by
	CONVERT (varchar(48),COALESCE (p.company, a.name))

/****************************** Tablebase *************************************/
print " "
print "Tablebase Concurrent User Limits"
print "================================"
select 
	a.id "Account", CONVERT (varchar(48),a.name) "Company/School", l.max_users "Concurrent Users", l.min_to_doze "Timeout 1", l.time_out "Timeout 2"
from
	account a, users_limit l
where
	a.id = l.account
and
	l.service=8

print " "
print "Valid Tablebase User IDs"
print "========================="
select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", p.last_name "Last Name", p.login "Login", CONVERT (varchar(12),p.commence,107) "Start Date", CONVERT (varchar(12),p.terminate,107) "End-Date"
from
	account a, users p
where
	p.account *= a.id
and
	p.service = 8
and
	p.terminate >= @startdate
and
	p.demo_flag is null
order by
	COALESCE (p.company, a.name), p.last_name

print " "
print "Tablebase Logins During Month"
print "============================="
select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", p.last_name "User", count(*) "Number of Logins"
from 
	logins l, account a, users p
where
	p.account = a.id
and
	l.userid = p.id
and
	l.return_code=0
and
	p.service = 8
and
	l.time >= @startdate
and
	l.time < @enddate
and
	p.demo_flag is null
group by
	CONVERT (varchar(48),COALESCE (p.company, a.name)), p.last_name


print " "
print "TableBase users that did not login during month"
print "=============================================="
select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", p.last_name "Last Name", p.login "Login", CONVERT (varchar(12),p.commence,107) "Start Date", CONVERT (varchar(12),p.terminate,107) "End-Date"
from
	account a, users p
where
	p.account *= a.id
and
	p.service = 8
and
	p.commence < @enddate
and
	p.terminate >= @startdate
and
	p.demo_flag is null
and
	p.id not in (select userid from logins where time >= @startdate and time < @enddate)
order by
	COALESCE (p.company, a.name), p.last_name

print " "
print "Tablebase users that did not download anything during month"
print "=========================================================="
select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", p.last_name "Last Name", p.login "Login", CONVERT (varchar(12),p.commence,107) "Start Date", CONVERT (varchar(12),p.terminate,107) "End-Date"
from
	account a, users p
where
	p.account *= a.id
and
	p.service = 8
and
	p.commence < @enddate
and
	p.terminate >= @startdate
and
	p.demo_flag is null
and
	p.id not in (select userid from usage where access_time >= @startdate and access_time < @enddate)
order by
	COALESCE (p.company, a.name), p.last_name

print " "
print "Tablebase Lockouts During Month"
print "==============================="

select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", p.last_name "User", count(*) "Number of Lockouts"
from 
	logins l, account a, users p
where
	p.account = a.id
and
	l.userid = p.id
and
	l.return_code=4
and
	p.service = 8
and
	l.time >= @startdate
and
	l.time < @enddate
and
	p.demo_flag is null
group by
	CONVERT (varchar(48),COALESCE (p.company, a.name)), p.last_name



print " "
print "Monthly Tablebase Subscription Usage"
print "===================================="
select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", count(distinct u.usageid) "Downloads", count (s.usage) "Stories"
from 
	usage u, users p , account a, usageSpecial s
where
	u.userid = p.id 
and
	p.account *= a.id
and
	u.usageid = s.usage
and 
	p.service = 8 
and 
	p.demo_flag is null
and
	access_time >= @startdate and access_time < @enddate
group by
	CONVERT (varchar(48),COALESCE (p.company, a.name))


/****************************** CWI *************************************/
print " "
print "CWI Concurrent User Limits"
print "=========================="
select 
	a.id "Account", CONVERT (varchar(48),a.name) "Company/School", l.max_users "Concurrent Users", l.min_to_doze "Timeout 1", l.time_out "Timeout 2"
from
	account a, users_limit l
where
	a.id = l.account
and
	l.service=4

print " "
print "Valid CWI User IDs"
print "=================="
select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", p.last_name "Last Name", p.login "Login", CONVERT (varchar(12),p.commence,107) "Start Date", CONVERT (varchar(12),p.terminate,107) "End-Date"
from
	account a, users p
where
	p.account *= a.id
and
	p.service = 4
and
	p.terminate >= @startdate
and
	p.demo_flag is null
order by
	COALESCE (p.company, a.name), p.last_name

print " "
print "CWI Logins During Month"
print "============================"
select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", p.last_name "User", count(*) "Number of Logins"
from 
	logins l, account a, users p
where
	p.account = a.id
and
	l.userid = p.id
and
	l.return_code=0
and
	p.service = 4
and
	l.time >= @startdate
and
	l.time < @enddate
and
	p.demo_flag is null
group by
	CONVERT (varchar(48),COALESCE (p.company, a.name)), p.last_name


print " "
print "CWI users that did not login during month"
print "========================================="
select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", p.last_name "Last Name", p.login "Login", CONVERT (varchar(12),p.commence,107) "Start Date", CONVERT (varchar(12),p.terminate,107) "End-Date"
from
	account a, users p
where
	p.account *= a.id
and
	p.service = 4
and
	p.commence < @enddate
and
	p.terminate >= @startdate
and
	p.demo_flag is null
and
	p.id not in (select userid from logins where time >= @startdate and time < @enddate)
order by
	COALESCE (p.company, a.name), p.last_name

print " "
print "CWI users that did not download anything during month"
print "====================================================="
select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", p.last_name "Last Name", p.login "Login", CONVERT (varchar(12),p.commence,107) "Start Date", CONVERT (varchar(12),p.terminate,107) "End-Date"
from
	account a, users p
where
	p.account *= a.id
and
	p.service = 4
and
	p.commence < @enddate
and
	p.terminate >= @startdate
and
	p.demo_flag is null
and
	p.id not in (select userid from usage where access_time >= @startdate and access_time < @enddate)
order by
	COALESCE (p.company, a.name), p.last_name

print " "
print "CWI Lockouts During Month"
print "========================="

select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", p.last_name "User", count(*) "Number of Lockouts"
from 
	logins l, account a, users p
where
	p.account = a.id
and
	l.userid = p.id
and
	l.return_code=4
and
	p.service = 4
and
	l.time >= @startdate
and
	l.time < @enddate
and
	p.demo_flag is null
group by
	CONVERT (varchar(48),COALESCE (p.company, a.name)), p.last_name




print " "
print "Monthly CWIDB Subscription Usage"
print "================================"
select 
	CONVERT (varchar(48),COALESCE (p.company, a.name)) "Company/School", count(distinct u.usageid) "Downloads", count (s.usage) "Stories"
from 
	usage u, users p , account a, usageSpecial s
where
	u.userid = p.id 
and
	p.account *= a.id
and
	u.usageid = s.usage
and 
	p.service = 4 
and 
	p.demo_flag is null
and
	access_time >= @startdate and access_time < @enddate
group by
	CONVERT (varchar(48),COALESCE (p.company, a.name))






