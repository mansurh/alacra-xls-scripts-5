# Command file to send report of tearsheets for JP Morgan UK
# Parse the command line arguments.
#   	1 = dbserver
#   	2 = dbuser
#   	3 = dbpassword
#   	4 = date (optional) YYMM

shname=jpmsnapshots.sh
if [ $# -lt 3 ]
then
	echo "Usage: ${shname} dbserver dbuser dbpassword [YYMM]"
	exit 1
fi


targetacct=3762

# Set up vars
dbserver=$1
dbuser=$2
dbpassword=$3
if [ $# -gt 3 ]
then

	lastmonthYYYY=20${4%??}
	lastmonthMM=${4#??}

else


	# Call into ISQL to get the date from 1 month ago:
	lastmonth=`isql -S${dbserver} -U${dbuser} -P${dbpassword} -h-1 -n -Q"set nocount on; select CONVERT (varchar(16), DATEADD(month,-1,GETDATE()), 112)"`
	if [ $? != 0 ]
	then
		echo "${shname}: Could not compute yesterdays date through isql"
		exit 1
	fi

	lastmonth=`echo ${lastmonth} | sed -e "s/ //g"`
	if [ $? != 0 ]
	then
		echo "${shname}: Could not edit date"
		exit 1
	fi

	#echo "lastmonth=${lastmonth}"

	lmtemp=${lastmonth#????}
	#echo "lmtemp=${lmtemp}"

	lastmonthMM=${lmtemp%??}

	lastmonthYYYY=${lastmonth%????}

fi

echo "lastmonthMM=${lastmonthMM}"
echo "lastmonthYYYY=${lastmonthYYYY}"



extractfilename=jpmorgan${lastmonthYYYY}-${lastmonthMM}.dat
tempextractfilename=jpmorgan${lastmonthYYYY}-${lastmonthMM}.tmp


# extract the column names first
colnamefile=columnNames.tmp
colnamefile2=columnNames2.tmp

rm -f ${colnamefile}
rm -f ${colnamefile2}
query="exec sp_columns 'alacralog'"
bcp "${query}" queryout ${colnamefile} /S${dbserver} /U${dbuser} /P${dbpassword} /c /t"|" /r"\n"
if [ $? != 0 ]
then
	echo "${shname}: Could not extract column names via SQL"
    exit 1
fi
# turn the column names into a single line, pipe delimted
awk -f $XLS/src/scripts/reports/usage/extractColumns.awk ${colnamefile} > ${colnamefile2}
rm -f ${colnamefile}


# extract the usage
query="select * from alacralog where accountid = ${targetacct} and MONTH(eventtime) = ${lastmonthMM} and YEAR(eventtime) = ${lastmonthYYYY} order by eventtime desc"
bcp "${query}" queryout ${tempextractfilename} /S${dbserver} /U${dbuser} /P${dbpassword} /c /t"|" /r"\n"
if [ $? != 0 ]
then
	echo "${shname}: Could not extract usage via SQL"
    exit 1
fi

# assemble the final file
rm -f ${extractfilename}
cat ${colnamefile2} ${tempextractfilename} > ${extractfilename}
rm -f ${colnamefile2}
rm -f ${tempextractfilename}


echo "Attached is a report of tearsheets on account ${targetacct} for ${lastmonthMM}/${lastmonthYYYY}" > temp.tmp

# now e-mail the resulting file to Alacra UK
blat222 temp.tmp -attach ${extractfilename} -t "gail.jewsbury@uk.alacra.com,angela.lowther@alacra.com,owen.hourican@uk.alacra.com" -s "JP Morgan Tearsheet Report for ${lastmonthMM}/${lastmonthYYYY}"
#blat222 temp.tmp -attach ${extractfilename} -t "anthony.bruni@alacra.com" -s "JP Morgan Tearsheet Report for ${lastmonthMM}/${lastmonthYYYY}"

rm temp.tmp


exit 0
