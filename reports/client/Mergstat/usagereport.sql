declare @startdate smalldatetime
declare @enddate smalldatetime


SET NOCOUNT ON

/* Run for last week */
select @startdate = DATEADD(day, -7, GETDATE())

select @startdate = DATEADD(hour, -1 * DATEPART(hour, @startdate), @startdate)
select @startdate = DATEADD(minute, -1 * DATEPART(minute, @startdate), @startdate)

/* Calculate the end of the monthly period */
select @enddate = DATEADD (month, 1, @startdate)


PRINT ".XLS Usage Report for Mergerstat"
PRINT ""
PRINT ""
select CONVERT(varchar(16),@startdate,107) "For Week Starting"
PRINT ""
PRINT ""

PRINT "Downloads by User"
PRINT "==================================="
select 
	convert (varchar(40),COALESCE (p.company, a.name)) "Company/Account Name",
	convert (varchar(16),p.last_name) "User Last Name",
	convert (varchar(16),s.last_name) "Salesperson Last Name",
	convert (varchar(40),u.description) "Description",
	convert (varchar(10),u.access_time, 1) "Date",
	u.list_price "List Price",
	u.price "Price"
from 
	usage u, users p, salesperson s, account a
where
	u.access_time >= @startdate
and
	u.access_time < @enddate
and
	u.userid = p.id
and
	u.ip = 20
and
	u.access_time >= p.commence
and
	p.demo_flag is null
and
	p.account = a.id
and	
	s.id = p.salesrep
and	
	u.no_charge_flag is null

order by
	COALESCE (p.company, a.name), p.last_name, u.access_time
compute sum(u.price), 
	sum(u.list_price) 

go
