# Define the scheduling server
schedserver=data3

# Korn shell example
# at \\\\data2 21:00 "c:\\usr\\mks\\mksnt\\sh.exe -c \"cd c:/users/xls/src/scripts/loading/mgfs;c:/users/xls/src/scripts/loading/mgfs/startprices_both.sh\""

# Set up the MGFS daily load
at \\\\${schedserver} 01:00 loadjob c:/users/xls/src/scripts/jobstreams/vtech
